function onLoad()
    createButtons()
    bagCharacter = getObjectFromGUID('2e05bf')
    bagFigurines = getObjectFromGUID('d72f95')
    pos = self.getPosition()
    actionDeckPos = {-4.91, 4, -41.52}
    zoneActionDeck = getObjectFromGUID(Global.getTable('ZONES').ZONE_ACTION_DECK
                                           .guid)

    charSelectionMat = getObjectFromGUID(
                           Global.getTable('ZONES').ZONE_CHOOSE_CHARACTER.guid[1])
    player1CharPos = {-21.9, 4, -50.6}
    player2CharPos = {12.4, 4, -50.6}
    player3CharPos = {-40.2, 4, -50.6}
    player4CharPos = {30.7, 4, -50.6}
end

function createButtons()
    self.createButton({
        click_function = "loadFerdinand",
        function_owner = self,
        label = "",
        position = {0, 0.6, -5.4},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadVictor",
        function_owner = self,
        label = "",
        position = {0, 0.6, -4.6},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadDimitri",
        function_owner = self,
        label = "",
        position = {0, 0.6, -3.8},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadLovecraft",
        function_owner = self,
        label = "",
        position = {0, 0.6, -3},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadKeelan",
        function_owner = self,
        label = "",
        position = {0, 0.6, -2.2},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadEliot",
        function_owner = self,
        label = "",
        position = {0, 0.6, -1.4},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadMary",
        function_owner = self,
        label = "",
        position = {0, 0.6, 2},
        scale = {0.5, 0.5, 0.5},
        width = 5900,
        height = 600,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadAnjika",
        function_owner = self,
        label = "",
        position = {0, 0.6, -0.6},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadPhileas",
        function_owner = self,
        label = "",
        position = {0, 0.6, 0.2},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadAmelia",
        function_owner = self,
        label = "",
        position = {0, 0.6, 1},
        width = 4700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadSinglePlayer",
        function_owner = self,
        label = "",
        position = {-1, 0.6, 5.6},
        width = 600,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadTwoPlayers",
        function_owner = self,
        label = "",
        position = {1.2, 0.6, 5.6},
        width = 600,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadThreePlayers",
        function_owner = self,
        label = "",
        position = {3.6, 0.6, 5.6},
        width = 800,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "loadFourPlayers",
        function_owner = self,
        label = "",
        position = {5.7, 0.6, 5.6},
        width = 700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "setupPlayerOne",
        function_owner = self,
        label = "",
        position = {-1, 0.6, 6.5},
        width = 600,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "setupPlayerTwo",
        function_owner = self,
        label = "",
        position = {1.2, 0.6, 6.5},
        width = 600,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "setupPlayerThree",
        function_owner = self,
        label = "",
        position = {3.6, 0.6, 6.5},
        width = 800,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "setupPlayerFour",
        function_owner = self,
        label = "",
        position = {5.7, 0.6, 6.5},
        width = 700,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    -- Sends card to action Deck
    --  self.createButton({click_function = "sendActionDeck", function_owner = self, label = "",
    --  position = {3.9, 0.6, 7.9}, scale = {0.5, 0.5, 0.5}, width = 5900, height = 600, font_size = 400,
    --  color = {1, 1, 1, 0}, font_color = {0, 0, 0, 100}})
end

function sendActionDeck()
    local hitList = Physics.cast({
        origin = {pos.x + 1.63, pos.y + 1, pos.z - 2.76},
        direction = {0, 1, 0},
        type = 2,
        size = {1, 1, 1},
        max_distance = 0
    })
    for _, entry in ipairs(hitList) do
        if entry.hit_object.tag == "Deck" then
            entry.hit_object.flip()
            entry.hit_object.setScale({0.69, 1.00, 0.69})
            entry.hit_object.setPosition(actionDeckPos)
        end
    end
    startLuaCoroutine(self, 'shuffleActionDeck')
end

function shuffleActionDeck()
    wait(1)
    for _, object in ipairs(zoneActionDeck.getObjects()) do
        if object.tag == "Deck" then object.shuffle() end
    end
    return 1
end

-- Coroutine timer
function wait(time)
    local start = os.time()
    -- each frame the coroutine is given control back, but we immediately call coroutine.yield() again until we exceed the time limit
    repeat coroutine.yield(0) until os.time() > start + time
end

function loadCharacter(characterName)
    cleanCharSelectionMat()
    firstName = string.match(characterName, "(.-) ")
    for _, card in ipairs(bagCharacter.getObjects()) do
        local name = card.name
        if name == characterName then
            bagCharacter.takeObject({
                position = {pos.x - 1.2, pos.y + 1, pos.z - 2.76},
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
        elseif card.name == "Skills " .. characterName then
            bagCharacter.takeObject({
                position = {pos.x + 1.2, pos.y + 1, pos.z - 2.76},
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
        elseif card.name == "Skills " .. firstName then
            bagCharacter.takeObject({
                position = {pos.x + 1.2, pos.y + 1, pos.z - 2.76},
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
        end
    end

    figurinesFound = 0
    for _, object in ipairs(bagFigurines.getObjects()) do
        local name = object.name
        if name == characterName then
            Wait.time(function()
                bagFigurines.takeObject({
                    position = {
                        pos.x - 2.6 + 3 * figurinesFound, pos.y + 1,
                        pos.z - 0.13
                    },
                    rotation = {0, 180, 0},
                    guid = object.guid
                })
                figurinesFound = figurinesFound + 1
            end, 0.5, 0)
        end
    end
end

function loadFerdinand()
    playerName = "Ferdinand"
    loadCharacter("Ferdinand Lachapelliere")
end

function loadAmelia()
    playerName = "Amelia"
    loadCharacter("Amelia Earhart")
end

function loadPhileas()
    playerName = "Phileas"
    loadCharacter("Phileas Fogg")
end

function loadAnjika()
    playerName = "Anjika"
    loadCharacter("Anjika Patel")
end

function loadVictor()
    playerName = "Victor"
    loadCharacter("Victor Frankenstein")
end

function loadDimitri()
    playerName = "Dimitri"
    loadCharacter("Dimitri Gorchkov")
end

function loadLovecraft()
    playerName = "Howard"
    loadCharacter("Howard P. Lovecraft")
end

function loadKeelan()
    playerName = "Keelan"
    loadCharacter("Keelan McCluskey")
end

function loadEliot()
    playerName = "Eliot"
    loadCharacter("Eliot Pendleton")
end

function loadMary()
    playerName = "Mary"
    loadCharacter("Mary Kingsley")
end

function loadSinglePlayer()
    playerName = "Player"
    loadCharacter("Single Player")
    Global.setVar('NUM_PLAYERS', 1)
    Global.call('loadPlayerMats', true)
end

function loadTwoPlayers()
    Global.setVar('NUM_PLAYERS', 2)
    Global.call('loadPlayerMats', true)
end

function loadThreePlayers()
    Global.setVar('NUM_PLAYERS', 3)
    Global.call('loadPlayerMats', true)
end

function loadFourPlayers()
    Global.setVar('NUM_PLAYERS', 4)
    Global.call('loadPlayerMats', true)
end

function setupPlayerOne()
    local PLAYER_NAME = Global.getTable('PLAYER_NAME')
    PLAYER_NAME[1] = playerName
    Global.setTable('PLAYER_NAME', PLAYER_NAME)
    sendActionDeck()
    sendPlayerCard(player1CharPos, -3)
end

function setupPlayerTwo()
    if Global.getVar('NUM_PLAYERS') > 1 then
        local PLAYER_NAME = Global.getTable('PLAYER_NAME')
        PLAYER_NAME[2] = playerName
        Global.setTable('PLAYER_NAME', PLAYER_NAME)
        sendActionDeck()
        sendPlayerCard(player2CharPos, -1)
    else
        printToAll("Number of players is too low: " ..
                       Global.getVar('NUM_PLAYERS') .. ".")
    end
end

function setupPlayerThree()
    if Global.getVar('NUM_PLAYERS') > 2 then
        local PLAYER_NAME = Global.getTable('PLAYER_NAME')
        PLAYER_NAME[3] = playerName
        Global.setTable('PLAYER_NAME', PLAYER_NAME)
        sendActionDeck()
        sendPlayerCard(player3CharPos, 1)
    else
        printToAll("Number of players is too low: " ..
                       Global.getVar('NUM_PLAYERS') .. ".")
    end
end

function setupPlayerFour()
    if Global.getVar('NUM_PLAYERS') > 3 then
        local PLAYER_NAME = Global.getTable('PLAYER_NAME')
        PLAYER_NAME[4] = playerName
        Global.setTable('PLAYER_NAME', PLAYER_NAME)
        sendActionDeck()
        sendPlayerCard(player4CharPos, 3)
    else
        printToAll("Number of players is too low: " ..
                       Global.getVar('NUM_PLAYERS') .. ".")
    end
end

function sendPlayerCard(playerCharPos, posOffset)
    local char_is_selected = false
    local multipleFigurinesPosOffset = 0
    for _, object in ipairs(charSelectionMat.getObjects()) do
        if object.name == "Card" then
            object.setPosition(playerCharPos)
            char_is_selected = true
        end
        if (object.name == "Figurine_Custom") or (object.name == "Custom_Model") then
            object.setPosition({
                1.2 + posOffset, 7, 5 - multipleFigurinesPosOffset
            })
            multipleFigurinesPosOffset = multipleFigurinesPosOffset + 1
        end
    end
    if not char_is_selected then
        printToAll("You must choose a character before setting it up.")
    end

end

function cleanCharSelectionMat()
    for _, object in ipairs(charSelectionMat.getObjects()) do
        if object.name == "Card" or object.name == "Deck" or object.name ==
            "Custom_Model" or object.name == "Figurine_Custom" then
            object.destroy()
        end
    end
end
