function onLoad()
    self.createButton({
        click_function = "finishSetup",
        function_owner = self,
        tooltip = "Click here to finish setting up expansions and curses",
        position = {0, 0, 0},
        scale = {35, 1, 8},
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })
end

function finishSetup() Global.call('finishSetup', {self.guid}) end
