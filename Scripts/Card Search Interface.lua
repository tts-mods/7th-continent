function onLoad()
    createInput()
    createButton()
    zoneAdvDeck = getObjectFromGUID(Global.getTable('ZONES').ZONE_ADVENTURE_DECK
                                        .guid)
    zoneRight = getObjectFromGUID(Global.getTable('ZONES').ZONE_DRAW.guid)
    zoneLeft = getObjectFromGUID(Global.getTable('ZONES').ZONE_SEARCH_LEFT.guid)
    zoneGameboard = getObjectFromGUID(Global.getTable('ZONES').ZONE_MAP.guid)
    zonePast = getObjectFromGUID(Global.getTable('ZONES').ZONE_PAST.guid)
end

function setSearchValue() self.editInput({index = 0, value = ''}); end

icard = 0

function sortExploreDeck()
    local off = (math.floor(icard / 50) * 7) - 9 * 5

    if icard < 10 then
        if not getNumberedCardHelper('00' .. tostring(icard), {off, 25, 25},
                                     false, true, false) then
            icard = icard + 1;
        end
    elseif icard < 100 then
        if not getNumberedCardHelper('0' .. tostring(icard), {off, 25, 25},
                                     false, true, false) then
            icard = icard + 1;
        end
    else
        if not getNumberedCardHelper(tostring(icard), {off, 25, 25}, false,
                                     true, false) then icard = icard + 1; end
    end
end

function inputFn(obj, playerClickerColor, inputValue, is_selected)
    local reset = false
    local len = string.len(inputValue);
    for i = 1, len do
        local c = inputValue:byte(i)
        if c == 10 and len == 1 then
            Wait.frames(setSearchValue, 1);
            return
        end

        local sort = true
        if i == 1 and c ~= 115 then -- s
            sort = false
        end
        if i == 2 and c ~= 111 then -- o
            sort = false
        end
        if i == 3 and c ~= 114 then -- r
            sort = false
        end
        if i == 4 and c ~= 116 then -- t
            sort = false
        end

        if i == 4 and sort then -- sort
            icard = 0
            Wait.frames(setSearchValue, 1);
            Wait.time(sortExploreDeck, 0.1, 2000)
            return;
        end

        if not sort and (c < 48 or c > 57) then
            reset = true
            break
        end
    end
    if reset then
        getNumberedCardHelper(inputValue:sub(1, #inputValue - 1), {0, 0, 0},
                              false, false, false);
        Wait.frames(setSearchValue, 1);
    end
end

function createInput()
    searchInput = self.createInput({
        input_function = "inputFn",
        label = "Type in Number",
        function_owner = self,
        alignment = 3,
        position = {3.34, 0.8, 4.7},
        scale = {0.4, 0.5, 0.8},
        width = 4500,
        height = 500,
        font_size = 450,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100},
        value = ""
    })
end

function dud() end

function createButton()
    self.createButton({
        click_function = "getWhiteNumberedCard",
        function_owner = self,
        label = "",
        position = {3.3, 0.6, 6.8},
        scale = {0.4, 0.5, 0.8},
        width = 5000,
        height = 400,
        font_size = 475,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "getNumberedCard",
        function_owner = self,
        label = "",
        position = {3.3, 0.6, 5.7},
        width = 4500,
        scale = {0.4, 0.5, 1},
        height = 350,
        font_size = 475,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "returnCards",
        function_owner = self,
        label = "",
        position = {3.2, 0.6, -4.1},
        scale = {0.4, 0.5, 0.8},
        width = 4500,
        height = 350,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "banishCards",
        function_owner = self,
        label = "",
        position = {-3.2, 0.6, 5.6},
        scale = {0.4, 0.5, 0.8},
        width = 3000,
        height = 400,
        font_size = 450,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "discardCards",
        function_owner = self,
        label = "",
        position = {-3.2, 0.6, 6.7},
        scale = {0.4, 0.5, 0.8},
        width = 5300,
        height = 400,
        font_size = 450,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "placeCenter",
        function_owner = self,
        label = "",
        position = {-2.8, 0.6, -4.2},
        scale = {0.4, 0.5, 0.8},
        width = 5000,
        height = 400,
        font_size = 475,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })
end

function getWhiteNumberedCard()
    local inputs = self.getInputs()
    for _, v in ipairs(inputs) do
        searchParam = v.value
        break
    end

    getNumberedCardHelper(searchParam, {0, 0, 0}, true, false, true)
end

function getNumberedCard()
    local inputs = self.getInputs()
    for _, v in ipairs(inputs) do
        searchParam = v.value
        break
    end

    getNumberedCardHelper(searchParam, {0, 0, 0}, false, false, true)
end

function getNumberedCardHelper(searchParam, offset, is_white_search, is_sorting,
                               is_verbose)
    local pos = self.getPosition() -- {3.26, 1.49, -19.12}

    if is_white_search then
        printToAll("Searching for '" .. searchParam .. "' with white back.")
    else
        printToAll("Searching for '" .. searchParam .. "'.")
    end

    local card_is_in_play = false
    hasGreen = false
    deck = nil
    lastCard = nil
    for _, object in ipairs(zoneAdvDeck.getObjects()) do
        if object.tag == "Deck" then
            deck = object
            if not is_sorting then deck.shuffle() end
            lastCard = nil
            break
        elseif object.tag == "Card" then
            lastCard = object
        end
    end

    if is_white_search then
        -- just look for white cards
        if deck then
            for _, card in ipairs(deck.getObjects()) do
                if card.name == searchParam and
                    string.find(card.description, "white") then
                    deck.takeObject({
                        position = {
                            pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                            pos.z - 0.5 + offset[3]
                        },
                        guid = card.guid
                    })
                    return true
                end
            end
        elseif lastCard then
            if lastCard.getName() == searchParam and
                string.find(lastCard.getDescription(), "white") then
                lastCard.setPosition({
                    pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                    pos.z - 0.5 + offset[3]
                })
                return true
            end
        end

        if not is_sorting then
            for _, object in ipairs(zoneGameboard.getObjects()) do
                if object.tag == "Card" then
                    if object.getName() == searchParam and
                        string.find(object.getDescription(), "white") then
                        if is_verbose then
                            printToAll("Card '" .. searchParam ..
                                           "' with white back is already in play.")
                        end
                        return true
                    end
                end
            end

            for _, object in ipairs(zonePast.getObjects()) do
                if object.tag == "Card" then
                    if object.getName() == searchParam and
                        string.find(object.getDescription(), "white") then
                        if is_verbose then
                            printToAll("Card '" .. searchParam ..
                                           "' with white back is in the Past. Return all cards in the Past and try this search again.")
                        end
                        return true
                    end
                elseif object.tag == "Deck" then
                    for _, card in ipairs(object.getObjects()) do
                        if card.name == searchParam and
                            string.find(card.description, "white") then
                            if is_verbose then
                                printToAll(
                                    "Card '" .. searchParam ..
                                        "' with white back is in the Past. Return all cards in the Past and try this search again.")
                            end
                            return true
                        end
                    end
                end
            end
        end

        if not deck then
            printToAll('No adventure deck found.')
            return false
        else
            printToAll("Card '" .. searchParam .. "' with white back not found.")
            return false
        end
    else
        -- look for green first, then if no green, take the gold
        if deck then
            for _, card in ipairs(deck.getObjects()) do
                if card.name == searchParam and
                    string.find(card.description, "green") then
                    local placedCard = deck.takeObject({
                        position = {
                            pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                            pos.z - 0.5 + offset[3]
                        },
                        guid = card.guid
                    })
                    Global.call('createSpecialActionButtons', placedCard)
                    return true
                end
            end
        elseif lastCard then
            if lastCard.getName() == searchParam and
                string.find(lastCard.getDescription(), "green") then
                lastCard.setPosition({
                    pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                    pos.z - 0.5 + offset[3]
                })
                Global.call('createSpecialActionButtons', lastCard)
                return true
            end
        end

        green_card_is_in_past = false
        if not is_sorting then
            for _, object in ipairs(zoneGameboard.getObjects()) do
                if object.tag == "Card" then
                    if object.getName() == searchParam and
                        string.find(object.getDescription(), "green") then
                        card_is_in_play = true
                        break
                    end
                end
            end

            for _, object in ipairs(zonePast.getObjects()) do
                if object.tag == "Card" then
                    if object.getName() == searchParam and
                        string.find(object.getDescription(), "green") then
                        green_card_is_in_past = true
                        break
                    end
                elseif object.tag == "Deck" then
                    for _, card in ipairs(object.getObjects()) do
                        if card.name == searchParam and
                            string.find(card.description, "green") then
                            green_card_is_in_past = true
                            break
                        end
                    end

                    if green_card_is_in_past then break end
                end
            end
        end

        if deck then
            for _, card in ipairs(deck.getObjects()) do
                local desc = card.description
                if card.name == searchParam and string.find(desc, "gold") then
                    local placedCard = deck.takeObject({
                        position = {
                            pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                            pos.z - 0.5 + offset[3]
                        },
                        guid = card.guid
                    })
                    Global.call('createSpecialActionButtons', placedCard)
                    return true
                end
            end

            if is_sorting then
                -- also do white cards when sorting
                for _, card in ipairs(deck.getObjects()) do
                    if card.name == searchParam and
                        string.find(card.description, "white") then
                        local placedCard = deck.takeObject({
                            position = {
                                pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                                pos.z - 0.5 + offset[3]
                            },
                            guid = card.guid
                        })
                        Global.call('createSpecialActionButtons', placedCard)
                        return true
                    end
                end
            end
        elseif lastCard then
            if lastCard.getName() == searchParam and
                (string.find(lastCard.getDescription(), "gold") or
                    (is_sorting and
                        string.find(lastCard.getDescription(), "white"))) then
                lastCard.setPosition({
                    pos.x + 2.22 + offset[1], pos.y + 2 + offset[2],
                    pos.z - 0.5 + offset[3]
                })
                return true
            end
        end

        if not is_sorting then
            for _, object in ipairs(zoneGameboard.getObjects()) do
                if object.tag == "Card" then
                    if object.getName() == searchParam and
                        string.find(object.getDescription(), "gold") then
                        if is_verbose then
                            printToAll("Card '" .. searchParam ..
                                           "' with gold back is already in play.")
                        end
                        return true
                    end
                end
            end

            for _, object in ipairs(zonePast.getObjects()) do
                if object.tag == "Card" then
                    if object.getName() == searchParam and
                        string.find(object.getDescription(), "gold") then
                        if is_verbose then
                            printToAll("Card '" .. searchParam ..
                                           "' with green and/or gold back is in the Past. Return all cards in the Past and try this search again.")
                        end
                        return true
                    end
                elseif object.tag == "Deck" then
                    for _, card in ipairs(object.getObjects()) do
                        if card.name == searchParam and
                            string.find(card.description, "gold") then
                            if is_verbose then
                                printToAll(
                                    "Card '" .. searchParam ..
                                        "' with green and/or gold back is in the Past. Return all cards in the Past and try this search again.")
                            end
                            return true
                        end
                    end
                end
            end
        end

        if green_card_is_in_past then
            if is_verbose then
                printToAll("Card '" .. searchParam ..
                               "' with green and/or gold back is in the Past. Return all cards in the Past and try this search again.")
            end
            return true
        elseif card_is_in_play then
            if is_verbose then
                printToAll("Card '" .. searchParam ..
                               "' with green back is already in play.")
            end
            return true
        elseif not deck then
            printToAll('No adventure deck found.')
            return false
        else
            printToAll("Card '" .. searchParam ..
                           "' with green or gold back not found.")
            return false
        end
    end
end

function returnCards() Global.call('returnCards', {zoneRight, false, {}}) end

function discardCards() Global.call('discardCards', {zoneLeft, {}}) end

function banishCards() banishCardsHelper() end

function banishCardsHelper()
    local cardsToBanish = zoneLeft.getObjects()

    if #cardsToBanish > 0 then
        for _, object in ipairs(cardsToBanish) do
            if object.tag == "Deck" or object.tag == "Card" then
                banishNextCard(object)
                Wait.time(banishCardsHelper, 0.1, 0)
                return
            end
        end
    end
end

banishPilePos = {4.69, 5, -49.77}
function banishNextCard(cardOrDeck)
    if not cardOrDeck or
        not (cardOrDeck.tag == "Deck" or cardOrDeck.tag == "Card") then
        -- Don't think it's actually possible to get here but just in case
        return
    elseif cardOrDeck.tag == "Deck" then
        for _, card in ipairs(cardOrDeck.getObjects()) do
            cardOrDeck.takeObject({
                position = banishPilePos,
                rotation = getBanishCardRot(card.description),
                guid = card.guid
            })
            break
        end
    else
        cardOrDeck.setRotation(getBanishCardRot(cardOrDeck.getDescription()))
        cardOrDeck.setPosition(banishPilePos)
    end
end

function getBanishCardRot(desc)
    if string.find(desc, 'green') or string.find(desc, 'gold') or
        string.find(desc, 'white') then
        return {x = 0, y = 180, z = 0}
    else
        return {x = 0, y = 180, z = 180}
    end
end

function placeCenter(obj, playerClickerColor)
    for _, object in ipairs(zoneRight.getObjects()) do
        if object.tag == "Card" then
            object.setRotation({0, 180, 0})
            object.setPosition({0, 5, 9.4})
            Player[playerClickerColor].lookAt({
                position = {x = 0, y = 0, z = 9.4},
                pitch = 70,
                distance = 40
            })
            break
        elseif object.tag == "Deck" then
            printToAll(
                'Please place only one card on the right page to send to the center of the board.')
        end
    end
end
