function onLoad()
    createButtons()
    bag_rules = getObjectFromGUID('abc131')
    pos = {-78.31, 10.87, -43.58}
    zone = getObjectFromGUID('f5ba77')

end

function createButtons()
    self.createButton({
        click_function = "display_3",
        function_owner = self,
        label = "",
        position = {0, 0.6, -5.4},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_2",
        function_owner = self,
        label = "",
        position = {0, 0.6, -4.9},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_4",
        function_owner = self,
        label = "",
        position = {0, 0.6, -4.35},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_8",
        function_owner = self,
        label = "",
        position = {0, 0.6, -3.8},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_9",
        function_owner = self,
        label = "",
        position = {0, 0.6, -3.2},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_10",
        function_owner = self,
        label = "",
        position = {0, 0.6, -2.15},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_11",
        function_owner = self,
        label = "",
        position = {0, 0.6, -1.6},
        width = 3200,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_14",
        function_owner = self,
        label = "",
        position = {0, 0.6, -1.05},
        width = 5500,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_17",
        function_owner = self,
        label = "",
        position = {0, 0.6, -0.48},
        width = 5500,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_18",
        function_owner = self,
        label = "",
        position = {0, 0.6, 0.09},
        width = 5500,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_19",
        function_owner = self,
        label = "",
        position = {0, 0.6, 1.25},
        width = 5500,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_19",
        function_owner = self,
        label = "",
        position = {0, 0.6, 1.75},
        width = 2000,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_21",
        function_owner = self,
        label = "",
        position = {0, 0.6, 2.33},
        width = 2900,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_21",
        function_owner = self,
        label = "",
        position = {0, 0.6, 2.83},
        width = 3100,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_22",
        function_owner = self,
        label = "",
        position = {0, 0.6, 3.43},
        width = 3100,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_22",
        function_owner = self,
        label = "",
        position = {0, 0.6, 4.53},
        width = 5800,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_22",
        function_owner = self,
        label = "",
        position = {0, 0.6, 5.13},
        width = 4000,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_23",
        function_owner = self,
        label = "",
        position = {0, 0.6, 5.73},
        width = 2500,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_23",
        function_owner = self,
        label = "",
        position = {0, 0.6, 6.3},
        width = 2500,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "display_24",
        function_owner = self,
        label = "",
        position = {0, 0.6, 6.82},
        width = 1600,
        height = 225,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })
end

function display_1()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 1" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_2()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 2" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_3()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 3" then
            print("found page 3 " .. pos[1] .. ',' .. pos[2] .. ',' .. pos[3])
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_4()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 4" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_5()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 5" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_6()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 6" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_7()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 7" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_8()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 8" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_9()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 9" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_10()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 10" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_11()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 11" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_12()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 12" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_13()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 13" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_14()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 14" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_15()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 15" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_16()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 16" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})

        end
    end
end

function display_17()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 17" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_18()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 18" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_19()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 19" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_20()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 20" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_21()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 21" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_22()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 22" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_23()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 23" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function display_24()
    remove_current_page()
    for _, card in ipairs(bag_rules.getObjects()) do
        local name = card.name
        if name == "Page 24" then
            c = bag_rules.takeObject({
                position = pos,
                rotation = {0, 180, 0},
                guid = card.guid,
                smooth = false
            })
            c.setScale({4, 1, 4})
        end
    end
end

function remove_current_page()
    for _, object in ipairs(zone.getObjects()) do
        if object.tag == "Card" or object.tag == "Deck" then
            object.destroy()
        end
    end
end
