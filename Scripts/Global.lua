ZONES = {
    ZONE_P1 = {guid = '00cc10'},
    ZONE_P2 = {guid = '18c2b9'},
    ZONE_P3 = {guid = '3ab02f'},
    ZONE_P4 = {guid = '7eb05d'},
    ZONE_MAP = {guid = 'ed36c3'},
    ZONE_ACTION = {guid = '2d4766'},
    ZONE_PAST = {guid = 'baf325'},
    ZONE_SEARCH_LEFT = {guid = '3c7915'},
    ZONE_DRAW = {guid = '7d9d2f'},
    ZONE_EXPLORE = {guid = '949c59'},
    ZONE_MATS = {guid = 'ad3637'},
    ZONE_SEARCH = {guid = '4b85b2'},
    ZONE_CHOOSE_CHARACTER = {guid = {'6bf0a4', 'e70a32'}},
    ZONE_ADVENTURE_DECK = {guid = 'c2e68a'},
    ZONE_ACTION_DECK = {guid = 'c973e5'},
    ZONE_ACTION_DISCARD = {guid = 'aaa917'},
    ZONE_SATCHEL = {guid = '7b7298'},
    ZONE_BANISH = {guid = '323550'}
}

playerRedStatePos = {
    {x = -21.7, y = 4, z = -50.6}, {x = 12.6, y = 4, z = -50.6},
    {x = -40, y = 4, z = -50.6}, {x = 30.9, y = 4, z = -50.6}
}

STATE_CARDS = {
    drunk = "092",
    [092] = "drunk",
    paranoid = "100",
    [100] = "paranoid",
    tired = "101",
    [101] = "tired",
    freezing = "102",
    [102] = "freezing",
    frightened = "103",
    [103] = "frightened",
    injured = "104",
    [104] = "injured",
    bloody = "105",
    [105] = "bloody",
    hungry = "106",
    [106] = "hungry",
    terrified = "107",
    [107] = "terrified",
    poisoned = "108",
    [108] = "poisoned",
    nauseated = "134",
    [134] = "nauseated",
    possessed = "586",
    [586] = "possessed",
    deaf = "612",
    [612] = "deaf",
    disheartened = "750",
    [750] = "disheartened",
    blinded = "800",
    [800] = "blinded",
    ["6075c6"] = "leech"
}

EXPLORE_ZONES = {
    ZONE1 = {guid = '32593b'},
    ZONE2 = {guid = 'd23dc2'},
    ZONE3 = {guid = 'cd9f70'},
    ZONE4 = {guid = '072ac3'},
    ZONE5 = {guid = 'c97f0e'},
    ZONE6 = {guid = '6a7df4'},
    ZONE7 = {guid = '55b1e6'},
    ZONE8 = {guid = '7d49eb'},
    ZONE9 = {guid = '147300'},
    ZONE10 = {guid = 'e6ca46'},
    ZONE11 = {guid = '96ff2e'},
    ZONE12 = {guid = 'ddd7b9'},
    ZONEC = {guid = '9d350f'}
}

EXPLORATION_DECKS_POSITIONS = {
    ONE = {-19.08, 5, -16.62},
    TWO = {-15.91, 5, -16.62},
    THREE = {-12.68, 5, -16.62},
    FOUR = {-9.48, 5, -16.62},
    FIVE = {-6.27, 5, -16.62},
    SIX = {-3.11, 5, -16.62},
    SEVEN = {0.06, 5, -16.62},
    EIGHT = {3.24, 5, -16.62},
    NINE = {6.43, 5, -16.62},
    TEN = {9.59, 5, -16.62},
    ELEVEN = {12.75, 5, -16.62},
    TWELVE = {15.91, 5, -16.62},
    CRYSTAL = {19.07, 5, -16.62}
}
ADVENTURE_DECK_POS = {0.01, 12, -55.65}
ACTION_DECK_POS = {-4.91, 5, -41.52}
ACTION_DISCARD_POS = {4.87, 4, -41.52}
PAST_POS = {-4.71, 5, -49.77}
ADVANCED_SKILL_DECK_POS = {9.28, 5, -21.89}
EXPANSION_TOKENS_POS = {12.36, 3, -21.91}
SATCHEL_POS = {0.01, 3, -49.00}
BANISH_DECK_POS = {x = 4.69, y = 5, z = -49.77}
PAST_DECK_POS = {x = -4.71, y = 5, z = -49.77}
WEATHER_POS = {x = -4.92, y = 3, z = -45.56}

RESET_BOARD_GUID = "547abe"
BAG_EXPANSIONS_GUID = '6dd285'
BAG_WGUMCD_TOKENS = '96b767'
BAG_FACING_THE_ELEMENTS_TOKENS = '0ad451'
BAG_FLYING_ROOTS_TOKENS = 'ad4dea'
BAG_FEAR_THE_DEVOURERS_TOKENS = '04ce4d'
BAG_FIRE_TOKENS = '48a10d'
ADVANCED_SKILL_DECK = '8b5253'
EXPANSIONS_AND_CURSES = {
    vg = {"d1f9a9", "8fe41e", "66a4fd"},
    dark_chest = {"aeb566", "14fc68", "501fd7"},
    bloody_hunt = {"4afff9", "e92147", "fc9a43"},
    offering = {"5c711f", "24a404", "e7e650"},
    icy_maze_ep = {"ab60cd"},
    icy_maze = {"aa10ac", "ab60cd", "68164a", "54c729"},
    sanctuary_ep = {"91d804"},
    sanctuary = {"314b5f", "ecc109", "e3e5e7", "91d804"},
    swamp_ep = {"167058"},
    swamp = {"61da45", "578eb7", "458d6e", "167058"},
    repentance = {"85962a", "8bd3d5", "910d89"},
    devourers = {"16e894", "be3a68"},
    elements = {"78d301", "5b1d34"},
    crystals_song_pnp = {"87e3be", "3247d7", "9d0751"},
    roots = {"a1cf58", "5810ee"},
    creatures = {"015b19", "80d6c0"},
    goes_up = {"c0fcb3"},
    prison = {"cf5b18", "fc593d", "403d44"},
    armageddon = {"6f9315", "9d7b7e", "ea7add"},
    beacon = {"723c5c", "456d94", "b99a8b"},
    veins = {"b63b53", "3046ea", "efc1d2"},
    crystals_song = {"df03c9", "452e21", "be8b60"}
}
EXPANSIONS_AND_CURSES_NAMES = {
    vg = "The Voracious Goddess",
    dark_chest = "The Dark Chest of the Damned",
    bloody_hunt = "The Bloody Hunt",
    offering = "An Offering to the Guardians",
    icy_maze_ep = "The Icy Maze",
    icy_maze = "The Icy Maze",
    sanctuary_ep = "The Forbidden Sanctuary",
    sanctuary = "The Forbidden Sanctuary",
    swamp_ep = "Swamp of Madness",
    swamp = "Swamp of Madness",
    repentance = "Path of Repentance",
    devourers = "Fear The Devourers",
    elements = "Facing the Elements",
    crystals_song_pnp = "The Crystal's Song",
    roots = "The Flying Roots",
    creatures = "Comfort Creatures",
    goes_up = "What Goes Up Must Come Down",
    prison = "A Prison of Clouds",
    armageddon = "Armageddon",
    beacon = "A Beacon in the Night",
    veins = "The Veins of the Earth",
    crystals_song = "The Crystal's Song"
}
NON_CURSE_EXPANSIONS = {
    "repentance", "devourers", "elements", "roots", "creatures"
}
WHAT_GOES_UP_CURSES = {
    "goes_up", "prison", "armageddon", "beacon", "veins", "crystals_song"
}
DELETED_CRYSTALS_PNP = false
DELETED_WHAT_GOES_UP = false
PICKED_CRYSTALS_SONG = false
FINISH_SETUP_BUTTON_GUID = '657e12'
FINISH_SETUP_BUTTON_AVAILABLE = false

SPECIAL_Z136_GUID = '27eff3' -- one of the 136 cards doesn't have a west direction
TERRAIN_CARD_CONNECTIONS = {
    -- n, s, e, w
    z004 = {10, 35, 8, 6},
    z004w = {720, 0, 0, 0},
    z006 = {9, 12, 4, 0},
    z007 = {18, 10, 11, 0},
    z009 = {34, 6, 10, 15},
    z010 = {7, 0, 5, 9},
    z010g = {7, 4, 0, 9},
    z014 = {9, 12, 4, 16},
    z015 = {0, 0, 9, 17},
    z017 = {164, 39, 15, 22},
    z017g = {164, 39, 15, 0},
    z024 = {164, 39, 15, 37},
    z038 = {54, 36, 0, 53},
    z043 = {253, 256, 287, 316},
    z048 = {0, 41, 90, 68},
    z054 = {59, 38, 132, 72},
    z056 = {0, 0, 149, 125},
    z057 = {122, 135, 136, 59},
    z059 = {98, 54, 66, 86},
    z059g = {63, 54, 66, 86},
    z066 = {122, 0, 136, 59},
    z067 = {0, 0, 87, 83},
    z072 = {86, 0, 54, 0},
    z076 = {0, 0, 87, 83},
    z083 = {0, 60, 67, 64},
    z083g = {0, 0, 67, 910},
    z086 = {0, 72, 59, 112},
    z087 = {86, 0, 54, 44},
    z088 = {0, 0, 125, 120},
    z094 = {111, 0, 119, 131},
    z095 = {235, 156, 65, 48},
    z111 = {0, 94, 133, 136},
    z112 = {140, 51, 86, 0},
    z120 = {0, 115, 88, 175},
    z122 = {238, 66, 142, 0},
    z123 = {220, 163, 191, 161},
    z123w = {722, 680, 729, 692},
    z125 = {46, 47, 56, 88},
    z131 = {136, 71, 94, 132},
    z132 = {0, 74, 131, 54},
    z132w = {0, 0, 768, 675},
    z136 = {142, 131, 111, 66},
    z140 = {183, 112, 231, 151},
    z141 = {214, 191, 271, 220},
    z142 = {154, 136, 137, 122},
    z143 = {191, 159, 0, 163},
    z149 = {268, 0, 241, 56},
    z151 = {0, 0, 140, 116},
    z151g = {0, 0, 140, 602},
    z154 = {163, 142, 0, 0},
    z161 = {0, 206, 123, 0},
    z163 = {123, 154, 143, 0},
    z168 = {443, 386, 0, 0},
    z175 = {94, 113, 120, 0},
    z179 = {0, 93, 140, 116},
    z182 = {288, 326, 0, 240},
    z183 = {0, 140, 196, 0},
    z184 = {426, 383, 397, 446},
    z186 = {235, 121, 65, 48},
    z187 = {281, 196, 155, 208},
    z191 = {264, 143, 202, 123},
    z191g = {264, 143, 202, 123},
    z196 = {205, 0, 167, 183},
    z198 = {278, 0, 127, 214},
    z203 = {252, 161, 220, 304},
    z205 = {281, 196, 155, 0},
    z205g = {0, 196, 862, 208},
    z208 = {284, 0, 205, 218},
    z214 = {0, 264, 198, 0},
    z218 = {0, 165, 208, 237},
    z220 = {177, 123, 264, 203},
    z241 = {312, 158, 225, 149},
    z252 = {259, 203, 0, 0},
    z252g = {259, 203, 0, 0},
    z253 = {326, 43, 0, 0},
    z254 = {82, 0, 149, 125},
    z258 = {190, 390, 0, 236},
    z258g = {302, 390, 0, 245},
    z259 = {306, 252, 290, 341},
    z259g = {306, 252, 290, 341},
    z262 = {319, 281, 304, 091},
    z264 = {0, 191, 271, 220},
    z271 = {91, 0, 288, 264},
    z281 = {262, 205, 192, 284},
    z284 = {289, 208, 281, 255},
    z284w = {0, 736, 692, 731},
    z288 = {0, 313, 301, 271},
    z288g = {0, 0, 887, 271},
    z290 = {0, 0, 346, 259},
    z302g = {0, 258, 632, 618},
    z303 = {369, 214, 0, 290},
    z304 = {0, 0, 203, 262},
    z306 = {334, 259, 0, 0},
    z307 = {0, 0, 564, 517},
    z311 = {262, 205, 192, 284},
    z312 = {263, 241, 438, 0},
    z313 = {288, 326, 0, 0},
    z319 = {328, 262, 0, 294},
    z320 = {331, 0, 346, 256},
    z321 = {338, 342, 371, 0},
    z326 = {313, 253, 339, 0},
    z326w = {0, 676, 0, 756},
    z328 = {0, 319, 341, 0},
    z331 = {364, 320, 0, 0},
    z334 = {352, 306, 364, 361},
    z334w = {0, 0, 708, 779},
    z338 = {276, 321, 387, 374},
    z339 = {0, 239, 348, 326},
    z341 = {309, 285, 259, 328},
    z346 = {369, 0, 0, 290},
    z348 = {365, 279, 330, 339},
    z355 = {459, 364, 0, 0},
    z361 = {429, 0, 334, 389},
    z361g = {845, 0, 334, 389},
    z364 = {345, 91, 217, 334},
    z365 = {0, 348, 356, 0},
    z365g = {322, 348, 356, 0},
    z368 = {380, 446, 426, 293},
    z374 = {427, 273, 338, 384},
    z376 = {0, 0, 446, 396},
    z377 = {434, 295, 0, 453},
    z382_c89dd8 = {760, 396, 0, 0},
    z387 = {415, 0, 439, 338},
    z389 = {226, 0, 361, 417},
    z390 = {258, 0, 284, 0},
    z395 = {0, 348, 356, 0},
    z396 = {382, 427, 416, 233},
    z397 = {0, 0, 403, 184},
    z402 = {282, 422, 418, 359},
    z415 = {446, 387, 0, 0},
    z418 = {0, 0, 374, 402},
    z423 = {451, 377, 0, 0},
    z423g = {0, 377, 0, 0},
    z426 = {329, 184, 0, 368},
    z427 = {396, 374, 0, 379},
    z428 = {0, 0, 374, 402},
    z442 = {435, 368, 0, 335},
    z446 = {368, 415, 184, 416},
    z446g = {391, 415, 184, 416},
    z449 = {0, 0, 0, 0},
    z452 = {419, 368, 0, 335},
    z459 = {501, 355, 0, 473},
    z475 = {449, 449, 449, 449},
    z478 = {449, 0, 449, 449},
    z483 = {0, 468, 485, 0},
    z485 = {460, 471, 492, 0},
    z494 = {456, 461, 0, 538},
    z497 = {499, 449, 499, 449},
    z499 = {0, 0, 0, 0},
    z504 = {577, 0, 595, 541},
    z504g = {577, 521, 595, 540},
    z511 = {0, 0, 564, 517},
    z514 = {0, 0, 557, 582},
    z514g = {0, 0, 546, 582},
    z517 = {0, 566, 544, 537},
    z522g = {546, 527, 0, 0},
    z527 = {522, 0, 577, 554},
    z533 = {0, 0, 588, 523},
    z536 = {0, 0, 493, 556},
    z539 = {588, 512, 0, 567},
    z542 = {0, 0, 588, 564},
    z546 = {502, 522, 591, 514},
    z546g = {502, 522, 0, 514},
    z560 = {726, 713, 0, 677},
    z564 = {519, 553, 579, 307},
    z575 = {592, 0, 0, 580},
    z577 = {524, 0, 0, 527},
    z577g = {0, 504, 0, 527},
    z580 = {0, 0, 551, 595},
    z587w = {731, 0, 736, 681},
    z588 = {507, 539, 535, 533},
    z595 = {532, 568, 548, 504},
    z602 = {621, 0, 151, 609},
    z609 = {616, 628, 602, 654},
    z610 = {640, 662, 0, 0},
    z613 = {655, 633, 0, 0},
    z618 = {596, 0, 302, 635},
    z622 = {664, 633, 0, 0},
    z626 = {665, 302, 639, 608},
    z628 = {609, 604, 645, 641},
    z631 = {607, 613, 619, 658},
    z633 = {613, 654, 0, 0},
    z635 = {662, 605, 618, 593},
    z640 = {649, 610, 643, 617},
    z641 = {654, 647, 628, 637},
    z642 = {613, 654, 0, 660},
    z654 = {633, 641, 609, 651},
    z656 = {626, 258, 632, 618},
    z659 = {611, 606, 0, 0},
    z659g = {0, 606, 0, 0},
    z662 = {610, 635, 0, 625},
    z664 = {607, 613, 619, 0},
    z673w = {791, 0, 710, 708},
    z675w = {736, 695, 132, 738},
    z676w = {326, 720, 0, 0},
    z677 = {718, 693, 704, 0},
    z677w = {708, 729, 0, 0},
    z680w = {123, 768, 756, 0},
    z684 = {844, 865, 599, 0},
    z692w = {0, 0, 123, 284},
    z694 = {735, 776, 755, 709},
    z702w = {734, 789, 0, 0},
    z704 = {726, 713, 0, 677},
    z708w = {0, 677, 673, 334},
    z710 = {774, 733, 0, 0},
    z710w = {0, 0, 0, 673},
    z719 = {264, 143, 202, 123},
    z720w = {676, 4, 0, 0},
    z729w = {677, 756, 0, 123},
    z731w = {0, 587, 284, 789},
    z735 = {709, 694, 771, 798},
    z735w = {738, 0, 0, 0},
    z738w = {587, 735, 675, 0},
    z745 = {848, 0, 778, 859},
    z752 = {0, 900, 865, 833},
    z755 = {771, 762, 0, 694},
    z756w = {729, 0, 326, 680},
    z758 = {0, 868, 879, 854},
    z760 = {773, 382, 0, 796},
    z763 = {724, 0, 869, 712},
    z768g = {680, 0, 0, 132},
    z773 = {782, 760, 711, 0},
    z774 = {769, 710, 793, 784},
    z778 = {759, 0, 0, 0},
    z778g = {883, 0, 803, 0},
    z782 = {799, 773, 0, 825},
    z789w = {702, 681, 731, 0},
    z793 = {0, 679, 788, 774},
    z798 = {743, 0, 735, 709},
    z803g = {822, 0, 855, 0},
    z833 = {0, 827, 752, 907},
    z833g = {904, 827, 752, 0},
    z844 = {697, 684, 842, 896},
    z851 = {830, 868, 866, 0},
    z855g = {812, 0, 872, 0},
    z860g = {745, 816, 761, 0},
    z865 = {684, 867, 894, 752},
    z866 = {810, 836, 871, 0},
    z871g = {859, 881, 0, 0},
    z872 = {0, 0, 893, 855},
    z876 = {859, 881, 884, 0},
    z879 = {859, 703, 860, 0},
    z881 = {0, 758, 794, 873},
    z893 = {691, 0, 896, 872},
    z896 = {672, 0, 844, 893},
    z901 = {898, 0, 891, 909},
    z902 = {903, 0, 0, 889},
    z904 = {0, 833, 839, 0}
}

ICY_MAZE_TERRAIN_CARD_CONNECTIONS = {
    zd73ad4 = {449, 449, 449, 449},
    z9dcb66 = {449, 449, 449, 449},
    z313ef5 = {449, 449, 449, 449},
    zd95686 = {449, 449, 449, 449},
    z51b75d = {449, 449, 449, 449},
    zee7794 = {449, 449, 449, 449},
    ze8996a = {449, 449, 449, 449},
    z3468fa = {449, 449, 449, 449},
    z38616d = {449, 449, 449, 449},
    z4398b8 = {449, 449, 449, 449},
    ze6acf1 = {449, 449, 449, 449},

    z96e86a = {449, 449, 499, 449},
    z6776a1 = {449, 449, 499, 449},
    z0dbc3a = {449, 449, 499, 449},

    z60a1b9 = {499, 449, 449, 449},
    z1096a9 = {499, 449, 449, 449},
    zd4b056 = {499, 449, 449, 449},

    z0ecb3e = {449, 449, 449, 499},
    z5d72f5 = {449, 499, 449, 449},
    z5d1f9a = {449, 0, 449, 449},
    z324307 = {499, 449, 499, 449}
}

-- pos is a 7x3 grid. H1 V1 is the top left corner. H1 V7 is the bottom left corner. H3 V7 is the bottom right corner
PERMANENT_EVENTS = {
    z004w = {{draw = "w701", balloonWhiteTerrainCard = "w004", pos = "H3 V3"}},
    z005 = {
        {fetch = "026", owner = "discard", pos = "V6"},
        {dealState = "104", pos = "V7"}
    },
    z008 = {
        {draw = "023", pos = "V3"},
        {draw = "023 + banner", banner = "raft", pos = "H3 V3"},
        {dealState = "101", pos = "H3 V4"}, {dealState = "102", pos = "H3 V5"},
        {rst = "017", pos = "H3 V6"}
    },
    z008_dadd73 = {{drawKeyword = "serenity", pos = "H1.5 V7"}},
    z011 = {{fetch = "031", owner = "return", pos = "H1 V3"}},
    z012 = {
        {draw = "030", pos = "V3", owner = "banish", fetch = "gold"},
        {draw = "2x030", pos = "V4", owner = "banish", fetch = "gold"},
        {draw = "3x030", pos = "V5", owner = "banish", fetch = "gold"}
    },
    z015 = {{dealState = "101", pos = "H2.5 V7"}},
    z018 = {{draw = "025", pos = "V5"}, {dealState = "102", pos = "H3 V7"}},
    z019 = {
        {owner = "satchel", draw = "2x001, 2x003", pos = "H1 V6"},
        {dealState = "104", pos = "H3 V7"}
    },
    z022 = {
        {fetch = "024", ret = "017", owner = "discard", pos = "H1 V5"},
        {fetch = "gold", owner = "banish", pos = "H1 V7"}
    },
    z031 = {
        {fetch = "139", owner = "discard", draw = "032", pos = "H1 V5"},
        {dealState = "102", pos = "H2 V7"}
    },
    z031_2af16f = {
        {fetch = "139", owner = "banish", draw = "002, 032", pos = "H2 V6"},
        {dealState = "104", pos = "H2 V7"}
    },
    z034 = {{draw = "029", pos = "V3"}},
    z035 = {{owner = "discard", pos = "H2 V7"}},
    z035_ddfa37 = {{drawKeyword = "stamina", pos = "H1 V2"}},
    z036 = {
        {draw = "084", pos = "V6"},
        {draw = "084 + banner", banner = "raft", pos = "H3 V6"},
        {dealState = "101", pos = "H3 V7"}
    },
    z037 = {
        {draw = "021", pos = "V7"},
        {draw = "021 + banner", banner = "gear1, gear2", pos = "H3 V7"}
    },
    z039 = {{draw = "666", pos = "V2"}},
    z041 = {
        {fetch = "gold", owner = "banish", pos = "H3 V3"},
        {dealActiveState = "101", pos = "H3 V6"}
    },
    z041g = {{draw = "666, 171", pos = "H1 V1"}},
    z044 = {
        {fetch = "076", owner = "discard", pos = "H3"},
        {fetch = "067", owner = "discard", pos = "H3 V6"}
    },
    z046 = {
        {draw = "189", pos = "H3 V2"},
        {draw = "189 + banner", banner = "raft", pos = "H3 V3"},
        {
            rst = "043",
            dealStateToAll = "102",
            dealActiveState = "104",
            pos = "H3 V6"
        }
    },
    z049 = {{owner = "discard", pos = "V6"}},
    z049_1a7b05 = {
        {dealState = "101", pos = "H1 V4"}, {dealState = "103", pos = "H1 V5"},
        {drawKeyword = "curse", pos = "H1 V7"}
    },
    z049_b2f919 = {{draw = "333", pos = "H1 V6.5"}},
    z053 = {{}},
    z053_db01ed = {{drawKeyword = "vigilance", pos = "H2 V3"}},
    z053_fd1bbc = {{fetch = "055", owner = "banish"}},
    z055 = {{draw = "350", pos = "V2"}},
    z060 = {
        {
            draw = "2x200",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x200",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "5x200",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z060g = {
        {draw = "1x300", pos = "V2.5", return_past_if_necessary = true},
        {draw = "2x300", pos = "V3.5", return_past_if_necessary = true},
        {draw = "3x300", pos = "V4.5", return_past_if_necessary = true},
        {draw = "4x300", pos = "V5.5", return_past_if_necessary = true}
    },
    z063 = {
        {draw = "073", pos = "H1 V3"},
        {draw = "073 + banner", banner = "sanctuary", pos = "V3"},
        {dealState = "105"}
    },
    z064 = {{}},
    z064_99e72d = {
        {owner = "discard", fetch = "078", pos = "H1 V5"},
        {owner = "return", dealState = "108", pos = "H1 V6.5"}
    },
    z064_441c4f = {
        {owner = "discard", fetch = "078", pos = "H1 V5"},
        {owner = "return", pos = "H1 V6.5"}
    },
    z065 = {
        {draw = "070", pos = "H2 V3"},
        {draw = "070 + banner", banner = "raft", pos = "H3 V3"},
        {draw = "173", dealStateToAll = "102", pos = "H3 V5"}
    },
    z068 = {
        {rst = "198", dealStateToAll = "102", pos = "H1 V5"},
        {draw = "173", dealStateToAll = "102", pos = "H1 V6"}
    },
    z088 = {
        {draw = "125 + banner", banner = "raft", pos = "H3 V5"},
        {dealState = "101", pos = "H3 V7"}
    },
    z071 = {{draw = "000", pos = "V7"}},
    z071_76d1e7 = {
        {owner = "discard", fetch = "071", pos = "H1.5 V7"},
        {owner = "discard", draw = "097", pos = "H2.5 V7"}
    },
    z074 = {
        {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z074g = {
        {draw = "2x250", pos = "V3", return_past_if_necessary = true},
        {draw = "3x250", pos = "V4", return_past_if_necessary = true},
        {draw = "5x250", pos = "V5", return_past_if_necessary = true}
    },
    z078 = {{draw = "117", pos = "V2"}, {draw = "069", pos = "H3 V3"}},
    z079 = {{drawKeyword = "vigilance", pos = "H2 V7"}},
    z082 = {{draw = "176", pos = "V5"}},
    z090 = {
        {fetch = "186", owner = "discard", pos = "V2"},
        {fetch = "095", owner = "discard", pos = "V3"}
    },
    z093 = {
        {fetch = "gold", owner = "banish", draw = "003, 350", pos = "H1 V3"},
        {dealActiveState = "101", pos = "H3 V3"}
    },
    z094 = {{draw = "119 + banner", banner = "torch", pos = "H2.5 V4"}},
    z098_2618b3 = {{fetch = "126", owner = "banish", pos = "H1 V3"}},
    z098 = {
        {draw = "153", pos = "H1"},
        {fetch = "059", banish = "059", pos = "H1 V5.5"},
        {fetch = "063", owner = "banish", pos = "H1.5 V6.5"},
        {fetch = "gold", owner = "banish", pos = "H1 V7"}
    },
    z098g = {
        {fetch = "059", banish = "059", pos = "H1 V3"},
        {fetch = "063", owner = "banish", pos = "H1 V4"},
        {dealState = "104", pos = "H1 V6"}
    },
    z113 = {
        {fetch = "124", owner = "banish", draw = "001", pos = "H3 V3"},
        {dealState = "104", pos = "H2 V5"}
    },
    z115 = {
        {
            draw = "2x200",
            fetch = "gold",
            owner = "banish",
            pos = "H1.5 V2.5",
            return_past_if_necessary = true
        }, {
            draw = "3x200",
            fetch = "gold",
            owner = "banish",
            pos = "H1.5 V3.5",
            return_past_if_necessary = true
        }, {
            draw = "4x200",
            fetch = "gold",
            owner = "banish",
            pos = "H1.5 V4.5",
            return_past_if_necessary = true
        }, {
            draw = "6x200",
            fetch = "gold",
            owner = "banish",
            pos = "H1.5 V5.5",
            return_past_if_necessary = true
        }
    },
    z115g = {
        {draw = "1x300", pos = "H1.5 V2.5", return_past_if_necessary = true},
        {draw = "2x300", pos = "H1.5 V3.5", return_past_if_necessary = true},
        {draw = "3x300", pos = "H1.5 V4.5", return_past_if_necessary = true},
        {draw = "4x300", pos = "H1.5 V5.5", return_past_if_necessary = true},
        {owner = "return", pos = "H1.5 V7"}
    },
    z119 = {
        {dealState = "107", pos = "H2 V5"},
        {draw = "234", owner = "banish", pos = "H1 V7"}
    },
    z119g = {{draw = "234", pos = "H3 V7"}},
    z121 = {
        {fetch = "gold", owner = "banish", pos = "H3 V6"},
        {dealActiveState = "106", pos = "H2 V7"}
    },
    z123w = {{draw = "w701", balloonWhiteTerrainCard = "w123", pos = "H1 V4"}},
    z124 = {{fetch = "113", owner = "banish", draw = "350", pos = "H3 V4"}},
    z126 = {
        {drawKeyword = "serenity", pos = "H1 V3"}, {draw = "000", pos = "H1 V6"}
    },
    z127 = {
        {dealState = "102", pos = "H1 V5"}, {rst = "043", pos = "H0.5 V7"},
        {rst = "043 + banner", banner = "raft", pos = "H1.5 V7"},
        {dealState = "102", rst = "125", pos = "H3 V7"}
    },
    z130 = {
        {draw = "003", pos = "H1 V7"}, {dealActiveState = "103", pos = "H3 V7"}
    },
    z132w = {{draw = "w701", balloonWhiteTerrainCard = "w132", pos = "H2 V4"}},
    z133 = {{draw = "160", pos = "H3 V3"}},
    z134 = {
        {owner = "discard", pos = "H2 V5"}, {dealState = "106", pos = "H2 V6"}
    },
    z137 = {{}},
    z137_2659c8 = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z137_e81d5b = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z137g = {
        {draw = "2x250", pos = "V3", return_past_if_necessary = true},
        {draw = "3x250", pos = "V4", return_past_if_necessary = true},
        {draw = "4x250", pos = "V5", return_past_if_necessary = true},
        {owner = "return", pos = "H3 V7"}
    },
    z152 = {{rst = "321", pos = "H1 V6"}},
    z155 = {
        {fetch = "gold", owner = "banish", pos = "H1 V5"},
        {fetch = "gold", owner = "banish", draw = "2x003, 199", pos = "H3 V7"},
        {fetch = "gold", owner = "banish", draw = "2x003, 201", pos = "H1 V7"}
    },
    z155g = {{draw = "172", pos = "H2 V2"}, {draw = "000", pos = "H1 V7"}},
    z156 = {{draw = "247", pos = "H2 V6"}},
    z158 = {{draw = "030"}, {fetch = "gold", owner = "banish", pos = "H3 V7"}},
    z159 = {{rst = "321", pos = "H1 V6"}},
    z162 = {{drawKeyword = "stealth", pos = "H2 V7"}},
    z164 = {
        {dealStateToAll = "102", pos = "H2 V3"}, {rst = "043", pos = "H1 V4"},
        {rst = "043 + banner", banner = "raft", pos = "H2 V4"},
        {rst = "009", dealStateToAll = "101", pos = "H2 V6"}
    },
    z165 = {
        {draw = "195", pos = "H1 V3"},
        {draw = "195 + banner", banner = "horns", pos = "H2 V3"},
        {draw = "209", pos = "H2 V5"}
    },
    z167 = {
        {draw = "169", pos = "H1 V2"},
        {draw = "169", dealState = "104", pos = "H1 V4"}
    },
    z174 = {{draw = "211", pos = "H2 V7"}},
    z177 = {{}},
    z177_2684b4 = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z177_420241 = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z177_b5a29e = {
        {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "6x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z177g = {
        {draw = "1x250", pos = "V3", return_past_if_necessary = true},
        {draw = "2x250", pos = "V4", return_past_if_necessary = true},
        {draw = "4x250", pos = "V5", return_past_if_necessary = true},
        {owner = "return", pos = "H3 V7"}
    },
    z185 = {{fetch = "390", owner = "discard", pos = "H2.5 V7"}},
    z190 = {{owner = "discard", fetch = "269", pos = "H1 V7"}},
    z192 = {{fetchSpecifiedCard = "draw", pos = "H1.5 V7"}},
    z202 = {
        {fetch = "174", owner = "discard", pos = "H1 V3"},
        {fetch = "207", owner = "discard", pos = "H3 V7"},
        {dealActiveState = "108", pos = "V5"}
    },
    z202_3fb097 = {
        {fetch = "174", owner = "discard", pos = "H1 V5"},
        {fetch = "207", owner = "discard", pos = "H1 V7"},
        {dealActiveState = "108", pos = "H3 V7"}
    },
    z206 = {{fetch = "gold", owner = "banish", draw = "3x003", pos = "H1 V7"}},
    z206_12dbc6 = {
        {owner = "banish", draw = "570", pos = "H1 V4"},
        {owner = "discard", fetch = "206", pos = "H2 V7"}
    },
    z206g = {
        {draw = "000", pos = "H1 V7"},
        {draw = "000 + banner", banner = "horns", pos = "V7"}
    },
    z207 = {{dealState = "108", pos = "H2 V7"}},
    z211 = {
        {discardAction = 5, pos = "H2 V5.5"},
        {fetch = "211", owner = "discard", pos = "H2.5 V7"}
    },
    z217 = {{}},
    z217_905505 = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z217_85da0c = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z217_e0b698 = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "7x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z217g = {
        {draw = "1x250", pos = "V2.5", return_past_if_necessary = true},
        {draw = "2x250", pos = "V3.5", return_past_if_necessary = true},
        {draw = "3x250", pos = "V4.5", return_past_if_necessary = true},
        {draw = "4x250", pos = "V5.5", return_past_if_necessary = true},
        {owner = "return", pos = "V7"}
    },
    z222 = {
        {draw = "248", pos = "H2.5 V7"}, {dealState = "104", pos = "H2.5 V7"}
    },
    z225 = {{draw = "310", pos = "H3 V2"}},
    z226 = {
        {returnState = "vigilance", pos = "H2 V2"},
        {draw = "310", pos = "H2 V3"}
    },
    z228 = {{draw = "243", pos = "H3 V7"}},
    z231 = {{draw = "310", pos = "H2.5 V3"}},
    z233 = {{draw = "462", pos = "H1 V3"}},
    z235 = {
        {draw = "049", pos = "H1 V2"},
        {draw = "049 + banner", banner = "offering", pos = "V2"},
        {draw = "2x003", pos = "H1 V6"}, {dealState = "105", pos = "V7"}
    },
    z236 = {
        {draw = "049", pos = "H1 V2"},
        {draw = "049 + banner", banner = "offering", pos = "V2"},
        {draw = "5x003", pos = "H1 V6"}, {draw = "7x003", pos = "H1 V7"}
    },
    z237 = {
        {draw = "049", pos = "H1 V2"},
        {draw = "049 + banner", banner = "offering", pos = "V2"},
        {draw = "5x003", pos = "H1 V6"}
    },
    z237g = {
        {draw = "049", pos = "H1 V2"},
        {draw = "049 + banner", banner = "offering", pos = "V2"},
        {draw = "6x003", pos = "H1 V6"}
    },
    z238 = {
        {draw = "049", pos = "V2"},
        {draw = "049 + banner", banner = "offering", pos = "H3 V2"},
        {draw = "3x003", pos = "H3 V6"}
    },
    z239 = {
        {draw = "049", pos = "H1 V2"},
        {draw = "049 + banner", banner = "offering", pos = "H1 V3"},
        {draw = "003", pos = "H1 V6"}
    },
    z240 = {
        {fetch = "gold", owner = "banish", draw = "003, 350", pos = "H3 V7"},
        {fetch = "gold", owner = "banish", pos = "H3"}
    },
    z244 = {
        {draw = "148", pos = "V6"},
        {draw = "148 + banner", banner = "gem1, gem2", pos = "V7"}
    },
    z245 = {{draw = "245 + banner", banner = "bottle", pos = "H1 V7"}},
    z255 = {{draw = "188", pos = "H3 V7"}},
    z256 = {
        {rst = "125", pos = "H3 V5.5"},
        {rst = "017", dealStateToAll = "101", pos = "H2 V6.5"}
    },
    z259g = {{draw = "489"}},
    z263 = {
        {fetch = "gold", owner = "banish", draw = "001", pos = "H3 V5"},
        {fetch = "gold", owner = "banish", draw = "099", pos = "H3 V6"}, {
            fetch = "gold",
            owner = "banish",
            draw = "001",
            dealStateToAll = "105",
            pos = "H3 V7"
        }
    },
    z268 = {
        {draw = "040", pos = "V6"},
        {draw = "040 + banner", banner = "sanctuary", pos = "H3 V6"}
    },
    z273 = {{}},
    z276 = {{}},
    z276_98cb25 = {{fetch = "gold", owner = "banish", pos = "H1 V5"}},
    z276_90925c = {{owner = "banish", pos = "H1 V5"}},
    z278 = {
        {
            draw = "2x200",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "4x200",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "6x200",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z278g = {
        {draw = "1x200", pos = "V2.5", return_past_if_necessary = true},
        {draw = "2x200", pos = "V3.5", return_past_if_necessary = true},
        {draw = "3x200", pos = "V4.5", return_past_if_necessary = true},
        {draw = "4x200", pos = "V5.5", return_past_if_necessary = true}
    },
    z279 = {{fetch = "305", owner = "discard", pos = "H3 V6"}},
    z281g = {
        {fetch = "311", owner = "discard", pos = "H2.5 V4"},
        {dealState = "103, 104", pos = "H2.5 V6"}
    },
    z282 = {{draw = "000", returnState = "tired", pos = "H1.5 V2"}},
    z284w = {{draw = "w701", balloonWhiteTerrainCard = "w284", pos = "H3 V4"}},
    z285 = {
        {draw = "315", pos = "H1.5 V5.5"},
        {returnState = "bloody", pos = "H3 V7"},
        {dealState = "102", pos = "H2.5 V7"}
    },
    z287 = {{}},
    z287_b6e0eb = {
        {
            draw = "3x200",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "4x200",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "5x200",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z287_b4e4c8 = {
        {
            draw = "1x200",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "3x200",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }
    },
    z287g = {
        {draw = "2x300", pos = "V3", return_past_if_necessary = true},
        {draw = "3x300", pos = "V4", return_past_if_necessary = true},
        {draw = "4x300", pos = "V5", return_past_if_necessary = true},
        {owner = "return", pos = "V7"}
    },
    z289 = {{}},
    z289_c88425 = {
        {
            draw = "1x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z289_702bca = {
        {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "4x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z289g = {
        {draw = "1x250", pos = "V3", return_past_if_necessary = true},
        {draw = "2x250", pos = "V4", return_past_if_necessary = true},
        {draw = "4x250", pos = "V5", return_past_if_necessary = true},
        {owner = "return", pos = "H3 V7"}
    },
    z291 = {
        {draw = "2x001, 2x003", owner = "banish", pos = "H1.5 V6"},
        {dealActiveState = "104", pos = "H2 V7"}
    },
    z294 = {
        {fetch = "337", owner = "return", pos = "V6"},
        {draw = "000", pos = "H1 V7"}, {dealState = "104", pos = "H3 V7"}
    },
    z295 = {{rst = "346", pos = "H2.5 V7"}},
    z301 = {
        {fetch = "079", owner = "discard", pos = "V5"},
        {fetch = "162", owner = "discard", pos = "H3 V6"},
        {fetch = "228", owner = "discard", pos = "V7"}
    },
    z302 = {{drawKeyword = "aggressiveness", pos = "H1 V7"}},
    z306 = {{draw = "292", pos = "H1 V5"}, {draw = "353", pos = "H1 V6"}},
    z307 = {{dealState = "104", pos = "H3 V7"}},
    z309 = {{}},
    z309_818f28 = {
        {
            draw = "2x200",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "3x200",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "4x200",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {
            draw = "5x200",
            fetch = "gold",
            owner = "banish",
            pos = "V5.5",
            return_past_if_necessary = true
        }
    },
    z309_71e3d2 = {
        {
            owner = "banish",
            dealActiveState = "105",
            draw = "5x001, 003",
            pos = "H3 V6"
        }, {owner = "discard", dealActiveState = "104, 105", pos = "V7"}
    },
    z309g = {
        {draw = "2x300", pos = "V3", return_past_if_necessary = true},
        {draw = "3x300", pos = "V4", return_past_if_necessary = true},
        {draw = "4x300", pos = "V5", return_past_if_necessary = true}
    },
    z316 = {{fetch = "323", owner = "banish", pos = "H2.5 V6"}},
    z323 = {{fetch = "332", owner = "banish", pos = "H2.5 V7"}},
    z326w = {{draw = "w701", balloonWhiteTerrainCard = "w326", pos = "H1 V6"}},
    z329 = {
        {draw = "340", dealState = "108", pos = "H2 V5"},
        {returnState = "poisoned", pos = "H2 V6"}
    },
    z330 = {
        {draw = "000", returnState = "tired", pos = "H0.5 V5.5"},
        {dealActiveState = "101", pos = "H0.5 V7"}
    },
    z332 = {{draw = "267", pos = "H2.5 V2"}},
    z334w = {{draw = "w701", balloonWhiteTerrainCard = "w334", pos = "H2.5 V5"}},
    z335 = {{draw = "325", pos = "H2.5 V7"}},
    z337 = {
        {owner = "discard", fetch = "337", draw = "003", pos = "H3 V6"},
        {owner = "discard", dealActiveState = "104, 105", pos = "H2 V7"}
    },
    z337_cf45e8 = {{draw = "337 + banner", banner = "feather", pos = "H3 V7"}},
    z342 = {{rst = "143", pos = "H2.5 V7"}},
    z344 = {{draw = "001, 003", owner = "banish", pos = "H2.5 V6"}},
    z345 = {{}},
    z352 = {
        {fetch = "gold", owner = "banish", pos = "H1 V4"},
        {dealState = "106", pos = "H1 V7"}
    },
    z352g = {
        {draw = "000", returnState = "tired", pos = "H1 V6"},
        {drawKeyword = "serenity", pos = "H1 V3"},
        {dealState = "102", pos = "H1 V7"}
    },
    z355 = {{draw = "467", pos = "H3 V6"}, {draw = "391", pos = "H3 V7"}},
    z356 = {
        {draw = "388", pos = "H1 V7"},
        {draw = "356 + banner", banner = "feather", pos = "H3 V7"}
    },
    z358 = {{banish = "green 446", rst = "321"}},
    z359 = {
        {draw = "360", pos = "H1 V7"},
        {draw = "360 + banner", banner = "sanctuary", pos = "V7"}
    },
    z369 = {
        {rst = "377", pos = "H2.5 V5"}, {dealState = "101", pos = "H2.5 V6.5"}
    },
    z371 = {{rst = "358", owner = "banish", pos = "H2 V7"}},
    z371g = {
        {owner = "discard", drawKeyword = "serenity", pos = "H1 V6"},
        {owner = "discard", dealState = "100", pos = "H2 V7"}
    },
    z373 = {{draw = "003", owner = "discard", pos = "H2.5 V7"}},
    z379 = {{draw = "421", pos = "H1 V5"}, {dealState = "101", pos = "H0.5 V7"}},
    z380 = {
        {fetch = "452", owner = "discard", pos = "H1 V3"},
        {fetch = "442", owner = "discard", pos = "H1 V7"}
    },
    z382_c89dd8 = {{dealActiveState = "101", pos = "H2 V7"}},
    z383 = {{fetch = "gold", owner = "banish", draw = "003", pos = "H3 V7"}},
    z383g = {{draw = "383 + banner", banner = "fullhead", pos = "H1 V7"}},
    z384 = {{fetch = "384 + banner", banner = "vine", pos = "H3 V7"}},
    z385 = {{drawKeyword = "stealth", pos = "H1 V7"}},
    z386 = {
        {dealState = "101", pos = "H1 V 4.5"},
        {returnActiveState = "freezing", returnAction = 5, pos = "H3 V4.5"},
        {draw = "386 + banner", banner = "emptyhead", pos = "V7"}
    },
    z391 = {
        {dealActiveState = "104", pos = "H1 V1"},
        {fetch = "368", owner = "discard", draw = "5x003", pos = "H1 V5"},
        {fetch = "368", owner = "discard", dealActiveState = "108", pos = "V7"}
    },
    z394 = {
        {fetch = "418", owner = "discard", pos = "H2 V7"},
        {fetch = "428", owner = "discard", pos = "H2.5 V7"}
    },
    z399 = {{owner = "banish", pos = "H1 V7"}},
    z403 = {
        {draw = "367", pos = "V5"}, {draw = "375", pos = "V6"},
        {draw = "378", pos = "V7"}
    },
    z416 = {{draw = "349", pos = "H1.5 V7"}},
    z417 = {{fetch = "314", owner = "banish", pos = "V7"}},
    z417g = {
        {
            owner = "banish",
            dealActiveState = "105",
            draw = "3x001, 2x003",
            pos = "V6"
        }, {fetch = "417", owner = "discard", pos = "V7"}
    },
    z417g_b88259 = {
        {dealActiveState = "102", pos = "V6"}, {owner = "discard", pos = "V7"}
    },
    z422 = {{drawKeyword = "vigilance", pos = "H2 V7"}},
    z429 = {{fetch = "gold", owner = "banish", pos = "H1.5 V3"}},
    z429g = {{fetch = "457", owner = "discard", pos = "V3"}},
    z430 = {{draw = "448", dealState = "108", pos = "H1 V6"}},
    z433 = {{drawKeyword = "vigilance", pos = "H1 V7"}},
    z434 = {
        {fetch = "423", owner = "discard", pos = "H1 V5"},
        {dealState = "101", pos = "H1 V6"}
    },
    z434_3e8e9e = {{fetch = "423", owner = "discard", pos = "H1 V5"}},
    z435 = {
        {fetch = "430", owner = "banish", pos = "H1 V3"},
        {dealState = "108", pos = "H1 V7"}
    },
    z437 = {{draw = "5x003, 448", pos = "V2"}},
    z438 = {{draw = "404", pos = "H1.5 V7"}, {draw = "436", pos = "H3 V7"}},
    z439 = {{}},
    z439_97723e = {{draw = "000, 572", pos = "H1 V7"}},
    z439_0e761a = {{draw = "172", pos = "V3"}},
    z443 = {
        {rst = "284", dealStateToAll = "104", pos = "H1.5 V4"},
        {dealStateToAll = "101"},
        pos = "H1.5 V7"
    },
    z447 = {{draw = "404", pos = "H2 V6.5"}, {draw = "436", pos = "H2.5 V6.5"}},
    z451 = {
        {
            fetch = "423",
            banish = "423",
            draw = "363",
            owner = "banish",
            pos = "H1 V5"
        }
    },
    z453 = {
        {fetch = "385", owner = "discard", pos = "H1 V6"},
        {fetch = "433", owner = "discard", pos = "V7"},
        {dealActiveState = "101", pos = "H3 V7"}
    },
    z457 = {{draw = "249", pos = "H1 V4"}},
    z461 = {{rst = "449", pos = "H1 V7"}},
    z468 = {
        {owner = "banish", fetch = "gold", draw = "002", pos = "H1 V5"},
        {owner = "banish", fetch = "gold", draw = "2x002", pos = "H1 V6"},
        {owner = "banish", fetch = "gold", draw = "3x002", pos = "H1 V7"},
        {dealActiveState = "106", pos = "H3 V7"}
    },
    z471 = {{draw = "454", pos = "V7", necessaryKeyword = "music"}},
    z473 = {
        {rst = "473 + banner", banner = "raft", pos = "V6"},
        {dealStateToAll = "101", pos = "V7"}
    },
    z483 = {{draw = "468", pos = "V6"}},
    z492 = {
        {fetch = "gold", owner = "banish", draw = "509", pos = "H1 V7"},
        {dealActiveState = "101", pos = "H3 V7"}
    },
    z492g = {{draw = "000", pos = "V6"}},
    z493 = {{draw = "531, 534", pos = "V7"}},
    z502 = {{fetch = "gold", owner = "banish", pos = "H3"}},
    z507 = {{fetchSpecifiedCard = "rst", owner = "return", pos = "H1.5 V4.5"}},
    z512 = {
        {fetch = "gold", owner = "banish", banish = "504", pos = "V7"},
        {fetch = "gold", owner = "banish", banish = "514", pos = "H3 V7"}
    },
    z513 = {
        {dealActiveState = "101", pos = "H3"},
        {owner = "return", fetch = "554", pos = "V7"}
    },
    z521 = {{dealActiveState = "104", pos = "H3 V7"}},
    z522 = {{fetch = "gold", owner = "banish", pos = "H1"}},
    z522g = {{dealActiveState = "103", pos = "V7"}},
    z524 = {{banish = "554", owner = "return", pos = "H1 V7"}},
    z524g = {{fetch = "577", owner = "return", banish = "577", pos = "H1 V7"}},
    z532 = {
        {
            owner = "banish",
            fetch = "gold",
            draw = "2x200",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            owner = "banish",
            fetch = "gold",
            draw = "3x200",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            owner = "banish",
            fetch = "gold",
            draw = "4x200",
            pos = "V4.5",
            return_past_if_necessary = true
        }, {owner = "banish", fetch = "gold", draw = "526", pos = "V6"}
    },
    z532g = {
        {draw = "2x300", pos = "V3", return_past_if_necessary = true},
        {draw = "3x300", pos = "V4", return_past_if_necessary = true},
        {draw = "4x300", pos = "V5", return_past_if_necessary = true},
        {draw = "526", pos = "V6"}
    },
    z533 = {{dealActiveState = "104", pos = "H3 V7"}},
    z535_247858 = {{}},
    z537 = {{rst = "149", pos = "H1 V6"}},
    z538 = {{fetch = "gold", owner = "banish", draw = "464", pos = "H2 V7"}},
    z538g = {{rst = "364", pos = "V5"}, {rst = "449", pos = "H1 V7"}},
    z540 = {
        {fetch = "581", owner = "discard", pos = "H1"},
        {dealActiveState = "2x104", pos = "H1 V6"},
        {dealState = "104", pos = "H1 V7"}
    },
    z541 = {
        {
            fetch = "504",
            banish = "504",
            owner = "discard",
            pos = "H1 V7",
            return_past_if_necessary = true
        }
    },
    z544 = {{fetchSpecifiedCard = "fetch", owner = "discard", pos = "H2.5"}},
    z548 = {
        {draw = "526", pos = "V5"},
        {fetch = "580", owner = "discard", pos = "V7"}
    },
    z551 = {
        {fetch = "565", owner = "discard", pos = "H3"},
        {fetch = "575", owner = "discard", pos = "H3 V6"}
    },
    z553 = {
        {
            draw = "2x200",
            fetch = "gold",
            owner = "banish",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x200",
            fetch = "gold",
            owner = "banish",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "4x200",
            fetch = "gold",
            owner = "banish",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z553g = {
        {draw = "2x300", pos = "V3.5", return_past_if_necessary = true},
        {draw = "3x300", pos = "V4.5", return_past_if_necessary = true},
        {draw = "555", pos = "V6"}
    },
    z554 = {
        {fetch = "513", owner = "return", pos = "H1 V7"},
        {owner = "return", pos = "V7"}
    },
    z554g = {{banish = "524", pos = "V7"}},
    z556 = {
        {dealState = "106", pos = "H2 V7"}, {rst = "259", pos = "H3 V4;5"},
        {rst = "389", pos = "H1 V5.5"}, {rst = "427", pos = "H3 V5.5"}
    },
    z557 = {{draw = "569", pos = "H1 V3"}},
    z560 = {{draw = "683", pos = "H2.5 V6"}},
    z567 = {{draw = "529", pos = "H2"}, {draw = "000", pos = "H1 V7"}},
    z568 = {{draw = "526", pos = "H1 V3"}},
    z579 = {
        {fetch = "523", owner = "discard", pos = "V6"},
        {fetch = "533", owner = "discard", pos = "H3 V7"}
    },
    z582 = {{rst = "588", pos = "V7"}},
    z591 = {{draw = "508", pos = "V6"}, {draw = "518", pos = "H3 V7"}},
    z592 = {
        {draw = "574", pos = "H1 V5"}, {draw = "584", pos = "H1 V6"},
        {draw = "594", pos = "H1 V7"}
    },
    z593 = {
        {draw = "597", pos = "H1 V6"}, {drawKeyword = "will", pos = "H3 V6"}
    },
    z596 = {{dealState = "104", pos = "H1 V5"}},
    z599 = {
        {ret = "600 from past", pos = "H2 V1"},
        {fetch = "gold", owner = "banish", pos = "H2.5 V7"}
    },
    z604 = {
        {owner = "banish", fetch = "gold", draw = "001", pos = "H1 V4"},
        {owner = "banish", fetch = "gold", draw = "2x001", pos = "H1 V5"},
        {owner = "banish", fetch = "gold", draw = "3x001", pos = "H1 V6"}, {
            owner = "banish",
            fetch = "gold",
            draw = "603",
            pos = "H2.5 V7",
            return_past_if_necessary = true
        }
    },
    z604g = {
        {draw = "001", pos = "H1 V6"},
        {draw = "603", pos = "H1 V7", return_past_if_necessary = true}
    },
    z605 = {{}},
    z605_2122fd = {
        {owner = "discard", pos = "V6"}, {dealStateToAll = "106", pos = "V7"}
    },
    z605_cad704 = {
        {owner = "banish", draw = "2x001", pos = "H1 V7"},
        {owner = "discard", fetch = "605", pos = "V7"},
        {owner = "discard", pos = "H3 V7"}
    },
    z606 = {
        {rst = "664", pos = "H3 V5.5"}, {dealStateToAll = "101", pos = "H2 V7"}
    },
    z607 = {
        {rst = "659", pos = "H3 V5"}, {dealActiveState = "103", pos = "H2 V7"}
    },
    z611 = {{draw = "624", pos = "H1 V6"}, {draw = "624", pos = "H1 V7"}},
    z613 = {
        {
            fetch = "613 + banner",
            banner = "torch",
            owner = "discard",
            pos = "H1 V2"
        }
    },
    z616 = {{dealState = "106", pos = "H1 V7"}},
    z617 = {
        {draw = "653", pos = "H2 V6"},
        {owner = "banish", fetch = "gold", pos = "H2 V7"}
    },
    z617g = {
        {drawKeyword = "aggressiveness", pos = "H2 V6"},
        {draw = "357", pos = "H2 V7"}
    },
    z619 = {{draw = "000", pos = "H1 V6"}, {dealState = "101", pos = "H2 V7"}},
    z621 = {{}},
    z621_95886a = {{draw = "350", owner = "banish", pos = "H2 V6"}},
    z625 = {
        {draw = "109", owner = "banish", pos = "H0.5 V6"},
        {draw = "601", owner = "banish", pos = "H1.5 V6"}
    },
    z625g = {{dealState = "108", pos = "H1 V7"}},
    z633 = {
        {
            owner = "discard",
            fetch = "633 + banner",
            banner = "torch",
            pos = "H1 V2"
        }
    },
    z637 = {
        {
            draw = "2x150",
            fetch = "gold",
            owner = "banish",
            pos = "V2.5",
            return_past_if_necessary = true
        }, {
            draw = "3x150",
            fetch = "gold",
            owner = "banish",
            pos = "V3.5",
            return_past_if_necessary = true
        }, {
            draw = "5x150",
            fetch = "gold",
            owner = "banish",
            pos = "V4.5",
            return_past_if_necessary = true
        }
    },
    z637g = {
        {draw = "1x250", pos = "V2.5", return_past_if_necessary = true},
        {draw = "2x250", pos = "V3.5", return_past_if_necessary = true},
        {draw = "3x250", pos = "V4.5", return_past_if_necessary = true},
        {draw = "4x250", pos = "V5.5", return_past_if_necessary = true}
    },
    z639_b15613 = {
        {dealActiveState = "108", pos = "H3 V3"},
        {dealActiveState = "2x108", pos = "H2 V4"}
    },
    z643 = {{draw = "627", pos = "H1 V7"}},
    z645 = {
        {dealState = "107", pos = "H2 V4.5"},
        {draw = "603", pos = "H2 V5.4", return_past_if_necessary = true},
        {
            returnActiveState = "frightened",
            drawKeyword = "will",
            pos = "H2 V6.5"
        }
    },
    z647 = {
        {owner = "discard", pos = "H1 V6"}, {dealState = "104", pos = "H3 V6"}
    },
    z649 = {{draw = "615", pos = "H3 V2.5"}},
    z651 = {
        {fetchSpecifiedCard = "draw", pos = "H2 V6"},
        {fetch = "gold", owner = "banish", pos = "H2.5 V7"}
    },
    z651g = {{dealActiveState = "101", pos = "H1 V7"}},
    z658 = {
        {draw = "629", pos = "H2 V6"},
        {drawKeyword = "will, aggressiveness", pos = "H2 V7"}
    },
    z660 = {{drawKeyword = "aggressiveness", pos = "H2 V7"}},
    z665 = {{fetch = "646", pos = "H1 V6"}, {dealState = "108", pos = "H2 V7"}},
    z672 = {{draw = "686", pos = "H1 V6"}},
    z677 = {{draw = "693 + banner", banner = "prison", pos = "H1.5 V3"}},
    z677w = {{draw = "w751", balloonWhiteTerrainCard = "w677", pos = "H1 V3"}},
    z679 = {{draw = "717", pos = "H3 V2"}},
    z681 = {
        {owner = "banish", fetch = "gold", draw = "705", pos = "H1 V5"}, {
            owner = "banish",
            fetch = "gold",
            draw = "705 + banner",
            banner = "horns",
            pos = "H1 V6"
        }, {dealActiveState = "103", pos = "H2 V7"}
    },
    z681g = {{drawKeyword = "serennity", pos = "H2 V7"}},
    z684 = {{draw = "600", pos = "H1 V7"}},
    z693 = {
        {
            owner = "banish",
            fetch = "gold",
            draw = "801",
            balloonGreenTerrainCard = "677",
            pos = "H1 V6"
        }, {dealActiveState = "102", pos = "H3 V6"}
    },
    z695 = {
        {
            owner = "return",
            draw = "695",
            necessaryKeyword = "music",
            pos = "H3 V6"
        }
    },
    z695g = {
        {returnAction = 3, owner = "return", pos = "H2 V4"},
        {returnAction = 6, owner = "return", pos = "H2 V4.5"},
        {returnAction = 9, owner = "return", pos = "H2 V5"},
        {returnAction = 12, owner = "return", pos = "H2 V5.5"},
        {owner = "return", pos = "H1 V7"}
    },
    z702 = {
        {owner = "return", draw = "818", pos = "H1 V4"},
        {owner = "return", draw = "2x818", pos = "H1 V5"},
        {owner = "return", pos = "H1 V7"}
    },
    z702w = {{draw = "w734 + banner", banner = "prison", pos = "H3 V1"}},
    z703 = {
        {owner = "return", draw = "818", pos = "H1 V4"},
        {owner = "return", draw = "2x818", pos = "H1 V5"},
        {owner = "return", pos = "H1 V7"}
    },
    z709 = {{owner = "return"}},
    z710w = {{draw = "w751", balloonWhiteTerrainCard = "w710", pos = "H2.5 V6"}},
    z712 = {{draw = "000", pos = "H1 V7"}},
    z713 = {{owner = "banish", draw = "003, 350", pos = "H2 V7"}},
    z718 = {
        {draw = "746", pos = "H1 V6"}, {dealActiveState = "103", pos = "H2 V7"}
    },
    z722w = {{draw = "739", pos = "H1 V6"}},
    z724 = {{owner = "discard", pos = "H1.5 V7"}},
    z726 = {{owner = "return", draw = "687", pos = "H2 V6"}},
    z726g = {{draw = "000", pos = "H1 V6"}},
    z731 = {{draw = "671", pos = "H1.5 V6"}},
    z731w = {{draw = "w751", balloonWhiteTerrainCard = "w731", pos = "H2 V4"}},
    z732 = {{owner = "return", fetch = "734", pos = "H1 V4"}},
    z733 = {
        {draw = "2x200", pos = "H1 V2", return_past_if_necessary = true},
        {draw = "4x200", pos = "H1 V3", return_past_if_necessary = true},
        {draw = "6x200", pos = "H1 V4", return_past_if_necessary = true},
        {dealActiveState = "104", pos = "H2 V7"}
    },
    z734 = {
        {owner = "return", fetch = "732", pos = "H1 V6"},
        {
            owner = "return",
            fetch = "732 + banner",
            banner = "wind",
            pos = "H2 V6"
        }
    },
    z753 = {
        {owner = "banish", draw = "753", pos = "H1 V6"},
        {dealActiveState = "101", pos = "H2 V7"}
    },
    z753_a4dcf2 = {
        {owner = "return", draw = "678", pos = "H1 V5"},
        {owner = "return", draw = "688", pos = "H1.5 V5"},
        {owner = "return", ret = "banished 753", pos = "H1 V7"}
    },
    z753_0d9eab = {
        {owner = "banish", draw = "753", pos = "H3 V5"},
        {draw = "750", pos = "H3 V7"}
    },
    z735w = {{draw = "w751", balloonWhiteTerrainCard = "w735", pos = "H2.5 V3"}},
    z743 = {
        {draw = "603", pos = "V1"},
        {draw = "250", pos = "H3", return_past_if_necessary = true},
        {dealActiveState = "103", pos = "H3 V5"}
    },
    z752 = {{draw = "600", pos = "H1 V7"}},
    z757 = {{owner = "banish", draw = "2x003, 801", pos = "H2 V6"}},
    z759 = {
        {owner = "banish", draw = "790", pos = "H2 V3"},
        {owner = "banish", draw = "790", pos = "H2 V5"},
        {dealActiveState = "101", pos = "H3 V7"}
    },
    z762 = {
        {draw = "767", pos = "H2.5 V6"}, {dealStateToAll = "103", pos = "V7"}
    },
    z768 = {{fetch = "gold", owner = "banish", draw = "2x003", pos = "H1 V7"}},
    z769 = {{banish = "769, 784", pos = "H1 V7"}},
    z769g = {{dealActiveState = "104", pos = "H3 V7"}},
    z775 = {
        {
            owner = "return",
            draw = "003",
            drawKeyword = "vigilance,serenity",
            pos = "H1.5 V4"
        }
    },
    z776 = {
        {
            draw = "2x200",
            owner = "discard",
            fetch = "776",
            pos = "V3",
            return_past_if_necessary = true
        }, {
            draw = "3x200",
            owner = "discard",
            fetch = "776",
            pos = "V4",
            return_past_if_necessary = true
        }, {
            draw = "4x200",
            owner = "discard",
            fetch = "776",
            pos = "V5",
            return_past_if_necessary = true
        }
    },
    z776g = {
        {draw = "2x250", pos = "V3", return_past_if_necessary = true},
        {draw = "3x250", pos = "V4", return_past_if_necessary = true},
        {dealActiveState = "100", pos = "H2.5 V7"}
    },
    z779w = {{draw = "772", pos = "H1 V7"}},
    z779w_b4079f = {
        {owner = "banish", draw = "3x001", pos = "H1 V7"},
        {owner = "return", dealActiveState = "104", draw = "772", pos = "H3 V6"}
    },
    z783 = {{fetch = "833", banish = "833", pos = "H2 V2"}},
    z784 = {{banish = "769, 784", pos = "H1 V4"}},
    z784g = {
        {
            owner = "return",
            ret = "banished 769, banished 784, 769, 784",
            pos = "H1 V6"
        }
    },
    z788 = {
        {dealState = "101", pos = "H3 V1"},
        {draw = "000", returnState = "tired", pos = "H2.5 V6"}
    },
    z791w = {{drawKeyword = "serenity, stamina", pos = "H1 V3"}},
    z791w_961983 = {
        {owner = "discard", draw = "003", pos = "H2.5 V5"},
        {dealActiveState = "104", pos = "H2.5 V7"}
    },
    z803 = {
        {draw = "885", pos = "H2 V6"}, {draw = "895", pos = "H2.5 V6"},
        {draw = "775", pos = "H1 V7"},
        {owner = "discard", fetch = "803", pos = "H2 V7"}
    },
    z808 = {{rst = "397", draw = "003", pos = "H1.5 V5"}},
    z810 = {{owner = "discard", pos = "H1 V7"}},
    z812 = {{rst = "763", pos = "H1.5 V6"}},
    z813 = {
        {draw = "913", rst = "851", figurine = "Barge", pos = "H1 V5"},
        {dealActiveState = "101", pos = "H2 V7"}
    },
    z816 = {{draw = "838", pos = "H1 V7"}},
    z822 = {
        {draw = "857", pos = "H1.5 V2"},
        {draw = "857 + banner", banner = "ball", pos = "H2 V2"}
    },
    z825 = {{draw = "863", pos = "H1 V6"}, {dealState = "104", pos = "H2 V7"}},
    z833 = {{draw = "600", pos = "H1 V7"}},
    z833g = {{draw = "600", pos = "H1 V7"}},
    z842 = {
        {fetch = "gold", owner = "banish", pos = "H1 V7"},
        {draw = "600", pos = "H2 V7"}
    },
    z843 = {
        {owner = "banish", draw = "4 x 001", pos = "H2.5 V6"},
        {owner = "banish", pos = "H2.5 V7"}
    },
    z844 = {{draw = "600", pos = "H1 V7"}},
    z845 = {
        {owner = "banish", fetch = "gold", draw = "3x003", pos = "H1 V4"},
        {owner = "banish", fetch = "gold", draw = "3x003,585", pos = "H1 V5"},
        {draw = "850", dealState = "102", pos = "H1 V7"}
    },
    z845g = {{owner = "discard", fetch = "457", pos = "H1 V6"}},
    z848 = {
        {fetch = "gold", owner = "banish", pos = "H3 V2"},
        {fetch = "gold", owner = "banish", pos = "H2.5 V6"}
    },
    z850_4a9092 = {
        {owner = "return", draw = "850", pos = "H1 V3"},
        {owner = "discard", pos = "H2 V6"}
    },
    z854 = {
        {draw = "764", pos = "H1.5 V5"}, {banish = "green 764", pos = "H2 V6"}
    },
    z855 = {{owner = "discard", fetch = "855", pos = "H3 V6"}},
    z859 = {{draw = "805", pos = "H1 V6"}},
    z859g = {{draw = "835", pos = "H1 V6"}, {draw = "805", pos = "H2 V6"}},
    z860 = {
        {draw = "864", pos = "H2 V6"}, {draw = "874", pos = "H2.5 V6"},
        {draw = "775", pos = "H1 V7"},
        {owner = "discard", fetch = "860", pos = "H2 V7"}
    },
    z862 = {
        {owner = "banish", fetch = "gold", draw = "3x003", pos = "H1 V5"},
        {owner = "banish", fetch = "gold", draw = "3x003,585", pos = "H1 V6"},
        {draw = "850", pos = "H2 V7"}
    },
    z862g = {{drawKeyword = "will", pos = "H2 V7"}},
    z865 = {{draw = "600", pos = "H1 V7"}},
    z868 = {{}},
    z868_48ea4d = {
        {owner = "discard", pos = "H2 V4.5"},
        {dealActiveState = "103", owner = "return", pos = "H2 V6"}
    },
    z868_04b6fb = {
        {returnAction = 3, owner = "discard", pos = "H2 V3"},
        {returnAction = 6, owner = "discard", pos = "H2 V4"},
        {returnAction = 9, owner = "discard", pos = "H2 V5"},
        {draw = "868", owner = "discard", pos = "H2 V6"}
    },
    z871 = {
        {draw = "882", pos = "H2 V6"}, {draw = "892", pos = "H2.5 V6"},
        {draw = "775", pos = "H1 V7"},
        {owner = "discard", fetch = "871", pos = "H2 V7"}
    },
    z871g = {{draw = "873", pos = "H1 V6"}},
    z883 = {{draw = "172", pos = "H3 V2"}},
    z884 = {{draw = "785", pos = "H2 V7"}},
    z887 = {
        {owner = "banish", fetch = "gold", draw = "3x003", pos = "H2 V5"},
        {owner = "banish", fetch = "gold", draw = "3x003, 585", pos = "H1.5 V6"},
        {draw = "850", pos = "H2.5 V7"}
    },
    z887g = {{drawKeyword = "will", pos = "H1.5 V7"}},
    z889 = {{draw = "906", pos = "H1 V5"}, {rst = "365", pos = "H3 V7"}},
    z891 = {{draw = "000", pos = "H2 V2"}},
    z893 = {{draw = "600", pos = "H1 V7"}},
    z896 = {{draw = "600", pos = "H1 V7"}},
    z907 = {{owner = "banish", fetch = "833", banish = "833", pos = "H2 V2"}},
    z909 = {{rst = "111", pos = "H3 V7"}},
    z910 = {
        {owner = "banish", fetch = "gold", draw = "3x003", pos = "H1 V5"},
        {owner = "banish", fetch = "gold", draw = "3x003, 585", pos = "H1 V6"},
        {draw = "850", pos = "H2 V7"}
    },
    z910g = {{drawKeyword = "serenity", pos = "H2 V7"}}
}

TEMP_EVENTS = {
    z000 = {{owner = "return", pos = "H1 V6"}},
    z000g = {
        {owner = "banish", pos = "V5", ret = "banished 000"},
        {dealState = "101", pos = "V7"}
    },
    z001g = {{owner = "discard", dealActiveState = "106"}},
    z002g = {{owner = "discard", dealActiveState = "106"}},
    z013 = {
        {draw = "003", owner = "banish", pos = "V6"},
        {draw = "2x003", owner = "banish", pos = "V7"}
    },
    z013_a34d34 = {{dealActiveState = "104", owner = "discard", pos = "V7"}},
    z020 = {{draw = "431, 5x003", owner = "banish", pos = "H3 V7"}},
    z023 = {{rst = "125", dealStateToAll = "101, 102"}},
    z025 = {{owner = "discard", dealStateToAll = "103", pos = "H1 V7"}},
    z026 = {{fetch = "010", banish = "010", owner = "discard", pos = "H2 V7"}},
    z027 = {{}},
    z028 = {{owner = "return", draw = "2x003", rst = "125"}},
    z030 = {{owner = "discard", draw = "001", pos = "V7"}},
    z030_5e83df = {{owner = "discard", draw = "2x001", pos = "V7"}},
    z030_713f38 = {
        {owner = "discard", draw = "002", pos = "H2 V5"},
        {owner = "discard", dealActiveState = "104", pos = "H2 V7"}
    },
    z033 = {
        {draw = "3x003", pos = "H1 V4"},
        {
            banish = "green 017, 016, 032",
            owner = "return",
            rst = "038",
            pos = "H3 V6"
        },
        {
            banish = "green 017, 016, 032",
            owner = "return",
            rst = "132",
            pos = "H2 V6"
        },
        {
            banish = "green 017, 016, 032",
            owner = "return",
            rst = "198",
            pos = "H1 V7"
        }
    },
    z040 = {{drawKeyword = "stamina, will", pos = "H2 V3"}},
    z045 = {
        {owner = "discard", dealState = "104", pos = "H3 V3"},
        {owner = "discard", draw = "003", pos = "H3 V6"}
    },
    z050_7fccd7 = {
        {
            owner = "discard",
            draw = "000",
            necessaryKeyword = "serenity",
            pos = "V7"
        }
    },
    z051g = {{owner = "discard", dealState = "106"}},
    z052 = {{rst = "517", owner = "return", pos = "H1 V7"}},
    z069 = {{}},
    z069_e3313e = {
        {
            owner = "discard",
            returnState = "tired, bloody",
            drawKeyword = "serenity",
            pos = "H2 V2"
        }
    },
    z069_9334eb = {
        {
            owner = "return",
            draw = "069 + banner",
            banner = "bloody_hunt",
            pos = "H2 V5"
        }
    },
    z069_4c9940 = {
        {owner = "discard", draw = "003", pos = "H1 V5"},
        {owner = "discard", dealState = "108", pos = "H1 V7"}
    },
    z070 = {{owner = "return", dealState = "102", rst = "198"}},
    z075 = {{owner = "return", rst = "198"}},
    z077 = {
        {owner = "satchel", draw = "3x003", pos = "H1 V5"},
        {owner = "discard", draw = "2x108", pos = "H1 V6"}
    },
    z080 = {{}},
    z081 = {{owner = "banish", pos = "H3 V3"}},
    z084 = {{dealState = "102", rst = "132", pos = "H1 V5"}},
    z089 = {{owner = "return", rst = "175"}},
    z091 = {{draw = "666", pos = "H1 V2"}},
    z091_4e2c03 = {{draw = "750", pos = "H2 V7"}},
    z097 = {{}},
    z097_8aa20d = {{drawKeyword = "serenity", owner = "discard", pos = "H1 V4"}},
    z097_97d4ee = {
        {dealState = "107", pos = "H3 V6"}, {dealState = "103", pos = "H3 V7"}
    },
    z097_5bb956 = {
        {owner = "discard", pos = "H2 V6"},
        {owner = "discard", dealState = "104", pos = "H3 V7"}
    },
    z097_7cc420 = {
        {owner = "discard", draw = "003", pos = "H1.5 V5"},
        {owner = "discard", dealState = "108", pos = "H1.5 V7"}
    },
    z109 = {{owner = "discard", draw = "003, 050, 2x350, 357", pos = "H2 V6"}},
    z110 = {
        {owner = "discard", draw = "003, 146", pos = "H3 V4"},
        {owner = "discard", dealState = "104", pos = "H2 V7"}
    },
    z114 = {{owner = "discard", draw = "2x003, 146", pos = "H2 V7"}},
    z135_cf1c30 = {{owner = "discard", dealActiveState = "108"}},
    z138 = {
        {owner = "satchel", draw = "2x003", pos = "H3 V4"},
        {dealState = "103", pos = "H2 V5.5"},
        {dealActiveState = "101, 104", pos = "H2 V6.5"}
    },
    z145 = {{owner = "return", draw = "170", pos = "H2 V7"}},
    z148 = {{owner = "discard", dealActiveState = "101", pos = "H2 V7"}},
    z150 = {{}},
    z150_21292a = {{draw = "2x001", owner = "discard", pos = "H2 V7"}},
    z150_dc4df1 = {
        {owner = "discard", draw = "003", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H2 V7"}
    },
    z150_990f78 = {
        {
            owner = "discard",
            dealActiveState = "105",
            draw = "4x001",
            pos = "H2 V6"
        }
    },
    z150_a9da74 = {
        {owner = "discard", draw = "001", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "104", pos = "H2 V7"}
    },
    z150_3cd7c8 = {
        {
            owner = "discard",
            dealActiveState = "105",
            draw = "3x001, 003",
            pos = "H2 V7"
        }
    },
    z150_d9d444 = {{owner = "discard", draw = "2x001", pos = "H2 V7"}},
    z150_25bbe4 = {
        {
            owner = "discard",
            dealActiveState = "105",
            draw = "3x001, 003",
            pos = "H2 V7"
        }
    },
    z150_b19ba6 = {
        {
            owner = "discard",
            dealActiveState = "105",
            draw = "5x001, 003",
            pos = "V5.5"
        }, {owner = "discard", dealActiveState = "104, 105", pos = "V7"}
    },
    z157 = {
        {dealActiveState = "104", pos = "H2 V6"},
        {dealState = "103", pos = "H2.5 V7"}
    },
    z160 = {
        {owner = "discard", draw = "110", pos = "H3 V5"},
        {
            owner = "discard",
            draw = "110 + banner",
            banner = "shield",
            pos = "H3 V6"
        }
    },
    z169 = {{draw = "118", pos = "H1 V3"}, {dealState = "104", pos = "H2 V6"}},
    z170 = {
        {owner = "return", draw = "2x003", pos = "H2.5 V6"},
        {owner = "return", discardAction = 3, pos = "H1.5 V6"}
    },
    z173 = {
        {dealState = "101", pos = "H2 V4"},
        {owner = "return", rst = "009", pos = "H3 V5"}
    },
    z181 = {{draw = "003", pos = "H2 V3"}, {draw = "246", pos = "H1.5 V6"}},
    z188 = {
        {fetch = "390", discard = "255", owner = "discard", pos = "H1 V3"}, {
            fetch = "185",
            discard = "255",
            dealActiveState = "104",
            owner = "discard",
            pos = "H2 V7"
        }
    },
    z189 = {
        {
            owner = "return",
            rst = "004",
            dealStateToAll = "101, 102",
            pos = "H2 V4"
        }
    },
    z193 = {
        {draw = "2x108", owner = "discard", pos = "H3 V3"},
        {draw = "2x003", owner = "discard", pos = "H3 V6"}
    },
    z194 = {
        {owner = "return", rst = "198", pos = "H3 V4"},
        {owner = "return", rst = "004", pos = "H3 V7"}
    },
    z195 = {{owner = "banish", dealActiveState = "105, 4x003", pos = "H2 V7"}},
    z200 = {{}},
    z200_5774fd = {
        {owner = "discard", draw = "2x002", pos = "V6"},
        {owner = "discard", draw = "4x002", pos = "V7"}
    },
    z200_9fa5f6 = {
        {owner = "discard", draw = "2x002", pos = "V6"},
        {owner = "discard", draw = "4x002", pos = "V7"}
    },
    z200_29fa77 = {
        {owner = "discard", draw = "2x002", pos = "V6"},
        {owner = "discard", draw = "4x002", pos = "V7"}
    },
    z200_4327eb = {{owner = "discard", draw = "3x002, 003", pos = "V7"}},
    z200_00eb80 = {{owner = "discard", draw = "3x002, 003", pos = "V7"}},
    z200_08ec79 = {
        {
            owner = "discard",
            draw = "5x001, 003",
            dealActiveState = "105",
            pos = "V5.5"
        }, {owner = "discard", dealActiveState = "104, 105", pos = "V7"}
    },
    z200_5daed8 = {
        {owner = "discard", draw = "003", pos = "V6"},
        {owner = "discard", dealActiveState = "108", pos = "V7"}
    },
    z200_07c9e6 = {
        {draw = "2x001", owner = "banish", pos = "V6"},
        {draw = "216", owner = "banish", pos = "V7"},
        {owner = "discard", pos = "H3 V7"}
    },
    z200_049869 = {{owner = "discard"}},
    z200_6bf6fc = {{owner = "discard"}},
    z200_ecece5 = {{owner = "discard"}},
    z204 = {
        {owner = "discard", draw = "2x003", pos = "H2 V5"},
        {dealState = "104", pos = "H3 V7"}
    },
    z209 = {
        {draw = "2x003", pos = "H1 V3"},
        {dealActiveState = "103", pos = "H2 V4"}
    },
    z209_bf4e35 = {{owner = "discard", dealActiveState = "107", pos = "H2 V7"}},
    z210 = {{owner = "discard", dealActiveState = "105", pos = "H2 V6"}},
    z215 = {
        {
            dealActiveState = "105",
            draw = "5x003",
            owner = "banish",
            pos = "H1 V6"
        }
    },
    z219 = {{dealActiveState = "108", pos = "H1 V5"}},
    z223 = {{owner = "discard", dealState = "104", pos = "H1 V3"}},
    z234 = {
        {draw = "003", owner = "discard", pos = "H1 V7"},
        {dealActiveState = "104, 105", owner = "discard", pos = "H2.5 V7"}
    },
    z242 = {
        {owner = "satchel", draw = "2x003", pos = "H1 V5"},
        {owner = "discard", dealActiveState = "104", pos = "H2 V7"}
    },
    z243 = {
        {owner = "discard", draw = "2x002, 2x003", pos = "H1 V5"},
        {owner = "discard", dealActiveState = "104", pos = "H2 V7"}
    },
    z246 = {{}},
    z246_71fd5f = {
        {owner = "discard", draw = "003, 405", pos = "H3 V3"},
        {owner = "discard", draw = "246", pos = "H3 V5"}
    },
    z246_5b448b = {
        {owner = "discard", draw = "003, 080", pos = "H3 V3"},
        {owner = "discard", draw = "246", pos = "H3 V5"}
    },
    z246_7bbfd0 = {
        {owner = "discard", draw = "003, 324", pos = "H1 V4"},
        {owner = "discard", draw = "246", pos = "H1 V6"}
    },
    z246_d029ad = {
        {owner = "discard", draw = "003, 261", pos = "H3 V3"},
        {owner = "discard", draw = "246", pos = "H3 V5"}
    },
    z246_482f9b = {
        {owner = "discard", draw = "003, 197", pos = "H1 V3"},
        {owner = "discard", draw = "246", pos = "H1 V5"}
    },
    z249 = {
        {draw = "3x002", pos = "V2"}, {draw = "050", pos = "H1 V4"},
        {draw = "050 + banner", banner = "sanctuary", pos = "V4"},
        {owner = "banish", pos = "V5"}
    },
    z250 = {{}},
    z250_040b52 = {
        {
            owner = "discard",
            dealActiveState = "104",
            draw = "3x001",
            pos = "H1.5 V6"
        }
    },
    z250_2def59 = {
        {owner = "discard", draw = "2x003", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H2.5 V7"}
    },
    z250_e9f759 = {
        {owner = "discard", draw = "003", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H2.5 V7"}
    },
    z250_bcb694 = {
        {owner = "discard", draw = "2x003", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H2 V7"}
    },
    z250_ccc504 = {
        {owner = "discard", dealActiveState = "105", draw = "4x001", pos = "V6"},
        {owner = "discard", pos = "V7"}
    },
    z250_362de8 = {{draw = "2x001", owner = "discard", pos = "H2 V7"}},
    z250_2e9d4f = {
        {draw = "001", owner = "discard", pos = "H1.5 V6"},
        {dealActiveState = "104", owner = "discard", pos = "H2.5 V7"}
    },
    z250_85ea2f = {
        {draw = "003, 479", owner = "banish", pos = "V6"},
        {owner = "banish", pos = "V7"}
    },
    z250_0136ff = {
        {owner = "satchel", pos = "H1 V7"},
        {owner = "banish", draw = "250", pos = "H2.5 V7"}
    },
    z251 = {
        {owner = "satchel", draw = "002, 3x003", pos = "H2 V5"},
        {dealState = "104", pos = "H2 V6.5"}
    },
    z257 = {{owner = "discard", drawKeyword = "serenity, will", pos = "H2 V4"}},
    z265 = {
        {draw = "274", pos = "H3 V2"}, {dealActiveState = "103", pos = "H1 V3"},
        {draw = "296", owner = "banish", pos = "V5"},
        {owner = "banish", pos = "V7"}
    },
    z269 = {{rst = "258", banish = "258", draw = "2x003", owner = "discard"}},
    z272 = {{dealState = "103", pos = "H2.5 V3"}},
    z270 = {{dealActiveState = "104"}},
    z275 = {
        {dealState = "103, 105", pos = "H2 V3"},
        {returnState = "frightened", pos = "H0.5 V5.5"}
    },
    z280 = {
        {dealActiveState = "612", pos = "H2 V4"},
        {dealState = "101", pos = "H2.5 V4.5"},
        {banish = "258, 151", owner = "discard", rst = "258"}
    },
    z292 = {
        {owner = "discard", dealActiveState = "102", rst = "341"},
        {owner = "discard", fetch = "315", pos = "V6"}
    },
    z296 = {{owner = "banish"}},
    z296g = {
        {draw = "3x003", pos = "H2 V3.5"}, {owner = "discard", pos = "H2 V4.5"}
    },
    z297 = {{owner = "discard", draw = "003", pos = "H3 V2"}},
    z299 = {{dealState = "101, 103", pos = "H2.5 V6"}},
    z300 = {{}},
    z300_216e94 = {{draw = "213", owner = "discard", pos = "H2.5 V6.5"}},
    z300_d8f55f = {{draw = "3x002, 003", owner = "discard", pos = "H2 V6.5"}},
    z300_764e72 = {{owner = "discard"}},
    z300_ae386f = {{owner = "discard"}},
    z300_669e71 = {{owner = "discard"}},
    z300_c7bf8e = {
        {draw = "003", owner = "discard", pos = "H2 V6"},
        {draw = "2x108", owner = "discard", pos = "H2 V7"}
    },
    z300_2b6434 = {
        {draw = "003", owner = "discard", pos = "H2 V6"},
        {draw = "2x108", owner = "discard", pos = "H2 V7"}
    },
    z300_0e35eb = {{draw = "2x108", owner = "discard", pos = "H2.5 V7"}},
    z300_ad76c4 = {
        {draw = "2x002", owner = "discard", pos = "H2 V6"},
        {draw = "4x002", owner = "discard", pos = "H2.5 V7"}
    },
    z300_10038c = {
        {draw = "2x002", owner = "discard", pos = "H2 V6"},
        {draw = "4x002", owner = "discard", pos = "H2 V7"}
    },
    z300_4d081f = {
        {
            dealActiveState = "105",
            draw = "5x001, 003",
            owner = "discard",
            pos = "H2.5 V5.5"
        }, {dealActiveState = "104, 105", owner = "discard", pos = "H2.5 V6.5"}
    },
    z310_7a97f9 = {{dealState = "108", pos = "V7"}},
    z310_ff69fa = {
        {draw = "001", owner = "discard", pos = "H1 V6"},
        {dealActiveState = "104", owner = "discard", pos = "V7"}
    },
    z315 = {{dealState = "102", rst = "056"}},
    z325 = {{owner = "return", pos = "H3 V7"}},
    z327 = {
        {returnActiveState = "bloody", pos = "H2.5 V2"},
        {dealActiveState = "104", pos = "H2 V2.5"}
    },
    z333 = {{}},
    z333_3f1556 = {{dealActiveState = "107", pos = "H3 V3"}},
    z333_d5a6b1 = {{drawKeyword = "curse", pos = "H2.5 V5.5"}},
    z333_833a4d = {{drawKeyword = "curse", pos = "H2.5 V6"}},
    z349 = {{fetch = "416", banish = "416", dealState = "101"}},
    z350_dd98dd = {
        {
            owner = "discard",
            dealActiveState = "800",
            draw = "350",
            pos = "H2 V6"
        }
    },
    z350_293fa9 = {{draw = "357", pos = "H1 V3"}, {draw = "350", pos = "H3 V4"}},
    z350g = {{}},
    z353 = {
        {owner = "discard", dealState = "102", pos = "V5"},
        {owner = "discard", dealState = "101, 102", pos = "V6"}
    },
    z354 = {
        {owner = "discard", draw = "3x001, 2x003", pos = "V6"},
        {owner = "discard", dealState = "104, 105", pos = "V7"}
    },
    z362 = {
        {owner = "satchel", draw = "3x001, 2x003", pos = "H1 V5"},
        {owner = "discard", dealState = "104, 105", pos = "H2 V7"}
    },
    z367 = {
        {dealStateToAll = "103", pos = "H3 V3"},
        {owner = "discard", dealStateToAll = "108", pos = "H2 V7"}
    },
    z375 = {{owner = "discard", dealStateToAll = "104", pos = "H3 V3"}},
    z378 = {{draw = "003", drawKeyword = "will", rst = "191", pos = "H2 V6"}},
    z382_8c913b = {
        {dealActiveState = "104", pos = "H2 V3"},
        {owner = "discard", pos = "H2 V4"},
        {owner = "discard", dealState = "101", pos = "H2 V6"}
    },
    z392 = {
        {draw = "406", owner = "banish"}, {
            draw = "7x003",
            owner = "banish",
            returnAction = -1,
            pos = "H2.5 V5",
            banishPlayerCard = "197_11b2e5"
        }, {rst = "095", pos = "H1 V7"}
    },
    z398 = {{draw = "408", pos = "H1 V6"}, {draw = "230", pos = "H0.5 V7"}},
    z400 = {{owner = "banish", draw = "3x003", pos = "V5.5"}},
    z404 = {
        {owner = "discard", dealActiveState = "103", pos = "H1 V5"},
        {dealActiveState = "107", pos = "H1 V6"}
    },
    z405 = {{}},
    z419 = {
        {dealActiveState = "103", pos = "H1.5 V5.5"},
        {dealActiveState = "107", pos = "H3 V5.5"}
    },
    z419_699b94 = {{dealState = "104"}},
    z425 = {
        {owner = "satchel", draw = "3x001, 4x003", pos = "H3 V4"},
        {
            owner = "banish",
            draw = "425",
            dealStateToAll = "102",
            pos = "H3 V6.5"
        }
    },
    z425g = {{owner = "satchel", draw = "4x001, 2x003", pos = "H3 V3"}},
    z436 = {
        {owner = "banish", draw = "002", pos = "H2 V5.5"},
        {owner = "discard", dealActiveState = "103", pos = "H2.5 V7"}
    },
    z432 = {
        {owner = "return", rst = "048", pos = "H1 V6"},
        {owner = "return", rst = "132", pos = "H1.5 V6"},
        {owner = "return", rst = "313", pos = "H3.5 V6"},
        {owner = "return", rst = "168", pos = "H2 V6"},
        {owner = "return", pos = "H1 V4"}
    },
    z444 = {
        {dealActiveState = "103", pos = "H3 V3.5"},
        {dealActiveState = "107", pos = "H3 V4.5"}
    },
    z449g = {{rst = "361", pos = "V5"}, {dealStateToAll = "106", pos = "V7"}},
    z454 = {{draw = "5x003", rst = "497"}},
    z460 = {
        {
            dealStateToAll = "104",
            pos = "V3",
            draw = "496",
            owner = "discard",
            fetch = "496"
        }
    },
    z463 = {{}},
    z463_e5bed9 = {{owner = "discard", dealActiveState = "104", pos = "H1 V4"}},
    z463_e2d016 = {{owner = "discard", drawKeyword = "curse", pos = "H1 V7"}},
    z465 = {{dealState = "107", pos = "H3"}},
    z466 = {
        {draw = "2x001, 3x003, 498", owner = "discard", pos = "V6"},
        {owner = "discard", dealActiveState = "104", pos = "V7"}
    },
    z467 = {{dealActiveState = "102", pos = "V3"}, {rst = "459", pos = "V5"}},
    z469 = {
        {owner = "return", drawKeyword = "serenity", pos = "H1 V5"},
        {owner = "return", draw = "495", pos = "H1 V7"}
    },
    z470 = {
        {dealStateToAll = "102", pos = "V3"}, {draw = "481", pos = "H1 V6"},
        {dealActiveState = "103", pos = "H3 V6"}
    },
    z472 = {
        {owner = "discard", draw = "003", pos = "H2 V4.5"},
        {owner = "discard", dealState = "107", pos = "H2 V7"}
    },
    z474 = {
        {draw = "2x001, 3x003, 498", pos = "V5"},
        {dealActiveState = "104", pos = "V7"}
    },
    z476 = {{rst = "483", owner = "return"}},
    z477 = {
        {owner = "banish", draw = "413", pos = "H1 V5"}, {
            owner = "banish",
            draw = "7x003",
            returnAction = -1,
            pos = "H1 V7",
            banishClueCard = "repentance"
        }
    },
    z479 = {{draw = "543", owner = "discard"}, {owner = "discard", pos = "V5"}},
    z481 = {{rst = "459", pos = "V5"}, {dealStateToAll = "101", pos = "V2"}},
    z482 = {{owner = "discard", draw = "3x001, 4x003", pos = "H2 V3"}},
    z487 = {{owner = "banish", draw = "401", pos = "H1 V6"}},
    z488 = {{draw = "3x003"}, {dealActiveState = "104, 105", pos = "V5"}},
    z489 = {{owner = "return", rst = "536", pos = "H2 V3"}},
    z491 = {{dealActiveState = "103", pos = "V5"}},
    z495 = {{owner = "discard", draw = "545", pos = "H3 V5"}},
    z496 = {{draw = "2x003", rst = "449", owner = "return"}},
    z499g = {{rst = "494", pos = "H1"}},
    z500 = {{owner = "banish"}},
    z500_793461 = {
        {owner = "banish", pos = "H1.5 V7"}, {owner = "banish", pos = "H2.5 V6"}
    },
    z500_61777b = {
        {owner = "banish", drawKeyword = "aggressiveness", pos = "H1 V7"},
        {owner = "banish", drawKeyword = "will", banish = "424", pos = "H3 V7"}
    },
    z500_29d69b = {
        {drawKeyword = "serenity", draw = "003", pos = "H2 V5"},
        {dealState = "103", pos = "H2 V4"}, {owner = "banish", pos = "H1 V6"}
    },
    z500_b86290 = {
        {drawKeyword = "will, stamina, aggressiveness", pos = "H2 V3"},
        {returnAction = 1, pos = "H2 V4"}, {owner = "banish", pos = "H2 V5"}
    },
    z500_8f615f = {
        {returnAction = 3, owner = "banish", pos = "V5"},
        {owner = "banish", pos = "H1 V6"}
    },
    z500g = {
        {draw = "477", owner = "banish", pos = "H1 V6"},
        {draw = "487", owner = "banish", pos = "H3 V6"}
    },
    z506 = {{draw = "350, 2x003", owner = "banish"}},
    z506g = {{dealActiveState = "102", pos = "H3 V6"}},
    z508 = {
        {
            draw = "003",
            banish = "546",
            fetch = "546",
            discard = "591",
            owner = "banish"
        }
    },
    z543 = {{draw = "466", owner = "discard"}, {owner = "discard"}},
    z518 = {{dealState = "104, 105", pos = "V7"}},
    z523 = {
        {dealState = "103"}, {dealState = "107", pos = "V5"},
        {rst = "056", pos = "H1 V6"}
    },
    z526 = {{draw = "526", owner = "discard", pos = "H3"}},
    z526_7a360b = {
        {owner = "return", pos = "V3"},
        {owner = "return", dealActiveState = "104, 105", pos = "V4"},
        {owner = "banish", draw = "2x002, 2x003", pos = "V6"},
        {owner = "return", dealActiveState = "104, 105", pos = "V7"}
    },
    z528 = {{draw = "003", owner = "discard", pos = "H2.5 V3"}},
    z528_519b94 = {
        {dealState = "106", owner = "discard", pos = "H3 V3"},
        {draw = "003", owner = "discard", pos = "H2.5 V5"},
        {dealState = "102", pos = "H2 V7"}
    },
    z528_784bd5 = {
        {draw = "003", owner = "discard", pos = "H2.5 V3"},
        {dealActiveState = "103", owner = "discard", pos = "H2 V4"}
    },
    z528_38cb3c = {
        {draw = "1x150", owner = "banish", pos = "V3"},
        {draw = "2x150", owner = "banish", pos = "V3.5"},
        {draw = "3x150", owner = "banish", pos = "V4"}
    },
    z529 = {{owner = "banish", returnAction = 5, pos = "H2.5 V6"}},
    z530 = {
        {dealActiveState = "104, 107", pos = "H2 V3"},
        {owner = "discard", pos = "H2.5 V5"}
    },
    z535 = {
        {owner = "banish", draw = "5x003", pos = "H1 V6"},
        {owner = "banish", pos = "H2 V7"}
    },
    z549 = {{}},
    z549_f97f03 = {
        {owner = "discard", draw = "003", pos = "H1 V5"},
        {dealState = "104", pos = "H2.5 V7"}
    },
    z549_ff44c3 = {
        {owner = "discard", draw = "001", pos = "H2 V6"},
        {owner = "discard", draw = "2x001, 549", pos = "H2 V7"}
    },
    z549_e2c248 = {
        {owner = "discard", drawKeyword = "will", pos = "H2 V5"},
        {dealActiveState = "107", pos = "H3 V7"},
        {dealActiveState = "103", pos = "H2 V7"}
    },
    z555 = {{draw = "2x003", pos = "H1 V5"}},
    z558 = {{draw = "003", pos = "V5"}, {dealActiveState = "104", pos = "V7"}},
    z558_ecde8c = {{draw = "001", owner = "banish", pos = "H3 V7"}},
    z561 = {{owner = "discard", dealState = "100", pos = "H2 V7"}},
    z561_fc264b = {{discardAction = 3, pos = "H2 V6"}},
    z561_6af44c = {{owner = "discard", dealState = "101", pos = "H2 V7"}},
    z561_016bc5 = {
        {owner = "discard", draw = "003", drawKeyword = "will", pos = "H1 V5"},
        {draw = "003", pos = "H2.5 V6"}
    },
    z563 = {{draw = "2x002, 003", pos = "H1 V5"}},
    z569 = {{draw = "2x003", banish = "514", rst = "514"}},
    z572 = {
        {owner = "return", pos = "H2.5 V5"},
        {owner = "return", dealActiveState = "104", pos = "H3 V6"}
    },
    z572_be5d0a = {{drawKeyword = "stamina", pos = "H2 V6"}},
    z573 = {{owner = "discard", drawKeyword = "serenity", pos = "H1 V6"}},
    z573_a58de6 = {
        {draw = "003", pos = "H1.5 V7"},
        {owner = "banish", draw = "350", pos = "H1.5 V6"}
    },
    z573_138852 = {
        {owner = "discard", dealState = "103", pos = "H3 V6"},
        {owner = "discard", dealActiveState = "107", pos = "H2 V6"},
        {dealState = "104", pos = "H2 V7"}
    },
    z573_2272db = {
        {dealState = "103", pos = "H2.5 V3"},
        {owner = "discard", draw = "003", pos = "H2 V6"}
    },
    z576 = {{owner = "discard", dealState = "101", pos = "H1.5 V5"}},
    z581 = {
        {
            draw = "003",
            discard = "504",
            fetch = "banished 504",
            owner = "discard"
        }
    },
    z594 = {
        {draw = "412", pos = "H2 V4.5", owner = "banish"}, {
            draw = "7x003",
            rst = "149",
            returnAction = 30,
            pos = "H1 V6",
            banishClueCard = "sanctuary",
            banishPlayerCard = "144_c8d432, 441_bed7cc, 085_dd793a, 414_e2bd4e, 372_0e09dd, 062_5e2a38",
            owner = "banish"
        }
    },
    z600 = {{owner = "return", pos = "H2 V7"}},
    z600_277c91 = {
        {owner = "discard", pos = "H2 V6"},
        {dealActiveState = "104", pos = "H2 V7"}
    },
    z600_7cb031 = {
        {owner = "return", pos = "H1.5 V5"},
        {dealState = "101", pos = "H2.5 V6"},
        {dealActiveState = "103", pos = "H1.5 V7"}
    },
    z600_5777be = {
        {owner = "return", draw = "003", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "104, 105", pos = "H2 V7"}
    },
    z600g = {{owner = "return", pos = "H2 V6"}},
    z601 = {{dealState = "108", pos = "H2 V6"}},
    z603 = {{dealStateToAll = "101", owner = "discard", pos = "H1 V6"}},
    z608 = {{}},
    z608_97236b = {
        {owner = "discard", draw = "2x003", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H2 V7"}
    },
    z615 = {{draw = "627", pos = "H2 V3"}, {owner = "discard", pos = "H2 V4"}},
    z621_465337 = {
        {owner = "discard", draw = "2x003", pos = "H3"},
        {dealState = "104", pos = "H2 V6"},
        {dealActiveState = "105", pos = "H3 V7"}
    },
    z624 = {
        {owner = "discard", draw = "638", pos = "H1 V4.5"},
        {dealStateToAll = "103", owner = "discard", pos = "H1 V6"}
    },
    z627 = {{}},
    z627_bb6f7b = {{owner = "return"}},
    z627_51102d = {
        {dealState = "104", pos = "H1 V4"}, {owner = "return", pos = "H1 V5"}
    },
    z632 = {{}},
    z632_3497e1 = {
        {owner = "discard", drawKeyword = "aggressiveness", pos = "V7"}
    },
    z632_f50807 = {
        {owner = "banish", drawKeyword = "will", draw = "620", pos = "V6.5"}
    },
    z636 = {
        {owner = "discard", pos = "V5"},
        {dealStateToAll = "101", pos = "V6", owner = "discard"}
    },
    z638 = {
        {owner = "discard", draw = "644", pos = "H1 V6"},
        {owner = "discard", draw = "644", dealStateToAll = "108", pos = "V7"}
    },
    z639 = {{}},
    z639_8e4dd4 = {
        {owner = "discard", pos = "H2 V3"},
        {owner = "discard", draw = "2x003", pos = "H2 V4"},
        {owner = "satchel", draw = "001, 2x003", pos = "H2 V5"},
        {owner = "discard", dealActiveState = "108, 134", pos = "H2 V6"}
    },
    z644 = {
        {owner = "discard", draw = "652", pos = "H3 V5"},
        {dealStateToAll = "104", pos = "H3 V7"}
    },
    z646 = {
        {owner = "return", rst = "319", pos = "H2 V1"},
        {owner = "discard", pos = "H2 V2"}
    },
    z652 = {{owner = "discard", draw = "661", pos = "H3 V3"}},
    z661 = {{owner = "discard", draw = "614", pos = "H1 V7"}},
    z663 = {
        {
            owner = "banish",
            returnAction = 12,
            returnActiveState = "tired, frightened",
            pos = "H2 V5"
        }
    },
    z666 = {{}},
    z666_afc05d = {{dealState = "102", pos = "H1 V5"}},
    z666_63e68b = {{dealState = "103", pos = "H1 V7"}},
    z666_879adc = {
        {owner = "banish", draw = "3x003", pos = "V5"},
        {owner = "banish", dealActiveState = "107", pos = "V7"}
    },
    z670 = {
        {owner = "discard", draw = "2x003", pos = "V5"},
        {owner = "return", pos = "H1 V7"}
    },
    z671 = {
        {owner = "discard", drawKeyword = "serenity, stealth", pos = "H2 V5"},
        {owner = "discard", draw = "674", pos = "H1.5 V6"}, {
            owner = "discard",
            draw = "674 + banner",
            banner = "bloody_hunt",
            pos = "H2 V6"
        }
    },
    z678 = {
        {owner = "return", draw = "734", ret = "banished 753", pos = "H1.5 V6"}
    },
    z683g = {
        {owner = "discard", dealActiveState = "103", pos = "H3 V5"},
        {owner = "discard", dealActiveState = "107", pos = "H2 V6"}
    },
    z685 = {{owner = "return", draw = "670", pos = "H1 V6"}},
    z686 = {
        {
            returnAction = 20,
            returnState = "tired, hungry, disheartened",
            owner = "banish",
            pos = "H1.5 V4"
        }, {
            returnAction = 30,
            returnState = "tired, hungry, disheartened",
            owner = "banish",
            pos = "H2.5 V4"
        }
    },
    z687 = {
        {owner = "discard", draw = "696", pos = "H3 V3"},
        {owner = "discard", dealActiveState = "102", pos = "H3 V5"}
    },
    z688 = {
        {owner = "return", draw = "734", ret = "banished 753", pos = "H1.5 V6"}
    },
    z696 = {
        {owner = "discard", draw = "741", pos = "H2 V4"},
        {owner = "discard", dealState = "103", pos = "H3 V5"},
        {owner = "discard", dealState = "102", pos = "H3 V7"}
    },
    z698 = {
        {drawKeyword = "aggressiveness", pos = "H2 V4"},
        {owner = "return", pos = "H1 V6"}
    },
    z701w = {
        {
            owner = "discard",
            drawKeyword = "skill, vigilance",
            draw = "w701",
            pos = "H2 V5"
        },
        {
            owner = "discard",
            draw = "w701",
            dealActiveState = "103",
            pos = "H2 V7"
        }
    },
    z701w_9c890f = {
        {rst = "balloonGreenTerrainCard", pos = "H2 V4"}, {
            draw = "balloonWhiteTerrainCard + banner",
            numAdd = 650,
            banner = "prison",
            pos = "H2 V6"
        }, {owner = "return", pos = "H1 V7"}
    },
    z706g = {{owner = "return", pos = "H2 V2"}},
    z711 = {
        {owner = "return", draw = "912", rst = "719", pos = "H2 V5"},
        {owner = "return", draw = "912", rst = "773", pos = "H3 V5"},
        {owner = "return", pos = "H1 V7"}
    },
    z713_5bc468 = {
        {owner = "return", drawKeyword = "stamina", pos = "H1.5 V5"},
        {dealState = "102", pos = "H2 V6"}, {discardAction = 3, pos = "H1 V7"},
        {owner = "return", pos = "H2 V7"}
    },
    z727 = {{owner = "discard", draw = "750", pos = "H2 V6"}},
    z741 = {
        {
            owner = "discard",
            banish = "green 726",
            draw = "2x003, 668, 726",
            pos = "H3 V4"
        }, {dealActiveState = "104", pos = "H2.5 V6"},
        {dealState = "103", pos = "H3.5 V7"}
    },
    z747w = {{owner = "return", draw = "734", pos = "H2.5 V6"}},
    z749 = {
        {
            owner = "satchel",
            banish = "green 726",
            draw = "5x001, 2x003, 668, 726",
            pos = "H3 V4"
        }, {dealActiveState = "104", pos = "H2 V6"},
        {dealState = "103", pos = "H3 V7"}
    },
    z750_ae1226 = {{owner = "return", pos = "V6"}},
    z751w = {
        {rst = "balloonGreenTerrainCard", pos = "H2 V4"},
        {
            draw = "balloonWhiteTerrainCard + banner",
            banner = "prison",
            pos = "H2 V6"
        }, {owner = "return", pos = "H1 V7"}
    },
    z753g = {
        {draw = "714", pos = "H2 V4", owner = "banish"}, {
            draw = "3x003",
            rst = "w334",
            balloonWhiteTerrainCard = "w334",
            returnAction = 15,
            pos = "H1 V6",
            owner = "banish",
            banishClueCard = "prison"
        }
    },
    z757g = {{owner = "return", pos = "H2 V4"}},
    z764 = {{owner = "return", pos = "H2 V7"}},
    z764g = {{owner = "return", draw = "banished 764", pos = "H1 V7"}},
    z770 = {
        {
            owner = "banish",
            draw = "7x003",
            ret = "banished 764",
            returnAction = -1,
            pos = "H2 V6"
        }
    },
    z772 = {
        {owner = "banish", pos = "H2 V2"}, {owner = "return", pos = "H2 V4"},
        {owner = "banish", pos = "H1.5 V6"}
    },
    z781 = {
        {owner = "return", rst = "901", pos = "H2 V6"},
        {owner = "return", pos = "H2.5 V7"}
    },
    z785 = {
        {
            owner = "banish",
            draw = "3x002, 350, 357, 050",
            drawKeyword = "skill, vigilance",
            pos = "H2 V4"
        }
    },
    z787 = {
        {owner = "return", pos = "V5"},
        {owner = "return", dealActiveState = "106", pos = "H3 V6"}
    },
    z790 = {
        {
            owner = "discard",
            draw = "740",
            fetch = "778",
            banish = "759",
            pos = "H2 V2"
        }, {owner = "discard", fetch = "778", banish = "759", pos = "H3 V6"}
    },
    z794 = {{owner = "discard", draw = "001", pos = "H2 V5"}},
    z799 = {{drawKeyword = "curse", pos = "H2 V6"}},
    z805 = {{owner = "banish", pos = "H1 V5"}},
    z805g = {
        {owner = "discard", draw = "003", pos = "H1 V5"},
        {dealState = "104, 105", pos = "H1 V7"}
    },
    z815 = {{}},
    z815_8691fd = {
        {banish = "green 815", pos = "H2 V1"},
        {
            owner = "banish",
            draw = "815 + banner",
            banner = "bloody_hunt",
            pos = "V3"
        }, {owner = "banish", draw = "003", pos = "H2 V5"}
    },
    z815_354721 = {
        {owner = "banish", banish = "green 815", draw = "003", pos = "H2 V4"},
        {owner = "return", pos = "H2 V5"}
    },
    z815_a2aef2 = {
        {banish = "green 815", pos = "H2 V1"}, {owner = "banish", pos = "H1 V6"}
    },
    z815_8ba0bc = {
        {banish = "green 815", pos = "H2 V1"},
        {drawKeyword = "stamina, will", pos = "H2 V3"},
        {owner = "banish", pos = "H1 V6"}
    },
    z818 = {{owner = "discard", draw = "3x001", pos = "H2 V6"}},
    z818_1736f3 = {
        {dealActiveState = "750", pos = "H2 V6.5"},
        {dealState = "106", pos = "H2.5 V7"}
    },
    z382_c89dd8 = {{}},
    z823 = {{owner = "satchel", draw = "3x003", pos = "H1 V6"}},
    z829 = {{}},
    z829_1d143f = {
        {banish = "2x green 829", pos = "H2 V1"},
        {owner = "banish", drawKeyword = "aggressiveness", pos = "H2 V5"},
        {owner = "banish", pos = "H2 V7"}
    },
    z829_78d26c = {
        {banish = "2x green 829", pos = "H2 V1"},
        {owner = "banish", draw = "003", pos = "H1 V5"}
    },
    z829_55cf42 = {
        {banish = "2x green 829", pos = "H2 V1"},
        {owner = "banish", draw = "050", pos = "H2 V4"},
        {owner = "banish", pos = "H2 V5"},
        {owner = "banish", drawKeyword = "stamina", pos = "H2 V7"}
    },
    z829_4514b2 = {
        {banish = "2x green 829", pos = "H2 V1"},
        {drawKeyword = "stamina", pos = "H1 V5"}, {draw = "134", pos = "H2 V6"},
        {owner = "banish", pos = "H1 V7"}
    },
    z829_08024d = {
        {banish = "2x green 829", pos = "H2 V1"},
        {owner = "banish", draw = "003", pos = "H2 V5"},
        {owner = "banish", dealActiveState = "108", pos = "H2 V6"}
    },
    z837 = {{}},
    z837_ef5b38 = {
        {banish = "green 837", pos = "H2 V1"},
        {drawKeyword = "serenity", pos = "H1 V4.5"},
        {dealState = "107", pos = "H1 V6"}, {owner = "banish", pos = "H1 V7"}
    },
    z837_afa78c = {
        {banish = "green 837", pos = "H2 V1"},
        {owner = "banish", draw = "003", pos = "H2 V4"},
        {owner = "banish", dealActiveState = "800", pos = "H2 V5"}
    },
    z837_a02daa = {
        {banish = "green 837", pos = "H2 V1"},
        {owner = "banish", draw = "350", pos = "H2 V4"},
        {owner = "banish", dealActiveState = "104", pos = "H2 V5"},
        {owner = "banish", drawKeyword = "serenity", pos = "H2 V7"}
    },
    z838 = {
        {draw = "2x003", pos = "H1 V2"}, {dealState = "103", pos = "H1 V3"},
        {dealState = "107", pos = "H1.5 V4"},
        {owner = "return", rst = "094", pos = "H1 V6"},
        {owner = "return", rst = "122", pos = "H2 V6"}
    },
    z846 = {{}},
    z846_58e077 = {
        {banish = "green 846", pos = "H2 V1"},
        {owner = "banish", draw = "003", pos = "H2 V5"}
    },
    z846_529cdb = {
        {banish = "green 846", pos = "H2 V1"},
        {owner = "banish", pos = "H1 V5"},
        {owner = "banish", draw = "750", pos = "H2 V7"}
    },
    z846_f370d2 = {
        {banish = "green 846", pos = "H2 V1"},
        {owner = "banish", draw = "5x003", pos = "H2 V4"}
    },
    z846_55766d = {
        {banish = "green 846", pos = "H2 V1"}, {owner = "banish", pos = "H1 V5"}
    },
    z849 = {
        {
            owner = "satchel",
            banishPlayerCard = "834_925fe7",
            draw = "4x001, 5x003, 804",
            pos = "H1 V6"
        }, {
            owner = "banish",
            banishPlayerCard = "834_925fe7",
            dealActiveState = "104, 105",
            pos = "H3 V6"
        }
    },
    z850 = {
        {owner = "discard", pos = "H1 V5"},
        {owner = "discard", dealActiveState = "104, 107", pos = "H2 V7"}
    },
    z850_441fc4 = {
        {owner = "discard", pos = "H1 V6"},
        {owner = "discard", dealActiveState = "102, 104", pos = "H3 V7"}
    },
    z850_9a4a6a = {
        {owner = "discard", dealActiveState = "103", pos = "H2 V5"},
        {owner = "discard", dealState = "107", pos = "H2 V7"}
    },
    z850_7753d6 = {
        {owner = "discard", draw = "003", pos = "H1 V7"},
        {owner = "discard", dealState = "101, 103", pos = "H3 V7"}
    },
    z852 = {
        {
            owner = "return",
            drawKeyword = "aggressiveness, serenity",
            pos = "H1.5 V7"
        }
    },
    z852_2f9ca0 = {
        {
            owner = "discard",
            banish = "green 852",
            dealActiveState = "108",
            pos = "H2 V7"
        }, {owner = "banish", draw = "2x001", pos = "H1.5 V6"}
    },
    z857 = {{draw = "855", discard = "green 855", pos = "H1 V4"}},
    z867 = {
        {owner = "discard", pos = "H2 V5"}, {dealState = "101", pos = "H2 V6"},
        {owner = "return", pos = "H3 V7"}
    },
    z870 = {{owner = "discard", draw = "590", pos = "H2 V7"}},
    z863 = {
        {
            owner = "banish",
            draw = "2x003",
            drawKeyword = "skill, will",
            pos = "H2 V5"
        }
    },
    z873 = {{owner = "banish", drawKeyword = "vigilance", pos = "H2 V7"}},
    z873_7d176e = {{owner = "banish", drawKeyword = "serenity", pos = "H2 V7"}},
    z873g = {
        {owner = "return", drawKeyword = "vigilance, serenity", pos = "H2 V6"}
    },
    z875 = {
        {owner = "return", rst = "902", pos = "H1 V6"},
        {owner = "return", pos = "H2.5 V7"}
    },
    z877 = {{owner = "return", pos = "H2 V7"}},
    z886 = {
        {draw = "3x003", pos = "H1 V3"},
        {draw = "855", discard = "green 855", pos = "H1 V6"}
    },
    z899 = {
        {
            owner = "banish",
            banish = "green 000",
            dealActiveState = "105",
            draw = "6x003",
            pos = "H1 V6"
        }
    },
    z908 = {{owner = "discard", draw = "753", pos = "H2 V7"}}
}

ITEMS_BONUSES_STATES = {
    z001 = {
        {returnAction = 3, owner = "return", pos = "H2.5 V5.5"},
        {returnAction = 6, owner = "return", pos = "H2.5 V6.5"}
    },
    z001_17ec5e = {
        {returnAction = 4, owner = "return", pos = "H2.5 V5.5"},
        {returnAction = 7, owner = "return", pos = "H2.5 V6.5"}
    },
    z001_2827ba = {
        {owner = "discard", fetch = "001", pos = "H1 V5.5"},
        {owner = "discard", pos = "H3 V6.5"}
    },
    z001_bdb6e1 = {{owner = "return", pos = "H2.5 V6"}},
    z002 = {
        {returnAction = 4, owner = "return", pos = "H2.5 V5.5"},
        {returnAction = 5, owner = "return", pos = "H2.5 V6.5"}
    },
    z002_64c5c3 = {
        {returnAction = 4, owner = "banish", draw = "589", pos = "H2.5 V5.5"},
        {returnAction = 5, owner = "banish", draw = "589", pos = "H2.5 V6.5"}
    },
    z051 = {
        {returnAction = 3, draw = "134", owner = "banish", pos = "V5"},
        {returnAction = 6, draw = "134", owner = "banish", pos = "V6"},
        {owner = "banish", pos = "V7"}
    },
    z061 = {{draw = "7x003", owner = "banish", pos = "H2 V6"}},
    z092 = {{owner = "discard", pos = "V6"}},
    z096 = {{owner = "banish", pos = "H2 V6"}},
    z100 = {{owner = "return", pos = "H2 V5.5"}},
    z101 = {{owner = "return", pos = "V6"}},
    z101_67bfab = {{owner = "return", pos = "V5"}, {draw = "750", pos = "V6"}},
    z102 = {{owner = "return", pos = "V6"}},
    z102_3545e5 = {
        {owner = "return", draw = "003", pos = "H3 V6"},
        {owner = "return", draw = "750", pos = "H3 V7"}
    },
    z103 = {
        {owner = "return", pos = "V5"}, {dealActiveState = "107", pos = "V6"}
    },
    z104 = {
        {owner = "return", pos = "V5", draw = "003"},
        {dealState = "105", pos = "V6"}
    },
    z104_801ec0 = {
        {owner = "return", pos = "V5", draw = "003"},
        {dealActiveState = "108", pos = "V6"}
    },
    z104_d7495e = {
        {owner = "return", pos = "V5"}, {dealState = "105", pos = "V6"}
    },
    z104_91f833 = {
        {owner = "return", pos = "V5"}, {dealState = "105", pos = "V6"}
    },
    z105 = {{owner = "return", pos = "V6"}},
    z106 = {{owner = "return", pos = "V6"}},
    z106_eb63de = {{owner = "return", pos = "V4"}},
    z107 = {{owner = "return", pos = "V5", draw = "003"}},
    z107_ae1226 = {{owner = "return", pos = "V5"}},
    z107_e740ad = {{owner = "return", pos = "V5"}},
    z107_c7cae9 = {
        {owner = "return", pos = "V5"},
        {owner = "discard", fetch = "107", pos = "V6"}
    },
    z108 = {{}},
    z108_9e7ff6 = {
        {owner = "return", draw = "003", pos = "H2 V5"},
        {owner = "return", fetch = "108", pos = "H2 V6"}
    },
    z117 = {{owner = "banish", dealActiveState = "092, 096", pos = "H2 V6"}},
    z118 = {{owner = "discard", pos = "H2 V6"}},
    z134 = {
        {owner = "discard", pos = "H2 V5"},
        {dealActiveState = "106", pos = "H1.5 V6"}
    },
    z135 = {
        {owner = "discard", pos = "H2.5 V6"},
        {owner = "discard", pos = "H1.5 V6"}
    },
    z150_383edd = {{owner = "banish", draw = "3x001", pos = "H2 V6"}},
    z150_015b19 = {
        {dealActiveState = "878", pos = "H3 V6"}, {draw = "150", pos = "H2 V6"},
        {owner = "banish", pos = "H2 V7"}
    },
    z172 = {{draw = "432", pos = "H2 V7"}},
    z172_9ca388 = {{owner = "banish", pos = "H2 V7"}},
    z210_1a6055 = {{owner = "discard", pos = "H2.5 V7"}},
    z219_efa40b = {{owner = "discard", pos = "H2 V6"}},
    z211_79c666 = {
        {draw = "357", pos = "H3 V2"}, {owner = "banish", pos = "H2.5 V7"}
    },
    z221 = {{owner = "discard", returnAction = 6, pos = "H2 V6"}},
    z224 = {
        {
            owner = "banish",
            returnAction = 30,
            returnState = "tired, hungry, frightened",
            pos = "V5"
        }
    },
    z267 = {{dealActiveState = "100", draw = "3x003", pos = "H1 V2"}},
    z277 = {{owner = "discard", draw = "336", pos = "H2 V7"}},
    z336 = {{owner = "return", pos = "H2 V5"}, {draw = "277", pos = "H2.5 V7"}},
    z310 = {{}},
    z310_79e740 = {
        {
            owner = "banish",
            returnAction = 30,
            returnState = "bloody, terrified, poisoned",
            pos = "V5"
        }
    },
    z310_edf693 = {
        {owner = "banish", returnAction = 4, pos = "H2 V5"},
        {owner = "discard", dealActiveState = "106", pos = "H2 V6.5"}
    },
    z322 = {{owner = "discard", pos = "H2 V6"}},
    z350 = {{}},
    z350_30a73d = {
        {draw = "350", pos = "H3 V3"}, {owner = "discard", pos = "H2 V7"}
    },
    z350_f3ff40 = {{draw = "357", pos = "H3 V2"}},
    z350_dfd7d6 = {{owner = "banish", pos = "H2.5 V7"}},
    z350_66f045 = {
        {
            owner = "discard",
            returnState = "injured, nauseated, poisoned",
            pos = "H2 V6"
        }
    },
    z350_dad9ae = {{owner = "banish", pos = "H2 V6"}},
    z350_aba756 = {{owner = "banish", draw = "586", pos = "H2 V6"}},
    z350_fa2539 = {{returnState = "freezing", pos = "H2 V7"}},
    z350_46fca9 = {{owner = "banish", pos = "H2.5 V6"}},
    z360 = {{owner = "banish", pos = "H2 V6"}},
    z382 = {{}},
    z382_3fc65b = {{owner = "discard", pos = "H2 V6.5"}},
    z382_96f2fd = {
        {owner = "banish", pos = "H3 V6"}, {owner = "banish", pos = "H2 V7"}
    },
    z561_e95e1e = {{draw = "463", pos = "H2 V7"}},
    z570 = {
        {dealActiveState = "800", pos = "H2 V6"},
        {owner = "banish", pos = "H2 V7"}
    },
    z571 = {{draw = "516", owner = "banish", pos = "H2.5 V5.5"}},
    z578 = {{}},
    z586 = {
        {drawKeyword = "curse", pos = "H2 V4"},
        {owner = "discard", pos = "H2 V6"}
    },
    z589 = {{}},
    z608_aea733 = {{draw = "657", pos = "H2 V6"}},
    z608_5d0ab1 = {{owner = "banish", draw = "648", pos = "H2 V6"}},
    z612 = {{owner = "discard", pos = "V6"}},
    z620 = {{owner = "discard"}, {owner = "discard", draw = "634", pos = "V7"}},
    z627_901cef = {{owner = "discard", pos = "H2 V7"}},
    z657 = {{owner = "return", pos = "H2 V6"}},
    z717 = {{owner = "banish", pos = "H2 V7"}},
    z725 = {{draw = "5x003", pos = "H1 V3"}},
    z739 = {{owner = "discard", pos = "H2 V7"}},
    z740 = {{owner = "banish", pos = "H1.5 V7"}},
    z750_8df38a = {{owner = "discard", pos = "H1 V7"}},
    z765 = {{owner = "return", returnAction = 2, pos = "H2 V6"}},
    z767 = {
        {owner = "banish", returnAction = 4, pos = "H1 V4"},
        {owner = "banish", draw = "814", pos = "V6"}
    },
    z795w = {
        {owner = "banish", draw = "807", pos = "H2 V5"},
        {owner = "banish", draw = "824", pos = "H2 V7"}
    },
    z800 = {{owner = "return", pos = "V6"}},
    z805_4078ca = {
        {
            owner = "banish",
            returnAction = 13,
            returnState = "tired, disheartened",
            pos = "V6"
        }
    },
    z814 = {
        {
            dealActiveState = "105",
            draw = "4x100",
            owner = "banish",
            pos = "H1 V3.5",
            return_past_if_necessary = true
        }, {draw = "765", pos = "V6"}
    },
    z829_fa5eca = {
        {banish = "2x green 829", pos = "H2 V1"},
        {owner = "banish", returnAction = 5, pos = "H2 V6"},
        {owner = "banish", pos = "H2 V7"}
    },
    z835 = {
        {owner = "banish", draw = "5x003", pos = "H1 V6"},
        {owner = "banish", pos = "H2 V7"}
    },
    z837_e2e0d2 = {
        {banish = "green 837", pos = "H2 V1"}, {draw = "003", pos = "V5"},
        {drawKeyword = "will", pos = "V6"}, {owner = "banish", pos = "V7"}
    },
    z906 = {{owner = "banish", pos = "H2 V7"}}
}

SATCHEL_CARDS = {
    z003 = {{owner = "return", pos = "H2 V6"}},
    z003g = {{}},
    z016 = {{}},
    z029 = {{}},
    z032 = {{}},
    z042 = {{draw = "3x003", pos = "H1 V5"}},
    z050 = {{}},
    z050_5cf3b0 = {{returnState = "poisoned", pos = "H2.5 V6.5"}},
    z050_48f0b1 = {{figurine = "fire", pos = "H2 V6"}},
    z050_a739b6 = {{returnState = "nauseated", pos = "H2 V6"}},
    z050_ed2aa7 = {{returnAction = 1, pos = "H2.5 V7"}},
    z050_4f1e51 = {{drawKeyword = "aggressiveness", pos = "H2 V6"}},
    z050_7363e9 = {{draw = "343", pos = "H1 V6", necessaryKeyword = "music"}},
    z050_0facb0 = {{dealActiveState = "108", pos = "H3 V6"}},
    z050_48052f = {
        {draw = "178", pos = "H1 V5", necessaryKeyword = "music"},
        {dealStateToAll = "101", pos = "V6.5", necessaryKeyword = "music"}
    },
    z050_172869 = {{draw = "257", pos = "H1 V7", necessaryKeyword = "music"}},
    z050_47fd37 = {
        {draw = "809", pos = "H1 V5.5", necessaryKeyword = "music"},
        {draw = "750", pos = "H1 V6.5", necessaryKeyword = "music"}
    },
    z058 = {{}},
    z058g = {{draw = "445", owner = "banish", pos = "H3 V7"}},
    z062 = {{}},
    z085 = {{}},
    z099 = {
        {returnState = "freezing", pos = "H2 V5"},
        {returnState = "frightened, terrified", pos = "H2 V6"},
        {owner = "discard", pos = "H1.5 V7"}
    },
    z128 = {{owner = "satchel", draw = "003", pos = "H2 V7"}},
    z129 = {{}},
    z144 = {{}},
    z153 = {{}},
    z166 = {
        {draw = "407", owner = "banish"}, {
            owner = "banish",
            rst = "218",
            banish = "237",
            draw = "7 x 003",
            returnAction = 30,
            pos = "H2 V6",
            banishClueCard = "vg, gem1, gem2"
        }
    },
    z171 = {{}},
    z172_8c4fc2 = {{owner = "banish", draw = "458", pos = "H1 V5"}},
    z172_44631a = {{owner = "banish", draw = "193", pos = "H2 V5"}},
    z172_72dfcc = {{}},
    z176 = {{}},
    z197 = {{}},
    z212 = {
        {draw = "4x003", pos = "H1 V6", owner = "satchel"},
        {dealState = "104", pos = "H3 V6"},
        {dealState = "107", owner = "discard", pos = "H3 V7"}
    },
    z229 = {{}},
    z247 = {{}},
    z248 = {{}},
    z248_ddeb26 = {
        {returnAction = 3, pos = "H2 V6"}, {owner = "discard", pos = "H2 V7"}
    },
    z260 = {{owner = "banish", pos = "H2 V7"}},
    z266 = {{dealState = "104"}, {fetch = "129", owner = "banish", pos = "V7"}},
    z274 = {{}},
    z283 = {{owner = "satchel", draw = "3x003", pos = "H3 V4"}},
    z308 = {{draw = "5x003", pos = "H3 V2.5"}},
    z350_b04b0c = {{}},
    z351 = {{draw = "3x003", pos = "H3", owner = "satchel"}},
    z357 = {{}},
    z366 = {
        {draw = "409", pos = "V5", owner = "banish"}, {
            draw = "7x003",
            returnAction = 30,
            pos = "H1.5 V6",
            banishClueCard = "offering",
            owner = "banish"
        }
    },
    z370 = {{}},
    z372 = {{}},
    z381 = {{owner = "satchel", draw = "3x003", pos = "H3 V4"}},
    z388 = {
        {owner = "discard", draw = "001", pos = "H2 V6"},
        {owner = "discard", pos = "H1 V7"}
    },
    z393 = {{dealState = "103"}, {draw = "266", owner = "banish", pos = "V7"}},
    z414 = {{}},
    z441 = {{}},
    z420 = {{draw = "3x003", owner = "satchel"}},
    z440 = {{draw = "3x003", owner = "satchel"}},
    z445 = {
        {returnState = "tired, freezing", pos = "H1.5 V3"},
        {dealState = "106", pos = "H1.5 V4.5"},
        {draw = "393", owner = "banish", pos = "V7"}
    },
    z455 = {{}},
    z464 = {
        {draw = "410", pos = "V5", owner = "banish"}, {
            draw = "7x003",
            returnAction = 30,
            pos = "H1.5 V6",
            banishClueCard = "icy_maze",
            owner = "banish"
        }
    },
    z480 = {
        {draw = "001", pos = "H1 V4"}, {dealState = "103, 104", pos = "H2 V5"},
        {draw = "003", pos = "H2 V6"}
    },
    z484 = {
        {draw = "001, 469", pos = "H2 V4"}, {dealState = "104", pos = "H3 V5"},
        {draw = "486", pos = "H2 V6"}, {dealActiveState = "104", pos = "H3 V7"}
    },
    z490 = {{draw = "4x003, 001", pos = "H2.5 V4", owner = "satchel"}},
    z531 = {{}},
    z545 = {
        {draw = "482", pos = "H1 V5"},
        {dealState = "103, 104, 108", pos = "H3 V6"},
        {draw = "472", pos = "H2 V7"}, {draw = "530", pos = "H3 V7"}
    },
    Z550 = {{draw = "2X003", pos = "H2 V7"}, {draw = "750", pos = "H3 V7"}},
    z550_f0ca99 = {{}},
    z552 = {{draw = "3x003"}},
    z562 = {{draw = "2x003", pos = "H1 V2"}},
    z583 = {{}},
    z585 = {
        {draw = "914", pos = "V4.5", owner = "banish"},
        {
            ret = "banished 000",
            returnAction = -1,
            pos = "H1.5 V6",
            owner = "banish"
        }
    },
    z589_607ec4 = {{}},
    z590 = {
        {draw = "715", pos = "H2 V4", "owner = banish"}, {
            draw = "7x003",
            rst = "396",
            returnAction = -1,
            pos = "H1 V6",
            banishClueCard = "veins",
            owner = "banish"
        }
    },
    z597 = {{}},
    z614 = {
        {draw = "411", owner = "banish"}, {
            owner = "banish",
            rst = "659",
            banish = "659",
            draw = "7x003",
            returnAction = 30,
            pos = "H2 V6",
            banishClueCard = "swamp"
        }
    },
    z623 = {{owner = "satchel", draw = "3 x 003", pos = "H2 V4"}},
    z653 = {{owner = "banish", pos = "H1 V7"}},
    z667w = {{returnAction = 2, pos = "H2 V6"}},
    z668 = {{draw = "w700", pos = "H1 V5"}},
    z669 = {{draw = "5x003", pos = "H1 V7"}},
    z674 = {{owner = "return", draw = "003, w700", rst = "w731", pos = "H1 V6"}},
    z682 = {
        {owner = "satchel", draw = "7x003, w700", rst = "w731", pos = "H1 V6"}
    },
    z683 = {
        {
            owner = "banish",
            draw = "683 + banner",
            banner = "gear2",
            pos = "H2.5 V7"
        }
    },
    z689 = {{}},
    z690w = {{returnAction = 2, pos = "H2 V6"}},
    z706 = {{}},
    z706w = {{returnAction = 2, pos = "H2 V6"}},
    z721 = {{}},
    z723w = {{returnAction = 2, pos = "H2 V6"}},
    z728 = {{draw = "7x003", pos = "H1 V1"}},
    z730 = {{}},
    z737 = {{}},
    z742 = {{}},
    z744w = {{}},
    z746 = {{}},
    z748w = {{returnAction = 2, pos = "H2 V6"}},
    z754 = {
        {returnActiveState = "poisoned", pos = "H1 V4"},
        {dealState = "107", pos = "H1 V5"}
    },
    z766 = {{}},
    z772g = {
        {
            rst = "123",
            banish = "801",
            dealState = "103, 104",
            draw = "797",
            ret = "banished 772",
            pos = "H2 V4"
        }
    },
    z780 = {{}},
    z786w = {{returnAction = 2, pos = "H2 V6"}},
    z787g = {
        {banish = "green 082, green 205, green 288, green 361", pos = "H2 V3"},
        {draw = "850", pos = "H3 V7"}
    },
    z792 = {{draw = "550", pos = "H1 V6"}, {draw = "750", pos = "H3 V7"}},
    z797 = {{owner = "return", ret = "banished 801", pos = "H1 V6"}},
    z801 = {{draw = "w700", rst = "balloonWhiteTerrainCard"}},
    z802 = {
        {owner = "banish", fetch = "802", pos = "H3 V3.5"},
        {
            owner = "banish",
            banish = "2x green 834",
            fetch = "834",
            pos = "H1 V7"
        }, {owner = "banish", pos = "H3 V7"}
    },
    z805g_a3590e = {
        {draw = "3x003", pos = "H1 V3"}, {owner = "banish", pos = "H1 V7"}
    },
    z806 = {{}},
    z806_819536 = {{owner = "banish", fetch = "806", pos = "H2 V4"}},
    z806_149a40 = {
        {owner = "banish", fetch = "806", pos = "H2 V4"},
        {owner = "banish", fetch = "806", pos = "H1 V7"}
    },
    z807 = {{draw = "2x003", pos = "H3 V7"}},
    z811 = {
        {owner = "banish", draw = "853", pos = "H1 V4"},
        {owner = "banish", draw = "828", pos = "H1 V7"}
    },
    z815g = {{draw = "846", pos = "H2 V7"}},
    z817 = {
        {owner = "discard", draw = "750", pos = "H2 V5"},
        {owner = "satchel", draw = "809", pos = "H3 V7"}
    },
    z819 = {
        {owner = "banish", fetch = "802", pos = "H1 V4"},
        {owner = "banish", fetch = "843", pos = "H1 V7"}
    },
    z820 = {
        {draw = "716", pos = "H2 V4", owner = "banish"}, {
            draw = "7x003",
            returnAction = 30,
            pos = "H1 V6",
            owner = "banish",
            banishClueCard = "beacon"
        }
    },
    z824 = {{draw = "7x003", pos = "H3 V7"}},
    z821 = {
        {owner = "banish", fetch = "821", pos = "H1 V4"},
        {owner = "banish", fetch = "856", pos = "H1 V5"}
    },
    z826 = {
        {owner = "banish", fetch = "841", pos = "H1 V4"},
        {owner = "banish", fetch = "806", pos = "H1 V7"}
    },
    z828 = {{owner = "banish", draw = "828", pos = "H1 V5"}},
    z829g = {{draw = "837", pos = "H2 V7"}},
    z831 = {
        {owner = "banish", fetch = "821", pos = "H2 V5"},
        {owner = "banish", fetch = "856", pos = "H2 V6"}
    },
    z832 = {{}},
    z834 = {{}},
    z834_36c490 = {
        {owner = "banish", fetch = "834", pos = "H1 V5"},
        {owner = "banish", fetch = "847", pos = "H3 V5"}
    },
    z834_94c0f7 = {
        {owner = "banish", fetch = "834", pos = "H2 V4"},
        {owner = "banish", fetch = "847", pos = "H1 V7"},
        {owner = "banish", pos = "H3 V7"}
    },
    z837g = {{draw = "815", pos = "H2 V7"}},
    z840 = {{}},
    z841 = {
        {owner = "banish", fetch = "806", pos = "H1 V4"},
        {owner = "banish", fetch = "841", pos = "H1 V7"}
    },
    z841g = {
        {
            owner = "banish",
            banishPlayerCard = "834_925fe7",
            draw = "2x001, 3x003, 804",
            pos = "H1 V6"
        }, {
            owner = "banish",
            banishPlayerCard = "834_925fe7",
            dealActiveState = "104, 750",
            pos = "H3 V6"
        }
    },
    z846g = {{}},
    z847 = {
        {owner = "banish", fetch = "847", pos = "H2 V4"},
        {owner = "banish", fetch = "834", pos = "H2 V7"}
    },
    z847g = {
        {
            draw = "2x003",
            dealActiveState = "819",
            banishPlayerCard = "150_015b19, 847_2cf381",
            pos = "H2.5 V3"
        }
    },
    z853 = {{owner = "banish", draw = "853", pos = "H2 V6"}},
    z853g = {
        {returnAction = 2, pos = "H1 V6"},
        {dealActiveState = "101", pos = "H1.5 V7"}
    },
    z856 = {{}},
    z856_e63bec = {
        {owner = "banish", fetch = "856", pos = "H1 V7"},
        {owner = "banish", fetch = "821", pos = "H1 V5"}
    },
    z856_5ca080 = {
        {owner = "banish", fetch = "856", pos = "H2 V4"},
        {owner = "banish", fetch = "821", pos = "H2 V7"}
    },
    z858 = {{}},
    z861 = {{draw = "829", pos = "H2 V7"}},
    z864 = {{owner = "banish", pos = "H1.5 V6"}},
    z874 = {{owner = "banish", pos = "H1.5 V6"}},
    z878 = {
        {owner = "banish", fetch = "834", pos = "H1 V4"},
        {owner = "banish", fetch = "847", pos = "H2 V7"}
    },
    z880 = {{draw = "7x003", pos = "H2 V1"}},
    z882 = {{owner = "banish", pos = "H1 V7"}},
    z885 = {{owner = "banish", pos = "H1 V7"}},
    z888 = {{draw = "699", pos = "H2.5 V7"}},
    z890 = {
        {draw = "2x003", pos = "H1.5 V2"}, {draw = "2x003", pos = "H1.5 V7"}
    },
    z892 = {
        {owner = "banish", pos = "H2 V4"},
        {returnState = "bloody", pos = "H1.5 V2"}
    },
    z895 = {{owner = "banish", draw = "3x003", pos = "H2 V6"}},
    z897 = {
        {draw = "2x003", pos = "H1.5 V2"}, {draw = "2x003", pos = "H1.5 V7"}
    },
    z905 = {{draw = "2x003", pos = "H1 V2"}, {draw = "2x003", pos = "H2 V7"}},
    z911 = {{}},
    z912 = {{}},
    z913 = {{}},
    z947w = {{returnAction = 2, pos = "H2 V6"}},
    z989w = {{returnAction = 2, pos = "H2 V6"}},
    z997w = {{returnAction = 2, pos = "H2 V6"}},
    z999 = {{}}
}

WEATHER_CARDS = {
    z505 = {{draw = "549", pos = "H3 V4"}, {dealState = "102", pos = "H1.5 V6"}},
    z510 = {{draw = "528", pos = "H2 V2"}},
    z515 = {{draw = "561", pos = "H2.5 V3"}},
    z520 = {{}},
    z525 = {{draw = "573", pos = "H2 V5"}},
    z700w = {{}},
    z700w_cc2c94 = {
        {
            owner = "discard",
            draw = "w700",
            pos = "H3 V6",
            return_past_if_necessary = true
        }
    },
    z700w_bd6bf0 = {
        {draw = "698", pos = "H2.5 V6"},
        {draw = "698 + banner", banner = "ball", pos = "H3.5 V6"}, {
            owner = "discard",
            draw = "w700",
            pos = "H3 V7",
            return_past_if_necessary = true
        }
    },
    z700w_02fc97 = {{dealActiveState = "106", pos = "H3 V6"}}
}

CARD_BACK_ACTIONS = {
    z011__back = {
        {fetch = "011 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z234__back = {
        {fetch = "234 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z235__back = {{fetch = "235 + banner", owner = "return", banner = "stone"}},
    z237__back = {
        {fetch = "237 + banner", owner = "return", banner = "vg", pos = "V6"}
    },
    z243__back = {
        {fetch = "243 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z269__back = {{fetch = "269 + banner", owner = "return", banner = "swamp"}},
    z273__back = {
        {fetch = "273 + banner", owner = "return", banner = "secretplace"}
    },
    z302__back = {{fetch = "gold", owner = "return"}},
    z345__back = {
        {fetch = "345 + banner", owner = "return", banner = "icy_maze"}
    },
    z354__back = {
        {fetch = "354 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z363__back = {{fetch = "363 + banner", owner = "return", banner = "vg"}},
    z417g__back = {
        {fetch = "417 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z417g_b88259__back = {
        {fetch = "417 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z438__back = {
        {fetch = "438 + banner", owner = "return", banner = "offering"}
    },
    z448__back = {{fetch = "448 + banner", owner = "return", banner = "vg"}},
    z466__back = {
        {fetch = "466 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z482__back = {
        {fetch = "482 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z538__back = {{fetch = "538 + banner", owner = "return", banner = "knife"}},
    z555__back = {
        {fetch = "555 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z570__back = {{fetch = "570 + banner", owner = "return", banner = "prison"}},
    z613__back = {
        {fetch = "613 + banner", owner = "return", banner = "torch", pos = "V5"}
    },
    z633__back = {
        {fetch = "633 + banner", owner = "return", banner = "torch", pos = "V5"}
    },
    z717__back = {{fetch = "717 + banner", owner = "return", banner = "prison"}},
    z741__back = {
        {fetch = "741 + banner", owner = "return", banner = "bloody_hunt"}
    },
    z767__back = {{fetch = "767 + banner", owner = "return", banner = "prison"}},
    z791w__back = {{fetch = "791 + banner", owner = "return", banner = "lamp"}},
    z799__back = {{fetch = "799 + banner", owner = "return", banner = "veins"}},
    z802g__back = {
        {dealActiveState = "811", banishPlayerCard = "847_2cf381", pos = "V6.5"}
    },
    z806g__back = {
        {dealActiveState = "831", banishPlayerCard = "834_925fe7", pos = "V5.5"}
    },
    z815g__back = {{draw = "832"}},
    z821g__back = {{banishPlayerCard = "806_02b163", pos = "V6"}},
    z829g__back = {{draw = "840"}},
    z834g__back = {
        {
            draw = "2x003",
            dealActiveState = "826",
            banishPlayerCard = "150_015b19, 847_2cf381",
            pos = "V6"
        }
    },
    z837g__back = {{draw = "858"}},
    z841g__back = {
        {
            fetch = "841 + banner",
            owner = "return",
            banner = "bloody_hunt",
            pos = "H2 V5.5"
        }
    },
    z843__back = {{banishPlayerCard = "847_2cf381", pos = "H2 V5.5"}},
    z846__back = {{draw = "911"}},
    z856g__back = {{banishPlayerCard = "806_02b163", pos = "V6"}}
}

EXPLORATION_PERMANENT_EVENTS = {
    exp1_bbfd12 = {
        {owner = "discard", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H2.5 V7"}
    },
    exp1_6531c5 = {
        {owner = "banish", draw = "001", pos = "H2 V3"},
        {owner = "discard", pos = "H1.5 V4.5"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp1_c31f66 = {{owner = "banish", pos = "H2 V6"}},
    exp1_d485e8 = {
        {owner = "banish", pos = "H2 V4"},
        {owner = "banish", figurine = "fire", pos = "H2.5 V5"},
        {owner = "banish", pos = "H2 V7"}
    },
    exp1_036166 = {
        {owner = "satchel", pos = "H2 V3"},
        {owner = "banish", draw = "001", pos = "H2 V5"},
        {owner = "discard", pos = "H2 V7"}
    },
    exp2_133ab9 = {
        {owner = "discard", draw = "003", pos = "H0.5 V6"},
        {owner = "discard", dealActiveState = "108", pos = "H3 V6"}
    },
    exp2_a81a67 = {
        {owner = "discard", pos = "H2 V6"},
        {owner = "discard", draw = "003", pos = "H1.5 V4"}
    },
    exp2_4cf85c = {
        {owner = "discard", pos = "H1.5 V4"},
        {owner = "discard", draw = "097", pos = "H1.5 V5"}
    },
    exp2_498d78 = {{owner = "discard", pos = "H2 V6"}},
    exp2_e6a68f = {
        {owner = "discard", pos = "H1 V4"},
        {owner = "discard", dealState = "104", pos = "H1.5 V6"}
    },
    exp2_f0b2b8 = {
        {owner = "discard", draw = "003, 221", pos = "H2.5 V3"},
        {dealState = "101", pos = "H2 V5"}, {dealState = "103", pos = "H2 V6"},
        {owner = "discard", dealState = "106", pos = "H2.5 V7"}
    },
    exp2_8ea492 = {
        {drawKeyword = "serenity, stamina", pos = "H2 V3"},
        {returnState = "tired, bloody", pos = "H2 V5"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp2_6ddca9 = {
        {owner = "discard", pos = "H2 V4"},
        {owner = "banish", drawKeyword = "vigilance, stealth", pos = "H1.5 V6"}
    },
    exp3_b0b5aa = {
        {owner = "banish", draw = "265", pos = "H2 V4"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp3_8b1e1f = {
        {owner = "discard", pos = "H2 V4"}, {dealState = "106", pos = "H2.5 V5"}
    },
    exp3_4154cb = {
        {owner = "discard", pos = "H1 V5"},
        {owner = "discard", dealState = "105", pos = "H2 V7"}
    },
    exp3_4f15d9 = {
        {owner = "discard", draw = "001", pos = "H2 V3"},
        {
            owner = "discard",
            draw = "001",
            dealActiveState = "104",
            pos = "H1 V5"
        }, {owner = "discard", draw = "003", pos = "H2 V7"}
    },
    exp3_88205d = {{owner = "discard", pos = "H2 V6"}},
    exp3_5541a9 = {{owner = "banish", draw = "001", pos = "H2 V3"}},
    exp3_ff46c4 = {
        {owner = "banish", draw = "350", pos = "H2 V3"},
        {owner = "banish", pos = "H2 V4"}
    },
    exp3_0b859e = {
        {owner = "discard", draw = "327", pos = "H2 V4"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp3_976581 = {
        {owner = "discard", dealState = "104", pos = "H2 V3"},
        {owner = "discard", draw = "003", pos = "H2 V5"},
        {dealState = "103", pos = "H2.5 V7"}
    },
    exp3_30b48b = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "banish", pos = "H1 V7"}, {owner = "banish", pos = "H3 V6"}
    },
    exp3_98503e = {
        {
            owner = "discard",
            returnAction = 5,
            returnState = "hungry",
            pos = "H2 V4"
        }, {
            owner = "discard",
            dealActiveState = "106",
            drawKeyword = "aggressiveness",
            pos = "H2 V6"
        }
    },
    exp3_a76411 = {
        {owner = "discard", pos = "H1 V5.5"},
        {dealActiveState = "103", pos = "H3 V6"}
    },
    exp4_4ef038 = {{owner = "discard", pos = "H1.5 V5"}},
    exp4_12df3e = {{owner = "discard", pos = "H1.5 V6"}},
    exp4_a3a09c = {
        {owner = "discard", pos = "H2 V2"}, {dealState = "103", pos = "H2 V5"},
        {dealState = "107", pos = "H2.5 V6"}, {draw = "001", pos = "H1.5 V6"}
    },
    exp4_5ef6d0 = {
        {owner = "discard", draw = "003", pos = "H2 V5"},
        {dealState = "103", pos = "H2.5 V7"}
    },
    exp4_105b42 = {
        {draw = "000", returnActiveState = "tired", pos = "H1.5 V5"},
        {owner = "discard", pos = "H1.5 V6"}
    },
    exp4_b03e2e = {
        {owner = "banish", draw = "350", pos = "H2 V3"},
        {owner = "banish", pos = "H2 V4"}
    },
    exp4_1d308d = {
        {dealState = "134", pos = "H1 V5.5"},
        {dealState = "108", pos = "H1.5 V6"},
        {owner = "discard", pos = "H1.5 V7"}
    },
    exp4_de63aa = {
        {owner = "banish", draw = "001", pos = "H1.5 V5"},
        {owner = "discard", draw = "2x001", pos = "H1.5 V5.5"},
        {owner = "banish", pos = "H1 V7"},
        {owner = "discard", draw = "001", pos = "H2 V7"}
    },
    exp4_5f0f78 = {
        {owner = "banish", draw = "571", pos = "H2 V4"},
        {
            owner = "banish",
            dealActiveState = "108",
            draw = "001",
            pos = "H2 V5.5"
        }, {owner = "discard", pos = "H2 V7"}
    },
    exp4_564d52 = {
        {owner = "discard", pos = "H2 V3"}, {owner = "satchel", pos = "H1 V6"},
        {dealActiveState = "108", pos = "H2.5 V7"}
    },
    exp5_7ba4bf = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "discard", draw = "003", pos = "H2.5 V5"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp6_f21b7a = {
        {owner = "discard", pos = "H2 V5"},
        {owner = "discard", dealActiveState = "104", pos = "H1.5 V6"}
    },
    exp6_a8785e = {
        {owner = "discard", drawKeyword = "will, serenity", pos = "H2 V5"},
        {owner = "discard", dealState = "103", pos = "H1 V7"}
    },
    exp6_500b26 = {
        {owner = "banish", draw = "2x150", pos = "H1 V4"},
        {owner = "banish", draw = "4x250", pos = "H2 V4"},
        {owner = "discard", dealState = "101", pos = "H2 V7"}
    },
    exp6_c1e2ec = {
        {draw = "000", returnActiveState = "tired", pos = "H2 V4"},
        {dealActiveState = "104", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2 V7"}
    },
    exp6_667eb4 = {
        {drawKeyword = "music", pos = "H2 V4"},
        {owner = "discard", drawKeyword = "serenity", pos = "H0.5 V7"}
    },
    exp6_f88662 = {
        {dealActiveState = "104", pos = "H2 V6"},
        {owner = "banish", pos = "H1.5 V7"}
    },
    exp6_76cd68 = {
        {draw = "219", pos = "H1.5 V5"}, {owner = "discard", pos = "H1.5 V6.5"}
    },
    exp6_0c45c9 = {
        {owner = "discard", figurine = "fire", pos = "H1.5 V5"},
        {owner = "discard", pos = "H2 V7"}
    },
    exp6_2656d3 = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "banish", pos = "H1 V7"}, {owner = "banish", pos = "H3 V6"}
    },
    exp7_25a0f7 = {
        {owner = "discard", pos = "H1 V5"},
        {owner = "discard", draw = "097", pos = "H1 V5"}
    },
    exp7_7c0a16 = {
        {returnAction = 3, owner = "discard", pos = "H2 V4"},
        {dealActiveState = "106", owner = "discard", pos = "H2 V5"},
        {dealActiveState = "107", owner = "discard", pos = "H1 V5.5"},
        {dealActiveState = "105", owner = "discard", pos = "H3 V5.5"},
        {dealActiveState = "108", owner = "discard", pos = "H2 V6"}
    },
    exp7_f20cd2 = {
        {owner = "discard", draw = "210", pos = "H2 V4"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp7_f548a9 = {{owner = "discard", pos = "H2 V7"}},
    exp7_a7c1da = {
        {owner = "banish", draw = "350", pos = "H1.5 V5"},
        {owner = "banish", pos = "H1.5 V6"}
    },
    exp7_589a14 = {
        {owner = "discard", pos = "H1 V5"},
        {owner = "discard", dealActiveState = "104", pos = "H2.5 V6"}
    },
    exp7_6c2981 = {
        {owner = "discard", pos = "H2.5 V3.5"},
        {returnState = "bloody", dealState = "102", pos = "H2 V5"},
        {owner = "discard", pos = "H2.5 V6.5"}
    },
    exp7_e7e072 = {
        {owner = "discard", draw = "003", pos = "H1.5 V5.5"},
        {owner = "discard", draw = "2x003", pos = "H2 V6"},
        {owner = "banish", draw = "3x003", pos = "H1.5 V6.5"}
    },
    exp7_db42c9 = {
        {owner = "discard", pos = "H1 V5"}, {owner = "discard", pos = "H2 V6"}
    },
    exp8_297b53 = {
        {owner = "discard", draw = "003", pos = "H2 V5.5"},
        {owner = "discard", draw = "223", pos = "H2 V6.5"}
    },
    exp8_1a5418 = {
        {returnState = "bloody", pos = "H2 V4"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp8_df8016 = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "banish", draw = "2x001", pos = "H1 V6"},
        {dealActiveState = "103", pos = "H3 V6"},
        {owner = "banish", pos = "H2 V7"}
    },
    exp8_f6a6c4 = {
        {owner = "discard", pos = "H2 V5"}, {owner = "banish", pos = "H1.5 V7"}
    },
    exp8_8eea41 = {
        {owner = "discard", draw = "003", pos = "H0.5 V5"},
        {rst = "288", pos = "H2 V6.5"}
    },
    exp9_ed247e = {
        {owner = "discard", draw = "003", pos = "H2 V4"},
        {dealState = "102", pos = "H2 V6"}, {owner = "discard", pos = "H2 V7"}
    },
    exp9_87f468 = {
        {owner = "discard", draw = "003", pos = "H1.5 V6"},
        {owner = "discard", draw = "354", pos = "H1.5 V7"}
    },
    exp9_ee29de = {
        {owner = "discard", pos = "H3 V5"}, {dealState = "101", pos = "H3 V6"}
    },
    exp9_1b2331 = {
        {returnState = "tired", pos = "H1 V4"},
        {returnState = "frightened", pos = "H2 V5"},
        {owner = "discard", pos = "H2 V7"}
    },
    exp9_5e7786 = {
        {owner = "banish", draw = "350", pos = "H2 V3"},
        {owner = "banish", pos = "H2 V5"}
    },
    exp9_b2af74 = {
        {owner = "discard", draw = "2x003", pos = "H2 V5"},
        {owner = "discard", dealActiveState = "106", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "101", pos = "H2 V6.5"}
    },
    exp9_48ff1c = {{owner = "discard", pos = "H2 V5"}},
    exp10_6a9e2b = {
        {owner = "banish", draw = "2x200", pos = "H1 V4"},
        {owner = "banish", draw = "4x300", pos = "H2 V4"},
        {owner = "discard", pos = "H1 V5.5"}, {owner = "discard", pos = "H2 V7"}
    },
    exp10_ef34b3 = {
        {owner = "discard", pos = "H1 V6"},
        {dealActiveState = "107", pos = "H3 V6"}
    },
    exp10_4666ad = {
        {owner = "discard", pos = "H2 V4.5"},
        {owner = "discard", returnAction = 7, pos = "H2 V6"}
    },
    exp10_c1eb61 = {
        {owner = "discard", pos = "H2 V4.5"},
        {owner = "discard", returnAction = 7, pos = "H2 V6"}
    },
    exp10_72830e = {
        {owner = "discard", pos = "H2 V4.5"},
        {owner = "discard", returnAction = 7, pos = "H2 V6"}
    },
    exp10_117330 = {
        {owner = "discard", pos = "H2 V4.5"},
        {owner = "discard", returnAction = 7, pos = "H2 V6"}
    },
    exp10_6dfb8f = {
        {owner = "discard", draw = "003", pos = "H1 V6"},
        {owner = "discard", draw = "498", pos = "H2.5 V6"}
    },
    exp10_bbd507 = {
        {owner = "discard", draw = "003", pos = "H2 V4"},
        {draw = "470", pos = "H2.5 V6.5"}
    },
    exp10_afe89c = {{owner = "discard", pos = "H2 V5.5"}},
    exp10_37499a = {
        {owner = "discard", pos = "H2 V4.5"}, {owner = "discard", pos = "H2 V6"}
    },
    exp10_2eb2ab = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "banish", pos = "H1 V7"}, {owner = "banish", pos = "H3 V6"}
    },
    exp11_fb1e54 = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "banish", pos = "H1 V7"}, {owner = "banish", pos = "H3 V6"}
    },
    exp11_b767c1 = {
        {owner = "discard", pos = "H1.5 V5"}, {draw = "772", pos = "H3 V6"}
    },
    exp11_513878 = {
        {owner = "discard", pos = "H2 V5.5"},
        {owner = "discard", pos = "H1.5 V7"}
    },
    exp11_cfd25e = {
        {owner = "discard", pos = "H2 V4"},
        {owner = "discard", draw = "003", pos = "H2 V6"},
        {draw = "772", pos = "H3 V6"}
    },
    exp11_19f4c6 = {
        {owner = "return", pos = "H2.5 V4"}, {owner = "return", pos = "H1.5 V6"}
    },
    exp11_3962f3 = {
        {owner = "return", pos = "H2.5 V4"},
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", draw = "772", pos = "H3 V6"}
    },
    expc_c70f41 = {
        {owner = "satchel", pos = "H1 V5"}, {owner = "banish", pos = "H2 V7"}
    },
    expc_34502e = {
        {owner = "satchel", pos = "H0.5 V7"},
        {owner = "banish", pos = "H2.5 V7"}
    },
    expc_865ff5 = {
        {owner = "satchel", pos = "H1 V6"}, {owner = "banish", pos = "H2.5 V6"}
    },
    expc_f7361c = {
        {owner = "banish", draw = "576", pos = "H1 V5.5"},
        {owner = "banish", draw = "576", pos = "H2.5 V5.5"},
        {owner = "banish", pos = "H2 V7"}
    },
    expc_44f9c2 = {
        {owner = "satchel", draw = "2x001", pos = "H1 V5"},
        {owner = "banish", dealActiveState = "104, 105", pos = "H2.5 V5"},
        {owner = "banish", pos = "H2 V7"}
    }
}

EXPLORATION_TEMP_EVENTS = {
    exp1_a28baa = {
        {owner = "discard", pos = "H1 V7"},
        {dealActiveState = "104", pos = "H2 V6"},
        {dealActiveState = "108", pos = "H2 V7"}
    },
    exp1_34660f = {
        {owner = "discard", draw = "003", pos = "H1.5 V4"},
        {dealState = "101", pos = "H3 V6"}, {dealState = "103", pos = "H1.5 V7"}
    },
    exp1_eca34c = {{owner = "banish", pos = "H1.5 V6"}},
    exp1_c8e95e = {{owner = "discard", pos = "H2 V6"}},
    exp1_4a77b1 = {{owner = "discard", draw = "045", pos = "H1 V7"}},
    exp1_5b1d34 = {
        {
            owner = "discard",
            draw = "515",
            figurine = "Astral Mist",
            pos = "H2 V6"
        }
    },
    exp1_d8cc01 = {
        {owner = "banish", draw = "003", pos = "H1.5 V5"},
        {owner = "discard", drawKeyword = "aggressiveness", pos = "H1.5 V7"}
    },
    exp1_bd0ff5 = {
        {
            owner = "discard",
            draw = "484",
            figurine = "Juvenile Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp1_e70d87 = {
        {owner = "return", returnAction = 1, pos = "H1.5 V5.5"},
        {owner = "return", returnAction = 3, pos = "H2.5 V5.5"},
        {owner = "banish", pos = "H1 V7"}
    },
    exp1_5b773c = {
        {owner = "banish", pos = "H1 V6"}, {owner = "return", pos = "H1 V7"}
    },
    exp2_3c97e9 = {
        {
            owner = "discard",
            draw = "484",
            figurine = "Juvenile Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp2_6996be = {
        {
            owner = "discard",
            draw = "480",
            figurine = "Adult Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp2_317873 = {
        {figurine = "Flying Root", pos = "H2 V3"},
        {owner = "discard", pos = "H1 V5.5"},
        {owner = "discard", pos = "H1 V7"},
        {owner = "discard", dealActiveState = "103, 104", pos = "H2 V7"}
    },
    exp2_ea8369 = {
        {owner = "discard", draw = "001", pos = "H1.5 V6"},
        {dealState = "101", pos = "H2 V7"}
    },
    exp2_7276d7 = {
        {owner = "banish", draw = "296", pos = "H2 V5"},
        {owner = "banish", pos = "H2 V7"}
    },
    exp2_d1a44e = {
        {
            owner = "discard",
            draw = "505",
            figurine = "Thunderstorm",
            pos = "H2 V6"
        }
    },
    exp2_c27185 = {
        {owner = "banish", draw = "297", pos = "H3 V3"},
        {owner = "banish", draw = "270", pos = "H3 V7"}
    },
    exp2_c7fabc = {
        {
            owner = "discard",
            draw = "520",
            figurine = "Snow Storm",
            pos = "H2 V6"
        }
    },
    exp2_5aed4a = {
        {owner = "discard", dealState = "100", pos = "H1 V5"},
        {owner = "discard", dealState = "103", pos = "H2 V5"},
        {owner = "discard", drawKeyword = "serenity", pos = "H2.5 V6"}
    },
    exp2_558c99 = {{owner = "banish", pos = "H1 V7"}},
    exp2_c89f1f = {
        {owner = "banish", pos = "H1 V7"}, {dealState = "108", pos = "H3 V6"}
    },
    exp2_bef295 = {
        {owner = "discard", draw = "003", pos = "H1.5 V6"},
        {owner = "discard", dealState = "108", pos = "H2.5 V7"}
    },
    exp2_2dd222 = {{owner = "banish", draw = "050", pos = "H2 V4"}},
    exp2_733abe = {{draw = "145", pos = "H3 V7"}},
    exp2_ea7d7f = {
        {draw = "000", pos = "H1.5 V4"},
        {returnState = "tired", pos = "H1.5 V5"},
        {dealState = "101", pos = "H3 V6"}, {dealState = "102", pos = "H2 V7"}
    },
    exp2_0b4127 = {{owner = "discard", dealState = "101", pos = "H2 V4"}},
    exp2_921ed7 = {
        {owner = "discard", draw = "317", pos = "H2 V4"},
        {owner = "discard", pos = "H2 V6"}
    },
    exp2_d86c21 = {
        {drawKeyword = "vigilance", owner = "discard", pos = "H2 V4"}
    },
    exp2_7abb4e = {{owner = "banish", draw = "172", pos = "H2 V3"}},
    exp2_bb4c54 = {{owner = "discard", dealActiveState = "750", pos = "H3 V6"}},
    exp2_28179f = {
        {
            owner = "banish",
            draw = "003",
            returnActiveState = "nauseated",
            pos = "H2 V5.5"
        }, {owner = "banish", dealActiveState = "103", pos = "H2 V7"}
    },
    exp3_92723c = {
        {
            owner = "discard",
            draw = "484",
            figurine = "Juvenile Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp3_98c60a = {
        {owner = "discard", draw = "510", figurine = "Fog", pos = "H2.5 V4"}
    },
    exp3_62c415 = {{owner = "discard", draw = "097", pos = "H1.5 V6"}},
    exp3_3bed03 = {{owner = "discard", draw = "001", pos = "H2.5 V5"}},
    exp3_66bacd = {
        {dealActiveState = "105", pos = "H2.5 V5"},
        {owner = "discard", draw = "003", pos = "H2 V6"},
        {owner = "discard", dealState = "101", pos = "H2.5 V7"}
    },
    exp3_870b8d = {
        {owner = "discard", draw = "001", pos = "H2.5 V5"},
        {owner = "discard", dealActiveState = "104", pos = "H2 V7"}
    },
    exp3_f1e0b1 = {
        {owner = "discard", dealActiveState = "100", pos = "H2.5 V4"},
        {owner = "discard", dealActiveState = "108", pos = "H2.5 V6"}
    },
    exp3_9f5e17 = {
        {owner = "discard", returnState = "tired", pos = "H2 V4"},
        {draw = "000", pos = "H2.5 V 5"},
        {owner = "discard", dealState = "101", pos = "H2 V6"}
    },
    exp3_a9405d = {
        {owner = "discard", draw = "003", pos = "H1.5 V5"},
        {returnState = "bloody", pos = "H2 V6"},
        {dealState = "102", pos = "H3 V7"}
    },
    exp3_8a216a = {{draw = "145", pos = "H3 V7"}},
    exp3_4fecb8 = {{owner = "banish", draw = "578", pos = "H2.5 V5"}},
    exp4_12b5ee = {
        {owner = "banish", pos = "H1 V5"}, {owner = "return", pos = "H1 V7"}
    },
    exp4_92bf05 = {
        {owner = "discard", draw = "003", pos = "H1.5 V5"},
        {owner = "discard", draw = "097", pos = "H1.5 V6"}
    },
    exp4_5285c7 = {{owner = "discard", dealState = "108", pos = "H2.5 V6"}},
    exp4_bea29a = {
        {
            owner = "discard",
            draw = "484",
            figurine = "Juvenile Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp4_611c9c = {
        {
            owner = "discard",
            draw = "480",
            figurine = "Adult Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp4_01a716 = {
        {returnState = "tired", pos = "H3 V4"},
        {returnAction = 2, pos = "H2 V4"}, {dealState = "2x101", pos = "H2 V7"}
    },
    exp4_f16d6f = {
        {owner = "discard", draw = "003", pos = "H1 V6"},
        {owner = "discard", dealState = "104", pos = "H2.5 V7"}
    },
    exp4_d9daa2 = {
        {
            owner = "discard",
            draw = "003",
            drawKeyword = "skill, vigilance",
            pos = "H2 V5"
        }, {owner = "discard", dealState = "104", pos = "H2 V7"}
    },
    exp4_ca400a = {
        {owner = "discard", draw = "001", pos = "H1.5 V5"},
        {owner = "discard", dealState = "104", pos = "H2 V6"}
    },
    exp4_d27805 = {
        {owner = "discard", returnState = "bloody", pos = "H1.5 V3"},
        {owner = "discard", drawKeyword = "vigilance", pos = "H2 V6"}
    },
    exp4_18cf4f = {
        {dealState = "101", pos = "H2.5 V6"}, {owner = "discard", pos = "H2 V7"}
    },
    exp4_ecfaf5 = {
        {owner = "banish", draw = "277", pos = "H3 V3"},
        {owner = "banish", draw = "297", pos = "H3 V7"}
    },
    exp4_18b048 = {
        {owner = "discard", pos = "H2 V4"},
        {owner = "discard", draw = "558", pos = "H2 V5"}
    },
    exp4_7601ac = {
        {dealActiveState = "104", pos = "H1 V3"},
        {dealActiveState = "105", pos = "H3 V3"},
        {owner = "banish", pos = "H1.5 V6"}, {owner = "return", pos = "H1.5 V7"}
    },
    exp5_5b2e34 = {{owner = "banish", draw = "3x003", pos = "H2 V4"}},
    exp5_942c87 = {{owner = "discard", draw = "003", pos = "H1 V6"}},
    exp5_426365 = {
        {owner = "discard", dealState = "106", pos = "H2.5 V4"},
        {owner = "discard", draw = "003", pos = "H2 V4.5"}
    },
    exp5_ef0aa1 = {
        {returnState = "disheartened", pos = "H3 V3"},
        {returnState = "frightened", pos = "H1 V3"},
        {drawKeyword = "will", pos = "H3 V4"}
    },
    exp6_f117ba = {
        {
            owner = "discard",
            draw = "545",
            figurine = "Giant Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp6_629423 = {
        {
            owner = "discard",
            draw = "480",
            figurine = "2 x Adult Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp6_ed0f3f = {
        {
            owner = "discard",
            drawKeyword = "stealth, serenity",
            pos = "H2.5 V5.5"
        }
    },
    exp6_38f116 = {
        {owner = "discard", draw = "525", figurine = "Tornado", pos = "H2.5 V6"}
    },
    exp6_3706a4 = {{draw = "145", pos = "H3 V7"}},
    exp6_330dfb = {
        {owner = "banish", draw = "050", pos = "H1.5 V4"},
        {owner = "banish", drawKeyword = "stamina", pos = "H1.5 V6"}
    },
    exp6_76817a = {
        {draw = "003", pos = "H2.5 V4"}, {dealState = "101", pos = "H3 V5"}
    },
    exp6_b1b0e1 = {
        {owner = "discard", dealState = "101", pos = "H1 V4"},
        {owner = "discard", pos = "H2 V5"},
        {owner = "discard", dealState = "101, 104", pos = "H2 V6"}
    },
    exp6_b93ce3 = {
        {owner = "banish", draw = "2x001", pos = "H1 V7"},
        {owner = "discard", dealActiveState = "104", pos = "H2.5 V7"}
    },
    exp6_0278ea = {
        {owner = "discard", dealActiveState = "101", pos = "H1 V5"},
        {owner = "discard", dealActiveState = "106", pos = "H2 V5"},
        {owner = "discard", drawKeyword = "stamina", pos = "H2 V6"}
    },
    exp6_46cab8 = {
        {owner = "discard", draw = "003", pos = "H2 V4"},
        {owner = "discard", draw = "001", pos = "H2 V5"},
        {owner = "banish", draw = "003", pos = "H1.5 V7"}
    },
    exp6_e8c570 = {
        {
            owner = "discard",
            draw = "505",
            figurine = "Thunderstorm",
            pos = "H2 V6"
        }
    },
    exp6_582f59 = {{owner = "discard", drawKeyword = "serenity", pos = "H2 V5"}},
    exp6_e2c7e7 = {{owner = "discard", returnState = "tired", pos = "H2 V4"}},
    exp6_54ed7d = {
        {owner = "return", pos = "H2 V5"},
        {owner = "return", returnAction = 2, pos = "H2 V6"},
        {owner = "banish", dealActiveState = "750", pos = "H2 V7"}
    },
    exp7_8e6378 = {
        {owner = "discard", draw = "001", pos = "H2.5 V4"},
        {dealState = "104", pos = "H2.5 V6"}
    },
    exp7_3885a6 = {
        {owner = "discard", draw = "003", pos = "H1.5 V5.5"},
        {dealActiveState = "103", pos = "H2 V6"}
    },
    exp7_c0d14a = {
        {owner = "discard", draw = "2x003", pos = "H1.5 V4.5"},
        {dealState = "103", pos = "H2 V6"}
    },
    exp7_d36d6a = {
        {owner = "banish", draw = "630", pos = "H1 V5.5"},
        {
            owner = "banish",
            draw = "630 + banner",
            banner = "relic",
            pos = "H1.5 V6"
        }
    },
    exp7_6146fb = {
        {owner = "discard", draw = "510", figurine = "Fog", pos = "H2.5 V4"}
    },
    exp7_bfca83 = {
        {dealActiveState = "800", pos = "H1 V4"},
        {owner = "banish", draw = "003", pos = "H1 V7"},
        {owner = "return", pos = "H2.5 V7"}
    },
    exp7_e8d0eb = {
        {owner = "return", pos = "H1.5 V4.5"},
        {owner = "banish", draw = "2x003", pos = "H1 V7"}
    },
    exp8_3c9506 = {
        {owner = "banish", draw = "001, 275", pos = "H3 V3.5"},
        {owner = "discard", dealState = "106", pos = "H3 V6"}
    },
    exp8_6ec51e = {
        {owner = "discard", draw = "003", pos = "H1.5 V5.5"},
        {dealActiveState = "107", pos = "H2 V6"},
        {discardAction = 4, pos = "H2.5 V7"}
    },
    exp8_9d904a = {{draw = "145", pos = "H3 V7"}},
    exp8_1f5cd7 = {{dealState = "101", pos = "H1.5 V5.5"}},
    exp8_2f8976 = {{owner = "banish", draw = "001", pos = "H2 V5"}},
    exp8_1e8000 = {
        {owner = "discard", draw = "003", pos = "H0.5 V6"},
        {owner = "discard", dealState = "104, 105", pos = "H2 V7"}
    },
    exp8_fe7c29 = {
        {owner = "discard", dealActiveState = "108", pos = "H2 V3"},
        {owner = "discard", draw = "003", pos = "H1.5 V5.5"},
        {owner = "return", dealActiveState = "108", pos = "H2 V7"}
    },
    exp8_47c05e = {
        {owner = "discard", drawKeyword = "will", pos = "H2 V5"},
        {dealState = "103", pos = "H2 V6"}
    },
    exp8_28a6cd = {
        {owner = "discard", draw = "003", pos = "H2 V3"},
        {owner = "discard", dealState = "106", pos = "H1 V5"}
    },
    exp8_821476 = {
        {
            owner = "discard",
            draw = "545",
            figurine = "Giant Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp8_144ce0 = {
        {
            owner = "discard",
            draw = "480",
            figurine = "Adult Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp8_86db0b = {
        {owner = "discard", draw = "525", figurine = "Tornado", pos = "H2.5 V6"}
    },
    exp8_9b2106 = {
        {dealState = "101", pos = "H0.5 V5"},
        {drawKeyword = "stamina", pos = "H1 V5.5"}
    },
    exp8_9e4ac9 = {
        {owner = "banish", pos = "H1 V6"}, {owner = "return", pos = "H1 V7"}
    },
    exp9_b1226e = {{draw = "145", pos = "H3 V7"}},
    exp9_8326a3 = {{owner = "discard", draw = "003", pos = "H1.5 V6"}},
    exp9_8c7017 = {
        {owner = "discard", dealActiveState = "102", pos = "H2.5 V7"}
    },
    exp9_cbf034 = {{owner = "discard", dealState = "102", pos = "H1.5 V4"}},
    exp9_24f3b6 = {
        {owner = "discard", draw = "003", pos = "H3 V4"},
        {owner = "banish", draw = "3x003", pos = "H3 V5.5"},
        {owner = "return", dealActiveState = "104, 105", pos = "H2 V7"}
    },
    exp9_76e248 = {
        {owner = "discard", draw = "003", pos = "H1.5 V4"},
        {owner = "discard", dealActiveState = "2x104", pos = "H2.5 V6"}
    },
    exp9_b35f13 = {
        {
            owner = "discard",
            draw = "545",
            figurine = "Giant Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp9_9d503d = {
        {
            owner = "discard",
            draw = "480",
            figurine = "Adult Rockworm",
            pos = "H2.5 V5"
        }
    },
    exp9_0fd9a3 = {
        {owner = "discard", draw = "2x001", pos = "H1.5 V6"},
        {owner = "discard", dealState = "106", pos = "H2.5 V6"},
        {owner = "banish", pos = "H1.5 V7"}
    },
    exp9_1c56cd = {
        {
            owner = "discard",
            draw = "520",
            figurine = "Snow Storm",
            pos = "H2 V6"
        }
    },
    exp9_121b49 = {
        {
            owner = "discard",
            draw = "515",
            figurine = "Astral Mist",
            pos = "H2 V6"
        }
    },
    exp9_d444d9 = {
        {owner = "discard", pos = "H2 V4.5"},
        {owner = "discard", dealState = "101, 102", pos = "H2 V6"}
    },
    exp9_336135 = {
        {owner = "discard", dealState = "800", pos = "H2.5 V5.5"},
        {owner = "discard", drawKeyword = "serenity", pos = "H2 V7"}
    },
    exp9_9ee507 = {
        {owner = "banish", draw = "2x150", pos = "H1 V4"},
        {owner = "banish", draw = "3x250", pos = "H2 V4"}
    },
    exp10_068319 = {{dealState = "106", pos = "H3 V4"}},
    exp10_6bd078 = {
        {owner = "discard", draw = "003", drawKeyword = "skill", pos = "H2 V4"},
        {owner = "discard", draw = "470", pos = "H2 V6"}
    },
    exp10_35ddc7 = {
        {owner = "banish", draw = "3x002", pos = "H1 V5"},
        {owner = "discard", pos = "H2 V5.5"}, {owner = "discard", pos = "H2 V7"}
    },
    exp10_86fc36 = {
        {dealActiveState = "102", pos = "H2 V2.5"},
        {owner = "discard", drawKeyword = "aggressiveness", pos = "H2 V5"},
        {
            owner = "discard",
            draw = "470",
            dealActiveState = "103",
            pos = "H2 V7"
        }
    },
    exp10_c06991 = {
        {owner = "banish", draw = "001", pos = "H2.5 V5"},
        {owner = "discard", dealState = "106", pos = "H2.5 V7"}
    },
    exp10_3c128e = {
        {returnState = "tired", pos = "H2 V4"},
        {owner = "discard", dealState = "102", pos = "H1.5 V5"}
    },
    exp10_c602da = {
        {
            owner = "discard",
            draw = "003",
            returnState = "paranoid",
            pos = "H2 V5"
        }, {owner = "discard", dealState = "103", pos = "H2 V6"},
        {owner = "discard", dealState = "107", pos = "H2.5 V7"}
    },
    exp10_f8a279 = {
        {draw = "000", returnState = "tired", pos = "H2.5 V4"},
        {owner = "discard", dealState = "100", pos = "H2 V6"},
        {owner = "discard", pos = "H2 V5"}
    },
    exp10_672aab = {
        {owner = "discard", draw = "002", pos = "H1.5 V6"},
        {owner = "discard", dealStateToAll = "108", pos = "H2 V7"}
    },
    exp10_a42e58 = {
        {owner = "banish", pos = "H1.5 V4.5"},
        {owner = "banish", draw = "476", pos = "H2 V5"}
    },
    exp10_2db40b = {
        {returnState = "tired", pos = "H2 V3"},
        {owner = "discard", drawKeyword = "aggressiveness", pos = "H1 V5.5"}
    },
    exp11_290d1e = {
        {draw = "w700", pos = "H1.5 V3"},
        {drawKeyword = "serenity", pos = "H2 V4"},
        {drawKeyword = "aggressiveness", pos = "H2 V5.5"}
    },
    exp11_061c68 = {
        {discardAction = 2, pos = "H2 V4"},
        {dealState = "102", pos = "H3 V5.5"}, {owner = "return", pos = "H1 V6"}
    },
    exp11_ab8d1d = {{draw = "685", pos = "H2 V2"}},
    exp11_e295ab = {{draw = "685", pos = "H2 V2"}},
    exp11_519b6f = {{draw = "685", pos = "H2 V2"}},
    exp11_f35862 = {
        {
            owner = "discard",
            draw = "787",
            drawKeyword = "vigilance",
            pos = "H1 V6.5"
        }
    },
    exp11_3df9fe = {{draw = "000, w700", rst = "w680", pos = "H2 V5.5"}},
    exp11_b89a3a = {{owner = "return", pos = "H1.5 V6"}},
    exp11_0c6812 = {
        {owner = "discard", drawKeyword = "vigilance", pos = "H1 V5.5"},
        {owner = "discard", dealActiveState = "750", pos = "H2.5 V7"}
    },
    exp11_7464e4 = {
        {owner = "return", drawKeyword = "serenity, stamina", pos = "H2 V3"},
        {owner = "return", pos = "H1.5 V7"}
    },
    exp11_76a686 = {
        {owner = "banish", pos = "H2 V6"}, {owner = "return", pos = "H1 V7"}
    },
    exp11_896c40 = {
        {owner = "discard", draw = "001", pos = "H3 V5.5"},
        {owner = "discard", dealState = "106", pos = "H2.5 V7"}
    },
    exp11_1e696c = {{dealActiveState = "104", pos = "H1.5 V4"}},
    exp11_20b6a7 = {
        {owner = "discard", drawKeyword = "serenity, stamina", pos = "H2 V4"},
        {owner = "discard", dealActiveState = "750", pos = "H1.5 V5.5"}
    },
    exp11_15ce82 = {
        {owner = "discard", draw = "3x001", pos = "H1 V5"},
        {dealState = "104, 105", pos = "H2.5 V6"},
        {dealState = "800", pos = "H1 V7.5"}
    },
    exp12_06b798 = {{draw = "097", pos = "H1 V7"}},
    exp12_212d56 = {
        {owner = "discard", dealActiveState = "134", pos = "H3 V5.5"}
    },
    exp12_60dce9 = {
        {
            owner = "discard",
            draw = "000",
            returnState = "tired",
            pos = "H1.5 V6"
        }
    },
    exp12_a71db9 = {{owner = "discard", pos = "H2 V5"}},
    exp12_e028f9 = {{dealActiveState = "104", pos = "H2.5 V7"}},
    exp12_bdf0b1 = {
        {returnActiveState = "nauseated", pos = "H2.5 V6"},
        {dealActiveState = "106", pos = "H2.5 V7"}
    },
    exp12_315981 = {
        {owner = "discard", drawKeyword = "serenity, will", pos = "H2.5 V5"},
        {owner = "discard", dealActiveState = "103", pos = "H1 V7"},
        {owner = "discard", dealActiveState = "107", pos = "H3 V7"}
    },
    exp12_62cbd2 = {
        {owner = "discard", draw = "003", pos = "H1 V5.5"},
        {owner = "discard", dealActiveState = "104, 105", pos = "H1.5 V7.5"}
    },
    exp12_c60c7b = {{owner = "discard", dealActiveState = "104", pos = "H2 V7"}},
    exp12_1d4ddb = {
        {
            owner = "discard",
            draw = "003",
            drawKeyword = "aggressiveness",
            pos = "H1 V6"
        }, {owner = "discard", dealActiveState = "800", pos = "H2.5 V7"}
    },
    exp12_82949b = {
        {owner = "discard", draw = "003", pos = "H1 V5"},
        {owner = "discard", dealActiveState = "800", pos = "H1.5 V7"}
    },
    exp12_1ba288 = {
        {owner = "discard", pos = "H2 V3"}, {owner = "discard", pos = "H2 V6"},
        {owner = "discard", dealActiveState = "800", pos = "H2 V7"}
    },
    exp12_5f6c68 = {
        {owner = "discard", draw = "001", pos = "H1 V6.5"},
        {owner = "discard", dealActiveState = "103", pos = "H3 V6"},
        {owner = "discard", dealActiveState = "107", pos = "H2.5 V7"}
    },
    exp12_caa0ee = {
        {owner = "discard", draw = "2x001", pos = "H1.5 V6"},
        {owner = "discard", dealActiveState = "106", pos = "H2 V7"}
    },
    exp12_03c117 = {
        {owner = "discard", pos = "H1 V5.5"},
        {owner = "discard", draw = "852", pos = "H2.5 V5.5"}
    },
    expc_12bda1 = {
        {owner = "banish", pos = "H2.5 V5.5"},
        {owner = "banish", dealActiveState = "101", pos = "H2.5 V7"}
    },
    expc_a8ca30 = {
        {owner = "satchel", pos = "H2 V5.5"}, {owner = "banish", pos = "H2 V7"}
    },
    expc_de5c84 = {
        {owner = "banish", dealState = "106", pos = "H3 V4"},
        {owner = "banish", drawKeyword = "stamina", pos = "H2 V5"}
    },
    expc_717161 = {
        {owner = "banish", pos = "H2 V6"}, {draw = "559", pos = "H3 V7"}
    }
}

EXPLORATION_ITEMS_BONUSES_STATES = {
    exp1_fbbc64 = {{}},
    exp1_d9fe3e = {{owner = "banish", draw = "420", pos = "H1.5 V5"}},
    exp1_4c0215 = {{owner = "banish", pos = "H3 V7"}},
    exp1_ed512b = {{owner = "discard", pos = "H3 V7"}},
    exp2_83ac76 = {{owner = "banish", draw = "283", pos = "H1.5 V5"}},
    exp2_6cdc41 = {
        {owner = "discard", draw = "134", returnAction = 5, pos = "H1 V6"}
    },
    exp2_a01789 = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp3_3d5757 = {{owner = "banish", draw = "351", pos = "H1.5 V4.5"}},
    exp3_0e75b7 = {
        {owner = "discard", draw = "134", returnAction = 5, pos = "H1 V6"}
    },
    exp3_3f1f16 = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp3_ff4bd0 = {
        {owner = "banish", pos = "H1.5 V6.5"},
        {owner = "banish", pos = "H2.5 V6.5"}
    },
    exp4_17b124 = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp4_695cfc = {{}},
    exp4_a00742 = {{owner = "banish", draw = "381", pos = "H3 V4"}},
    exp4_dc448d = {{owner = "banish", pos = "H2.5 V7"}},
    exp4_688176 = {{owner = "banish", pos = "H2 V7"}},
    exp5_482c2f = {{}},
    exp5_afdd13 = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp5_66a9bf = {
        {owner = "banish", draw = "3x003", returnAction = 10, pos = "H2 V5"}
    },
    exp5_5a9c35 = {
        {owner = "banish", draw = "7x003", pos = "H1 V7"},
        {dealActiveState = "107", pos = "H3 V6"},
        {dealState = "103", pos = "H2.5 V7"}
    },
    exp6_b15f2e = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp6_22caf3 = {
        {owner = "banish", pos = "H1.5 V6.5"},
        {owner = "banish", pos = "H2.5 V6.5"}
    },
    exp6_2b1e52 = {{owner = "banish", draw = "623", pos = "H0.5 V7"}},
    exp7_6075c6 = {
        {owner = "discard", draw = "2x003", pos = "H1 V6"},
        {dealActiveState = "105", pos = "H3 V6"}
    },
    exp7_a3c9b7 = {{owner = "discard", pos = "H2 V6"}},
    exp7_3f9b6c = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp7_2d519e = {{owner = "banish", draw = "552", pos = "H1 V4"}},
    exp7_3ef740 = {{owner = "discard", pos = "H2 V6"}},
    exp8_72ea20 = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"},
        {owner = "banish", draw = "322", pos = "H2 V7"}
    },
    exp8_1c9c1a = {{owner = "banish", returnAction = 8, pos = "H2.5 V6"}},
    exp8_9b2370 = {{owner = "banish", draw = "440", pos = "H2 V5"}},
    exp8_f3e7fb = {{owner = "return", pos = "H2.5 V6"}},
    exp8_253ca2 = {
        {owner = "banish", pos = "H1.5 V6.5"},
        {owner = "banish", pos = "H2.5 V6.5"}
    },
    exp9_8749dc = {{owner = "discard", pos = "H2.5 V6"}},
    exp9_8fd639 = {{}},
    exp9_4bb4d7 = {{owner = "discard", pos = "H2 V7"}},
    exp9_8fd639 = {
        {owner = "banish", draw = "7x003", returnAction = 15, pos = "H1 V6"}
    },
    exp11_096145 = {
        {owner = "banish", draw = "3x003", returnAction = 10, pos = "H1 V6"},
        {owner = "banish", draw = "7x003", returnAction = 10, pos = "H2 V6"}
    },
    exp11_7a0fe6 = {
        {owner = "discard", pos = "H1.5 V6"},
        {owner = "discard", pos = "H2.5 V6"}
    },
    exp12_6260cc = {
        {returnAction = 2, pos = "H1 V5.5"},
        {owner = "banish", draw = "3x003", pos = "H1 V6.5"}
    }
}

EXPLORATION_SATCHEL_CARDS = {
    exp7_dfed30 = {
        {owner = "banish", draw = "318", pos = "H1.5 V5.5"},
        {owner = "banish", draw = "318", pos = "H1 V7"}
    },
    exp2_036330 = {{}},
    exp11_a07505 = {{owner = "discard", draw = "050", pos = "H2 V7.5"}}
}

EXPLORATION_WEATHER_CARDS = {
    exp11_b7295b = {
        {draw = "772", pos = "H3 V6"}, {
            owner = "discard",
            draw = "w700",
            pos = "H2 V7.5",
            return_past_if_necessary = true
        }
    }
}

ZONE_CONNECTIONS = {
    -- n, s, e, w.  zone id
    z004 = {1, 0, 0, 1},
    z006 = {1, 0, 1, 0},
    z007 = {0, 1, 0, 0},
    z009 = {0, 1, 1, 1},
    z010 = {1, 0, 0, 1},
    z010g = {1, 1, 0, 1},
    z014 = {1, 0, 1, 0},
    z017 = {0, 0, 1, 0},
    z017g = {0, 0, 1, 0},
    z024 = {0, 0, 1, 0},
    z038 = {2, 0, 0, 0},
    z043 = {5, 0, 0, 0},
    z048 = {0, 0, 1, 0},
    z054 = {2, 2, 2, 2},
    z056 = {0, 0, 1, 1},
    z057 = {2, 0, 2, 2},
    z059 = {0, 2, 2, 2},
    z059g = {0, 2, 2, 2},
    z066 = {2, 0, 2, 2},
    z072 = {2, 0, 2, 0},
    z083 = {0, 0, 2, 0},
    z083g = {0, 0, 2, 0},
    z086 = {0, 7, 7, 7},
    z087 = {2, 0, 2, 2},
    z094 = {2, 0, 0, 2},
    z095 = {0, 0, 0, 1},
    z111 = {0, 2, 0, 2},
    z112 = {7, 0, 7, 0},
    z120 = {0, 0, 2, 2},
    z122 = {0, 2, 2, 0},
    z123 = {3, 3, 3, 3},
    z125 = {0, 0, 1, 1},
    z131 = {2, 0, 2, 2},
    z132 = {0, 0, 2, 2},
    z136 = {2, 2, 2, 2}, -- one version doesn't have west
    z140 = {7, 7, 0, 7},
    z141 = {3, 3, 3, 3},
    z142 = {2, 2, 0, 2},
    z143 = {3, 0, 0, 3},
    z149 = {0, 0, 1, 1},
    z151 = {0, 0, 7, 0},
    z151g = {0, 0, 7, 7},
    z161 = {0, 0, 3, 0},
    z163 = {2, 2, 2, 0},
    z175 = {2, 0, 2, 0},
    z179 = {0, 0, 7, 0},
    z182 = {8, 8, 0, 0},
    z183 = {0, 0, 0, 0},
    z184 = {4, 0, 4, 4},
    z186 = {0, 0, 0, 1},
    z187 = {6, 6, 0, 6},
    z191 = {3, 3, 0, 3},
    z191g = {3, 3, 0, 3},
    z196 = {7, 0, 0, 7},
    z198 = {0, 0, 0, 1},
    z203 = {3, 3, 3, 3},
    z205 = {6, 6, 0, 0},
    z208 = {6, 0, 6, 6},
    z214 = {0, 3, 3, 0},
    z218 = {0, 0, 6, 0},
    z220 = {0, 3, 3, 3},
    z241 = {1, 0, 0, 1},
    z254 = {0, 0, 1, 1},
    z258 = {0, 5, 0, 0},
    z258g = {5, 5, 0, 0},
    z259 = {6, 6, 6, 6},
    z259g = {6, 6, 6, 6},
    z262 = {6, 6, 6, 0},
    z264 = {0, 3, 3, 3},
    z271 = {0, 0, 8, 8},
    z281 = {6, 6, 0, 6},
    z284 = {0, 6, 6, 0},
    z284w = {0, 0, 11, 11},
    z288 = {0, 8, 0, 8},
    z290 = {0, 0, 9, 9},
    z302g = {0, 4, 0, 4},
    z303 = {0, 9, 0, 9},
    z311 = {6, 6, 0, 6},
    z312 = {0, 1, 0, 0},
    z313 = {8, 8, 0, 0},
    z319 = {6, 6, 0, 0},
    z320 = {9, 0, 9, 9},
    z321 = {4, 0, 0, 0},
    z326 = {8, 8, 8, 0},
    z328 = {0, 0, 0, 0},
    z331 = {9, 9, 0, 0},
    z334 = {0, 9, 9, 9},
    z338 = {0, 4, 4, 4},
    z339 = {0, 0, 8, 8},
    z341 = {0, 0, 6, 6},
    z346 = {0, 0, 0, 9},
    z348 = {8, 0, 0, 8},
    z361 = {0, 0, 9, 9},
    z364 = {9, 0, 0, 9},
    z365 = {0, 8, 0, 0},
    z365g = {0, 8, 0, 0},
    z368 = {0, 4, 4, 0},
    z374 = {4, 0, 4, 0},
    z377 = {5, 5, 0, 0},
    z382_c89dd8 = {0, 0, 0, 0},
    z387 = {4, 0, 0, 4},
    z389 = {0, 0, 9, 0},
    z395 = {0, 8, 0, 0},
    z396 = {0, 4, 4, 0},
    z397 = {0, 0, 0, 4},
    z402 = {0, 0, 4, 0},
    z423 = {0, 5, 0, 0},
    z423g = {0, 5, 0, 0},
    z426 = {0, 4, 0, 4},
    z427 = {4, 4, 0, 0},
    z442 = {0, 4, 0, 0},
    z446 = {4, 4, 4, 4},
    z446g = {4, 4, 4, 4},
    z449 = {10, 10, 10, 10},
    z449g = {0, 0, 0, 0},
    z452 = {0, 4, 0, 0},
    z459 = {0, 5, 0, 0},
    z475 = {10, 10, 10, 10},
    z478 = {10, 0, 10, 10},
    z497 = {10, 10, 10, 10},
    z499 = {10, 10, 10, 10},
    z504 = {4, 0, 4, 0},
    z504g = {4, 0, 4, 0},
    z514 = {0, 0, 4, 4},
    z514g = {0, 0, 4, 4},
    z517 = {0, 0, 4, 0},
    z527 = {4, 0, 4, 0},
    z539 = {4, 0, 0, 0},
    z546 = {4, 4, 0, 4},
    z546g = {4, 4, 0, 4},
    z564 = {0, 0, 4, 4},
    z575 = {0, 0, 0, 4},
    z577 = {0, 0, 0, 4},
    z577g = {0, 4, 0, 4},
    z580 = {0, 0, 0, 4},
    z588 = {4, 4, 0, 4},
    z595 = {0, 0, 4, 4},
    z602 = {0, 0, 7, 7},
    z609 = {0, 7, 7, 7},
    z613 = {5, 5, 0, 0},
    z618 = {0, 0, 4, 4},
    z622 = {5, 5, 0, 0},
    z626 = {0, 4, 0, 0},
    z628 = {7, 0, 0, 7},
    z631 = {0, 7, 0, 0},
    z633 = {5, 5, 0, 0},
    z635 = {4, 0, 4, 0},
    z640 = {0, 5, 0, 0},
    z641 = {7, 0, 7, 0},
    z642 = {5, 5, 0, 0},
    z654 = {7, 7, 7, 0},
    z656 = {4, 4, 0, 4},
    z662 = {4, 4, 0, 0},
    z483 = {0, 0, 5, 0},
    z485 = {0, 0, 0, 0},
    z494 = {0, 0, 0, 0},
    z664 = {0, 7, 0, 0},

    -- n, s, e, w.  zone id
    z361g = {0, 0, 9, 9},
    z793 = {0, 0, 0, 5},
    z205g = {0, 6, 0, 6},
    z833g = {5, 0, 5, 0},
    z758 = {0, 0, 12, 0},
    z735 = {7, 7, 0, 7},
    z871g = {0, 12, 0, 0},
    z872 = {0, 0, 0, 0},
    z720w = {11, 11, 0, 0},
    z879 = {0, 0, 12, 0},
    z326w = {0, 11, 0, 11},
    z702w = {11, 11, 0, 0},

    z673w = {0, 0, 11, 11},
    z782 = {0, 4, 0, 0},
    z789w = {11, 0, 11, 0},
    z729w = {11, 11, 0, 11},
    z680w = {11, 11, 11, 0},
    z798 = {0, 0, 7, 7},
    z710w = {0, 0, 0, 11},
    z692w = {0, 0, 11, 11},
    z833 = {0, 0, 5, 0},
    z760 = {4, 0, 0, 0},

    z876 = {0, 12, 0, 0},
    z694 = {7, 0, 7, 7},
    z738w = {11, 11, 11, 0},
    z774 = {0, 5, 5, 0},
    z704 = {0, 0, 0, 5},
    z768g = {11, 0, 0, 11},
    z731w = {0, 11, 11, 11},
    z004w = {11, 0, 0, 0},
    z904 = {0, 5, 0, 0},
    z860g = {12, 0, 0, 0},
    z893 = {0, 0, 5, 5},
    z719 = {3, 3, 0, 3},

    z677 = {0, 0, 5, 0},
    z778g = {0, 0, 12, 0},
    z755 = {0, 0, 0, 7},
    z684 = {5, 5, 0, 0},
    z710 = {5, 0, 0, 0},
    z587w = {11, 0, 0, 0},
    z334w = {0, 0, 11, 0},
    z881 = {0, 12, 0, 0},
    z708w = {0, 11, 11, 11},
    z123w = {0, 11, 11, 11},
    z675w = {0, 0, 11, 11},
    z773 = {4, 4, 0, 0},

    z677w = {11, 11, 0, 0},
    z752 = {0, 0, 5, 5},
    z896 = {0, 0, 5, 5},
    z288g = {0, 0, 0, 8},
    z140 = {7, 7, 0, 7},
    z851 = {0, 0, 12, 0},
    z844 = {0, 5, 0, 5},
    z745 = {0, 0, 12, 0},
    z756w = {11, 0, 11, 11},
    z866 = {0, 0, 12, 0},
    z560 = {0, 0, 0, 5},

    z132w = {0, 0, 11, 11},
    z803g = {0, 0, 12, 0},
    z735w = {11, 0, 0, 0},
    z676w = {11, 11, 0, 0},
    ----
    z015 = {0, 0, 0, 0},
    z088 = {0, 0, 0, 0},
    z076 = {0, 0, 0, 0},
    z067 = {0, 0, 0, 0},
    z252 = {0, 0, 0, 0},
    z252g = {0, 0, 0, 0},
    z168 = {0, 0, 0, 0},
    z154 = {0, 0, 0, 0},
    z304 = {0, 0, 0, 0},
    z376 = {0, 0, 0, 0},
    z428 = {0, 0, 0, 0},
    z418 = {0, 0, 0, 0},
    z253 = {0, 0, 0, 0},
    z306 = {0, 0, 0, 0},
    z415 = {0, 0, 0, 0},
    z390 = {0, 0, 0, 0},
    z778 = {0, 0, 0, 0},
    z855g = {0, 0, 0, 0},
    z865 = {5, 0, 0, 5},
    z763 = {0, 0, 0, 0},
    z901 = {0, 0, 0, 0},
    z902 = {0, 0, 0, 0},
    z731 = {0, 0, 0, 0},
    z536 = {0, 0, 0, 0},
    z659 = {0, 0, 0, 0},
    z659g = {0, 0, 0, 0},
    z355 = {0, 0, 0, 0},
    z511 = {0, 0, 0, 0},
    z522g = {0, 0, 0, 0},
    z533 = {0, 0, 0, 0},
    z542 = {0, 0, 0, 0},
    z610 = {0, 0, 0, 0}
}

function onSave()
    savedData = JSON.encode({
        EXPLORE_CARDS_PLAYED = EXPLORE_CARDS_PLAYED,
        NUM_PLAYERS = NUM_PLAYERS,
        PLAYER_NAME = PLAYER_NAME,
        CARDS_IN_ZONE_MAP = CARDS_IN_ZONE_MAP,
        CARDS_IN_ZONE_DRAW = CARDS_IN_ZONE_DRAW,
        BANNERS = BANNERS,
        balloonGreenCard = balloonGreenTerrainCard,
        balloonWhiteCard = balloonWhiteTerrainCard,
        activeWeatherCard = activeWeatherCard,
        SETUP_FINISHED = SETUP_FINISHED
    })
    return savedData
end

function onLoad(savedData)
    if savedData ~= "" then
        local loadedData = JSON.decode(savedData)
        EXPLORE_CARDS_PLAYED = loadedData.EXPLORE_CARDS_PLAYED
        CARDS_IN_ZONE_MAP = loadedData.CARDS_IN_ZONE_MAP
        CARDS_IN_ZONE_DRAW = loadedData.CARDS_IN_ZONE_DRAW
        NUM_PLAYERS = loadedData.NUM_PLAYERS
        PLAYER_NAME = loadedData.PLAYER_NAME
        BANNERS = loadedData.BANNERS
        balloonGreenTerrainCard = loadedData.balloonGreenCard
        balloonWhiteTerrainCard = loadedData.balloonWhiteCard
        activeWeatherCard = loadedData.activeWeatherCard
        SETUP_FINISHED = loadedData.SETUP_FINISHED
        loadPlayerMats()

        if SETUP_FINISHED then
            Wait.time(function()
                for _, obj in ipairs(getAllObjects()) do
                    if obj.tag == "Card" then
                        local name = getNameOfExploreCard(obj)
                        local exploreCard =
                            string.find(obj.getDescription(), "exploration")
                        local flip = (obj.getRotation()).z
                        if (not exploreCard and flip == 180) or
                            (exploreCard and flip == 0) then
                            Wait.time(function()
                                setupAdvCardButtons(obj)
                            end, 0.1)
                        elseif CARD_BACK_ACTIONS[name .. "__back"] then
                            createSpecialActionButtons(obj)
                        end
                    end
                end
            end, 2)
        end
    else
        EXPLORE_CARDS_PLAYED = {}
        CARDS_IN_ZONE_MAP = {}
        CARDS_IN_ZONE_DRAW = {}
        NUM_PLAYERS = 4
        PLAYER_NAME = {
            "Character 1", "Character 2", "Character 3", "Character 4"
        }
        BANNERS = {
            raft = {guid = "76227f, 79ee1a", numbox = 5},
            fullhead = {guid = "11b2e5", numbox = 9},
            emptyhead = {guid = "11b2e5", numbox = 6},
            feather = {guid = "e4a0b3", numbox = 10},
            gear1 = {guid = "8c6b49", numbox = 6},
            gear2 = {guid = "90c948", numbox = 6},
            vine = {guid = "d8a570, 1a6055", numbox = 10},
            torch = {guid = "fe98a4, 38e782, 57a442, 740809", numbox = 9},
            lamp = {guid = "57a442", numbox = 29},
            wind = {guid = "6a34de", numbox = 21},
            horns = {guid = "383edd", numbox = 20},
            bottle = {guid = "c16cee", numbox = 15},
            knife = {guid = "51cf26", numbox = 9},
            shield = {guid = "dfd7d6", numbox = 4},
            stone = {guid = "a6a2cf", numbox = 63},
            relic = {guid = "4c47b8", numbox = 6},
            secretplace = {guid = "869304", numbox = 13},
            gem1 = {guid = "ce39a6", numbox = 9},
            gem2 = {guid = "b624b8", numbox = 9},
            ball = {guid = "78f4d1", numbox = 0},
            -- curse card guids are available after the curse cards are setup
            offering = {guid = "", numbox = 9},
            bloody_hunt = {guid = "", numbox = 8},
            vg = {guid = "", numbox = 7},
            sanctuary = {guid = "", numbox = 12},
            icy_maze = {guid = "", numbox = 10},
            swamp = {guid = "", numbox = 11},
            veins = {guid = "", numbox = 14},
            prison = {guid = "", numbox = 13},
            beacon = {guid = "", numbox = 0},
            repentance = {guid = "", numbox = 0}
        }
        balloonGreenTerrainCard = ""
        balloonWhiteTerrainCard = ""
        activeWeatherCard = ""
        SETUP_FINISHED = false
    end

    SPECIAL_ACTIONS = {{}}
    for k, v in pairs(CARD_BACK_ACTIONS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(PERMANENT_EVENTS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(TEMP_EVENTS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(ITEMS_BONUSES_STATES) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(SATCHEL_CARDS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(WEATHER_CARDS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(EXPLORATION_PERMANENT_EVENTS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(EXPLORATION_TEMP_EVENTS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(EXPLORATION_ITEMS_BONUSES_STATES) do
        SPECIAL_ACTIONS[k] = v
    end
    for k, v in pairs(EXPLORATION_SATCHEL_CARDS) do SPECIAL_ACTIONS[k] = v end
    for k, v in pairs(EXPLORATION_WEATHER_CARDS) do SPECIAL_ACTIONS[k] = v end

    local setupZones = {"ZONE_MAP", "ZONE_SATCHEL"}
    for _, zone in ipairs(setupZones) do
        local zoneObjects = getObjectFromGUID(ZONES[zone]['guid']).getObjects()
        for k, obj in pairs(zoneObjects) do
            if obj.name == "Card" then
                if obj.getDescription() == "green" or obj.getDescription() ==
                    "gold" or obj.getDescription() == "white" then
                    createAdvButtons(obj)
                end
                createSpecialActionButtons(obj)
            end
        end
    end

end

function addTrailingZeros(num)
    out = tostring(num)
    if num < 100 then
        out = "0" .. out
        if num < 10 then out = "0" .. out end
    end

    return out
end

function setExpansionOrCurse(params)
    local expansionOrCurseCode = params[1]
    local include_curse = params[2]

    local is_non_curse_expansion = false
    if include_curse then
        for _, nonCurseExpansionCode in ipairs(NON_CURSE_EXPANSIONS) do
            if expansionOrCurseCode == nonCurseExpansionCode then
                is_non_curse_expansion = true
                break
            end
        end
    end

    -- This function sets the expansion and/or curses up automatically
    -- An expansion or curse code is the first argument, and the second argument is whether we should include that expansion's curse or just the regular expansion's cards without the curse
    local bagExpansions = getObjectFromGUID(BAG_EXPANSIONS_GUID)
    local fullExpansionOrCurseName =
        EXPANSIONS_AND_CURSES_NAMES[expansionOrCurseCode]

    -- The "Finish Setup" button becomes available when a curse is first included
    if not FINISH_SETUP_BUTTON_AVAILABLE and include_curse and
        not is_non_curse_expansion then
        FINISH_SETUP_BUTTON_AVAILABLE = true
        local finishSetupButton = getObjectFromGUID(FINISH_SETUP_BUTTON_GUID)
        if finishSetupButton then
            finishSetupButton.setPosition({-17.00, 1.6, -31.30})
            finishSetupButton.setRotation({0, 180, 0})
            finishSetupButton.setColorTint({1, 1, 1, 1})
        end
    end

    for _, object in ipairs(bagExpansions.getObjects()) do
        local objectName = object.name
        if string.find(objectName, fullExpansionOrCurseName) then
            if string.find(objectName, "Advanced Skills") then
                bagExpansions.takeObject({
                    position = ADVANCED_SKILL_DECK_POS,
                    rotation = {0, 180, 180},
                    guid = object.guid
                })
            elseif string.find(objectName, "Adventure Cards") or
                string.find(objectName, "Skills") then
                bagExpansions.takeObject({
                    position = PAST_POS,
                    rotation = {0, 180, 0},
                    guid = object.guid
                })
            elseif string.find(objectName, "Tokens") then
                bagExpansions.takeObject({
                    position = EXPANSION_TOKENS_POS,
                    rotation = {0, 180, 0},
                    guid = object.guid
                })
                EXPANSION_TOKENS_POS[1] = EXPANSION_TOKENS_POS[1] + 3.09
            elseif include_curse then
                if string.find(objectName, "Curse") then
                    bagExpansions.takeObject({
                        position = PAST_POS,
                        rotation = {0, 180, 0},
                        guid = object.guid
                    })
                elseif string.find(objectName, "Clue") or
                    string.find(objectName, "Pocket Watch") then
                    local clueCard = bagExpansions.takeObject({
                        position = SATCHEL_POS,
                        rotation = {0, 180, 180},
                        guid = object.guid
                    })
                    if BANNERS[expansionOrCurseCode] then
                        BANNERS[expansionOrCurseCode].guid = clueCard.guid
                    end
                end
            elseif not string.find(objectName, "Curse") and
                not string.find(objectName, "Clue") then
                bagExpansions.takeObject({
                    position = SATCHEL_POS,
                    rotation = {0, 180, 180},
                    guid = object.guid
                })
            end
        end
    end

    if expansionOrCurseCode == "crystals_song_pnp" and not DELETED_WHAT_GOES_UP then
        -- If the player picked the "Crystal's Song PnP" curse, delete every item associated with the "What Goes Up" expansion as they are incompatible
        for _, curseName in ipairs(WHAT_GOES_UP_CURSES) do
            for _, guid in ipairs(EXPANSIONS_AND_CURSES[curseName]) do
                local object = getObjectFromGUID(guid)
                if object then object.destruct() end
            end
        end

        DELETED_WHAT_GOES_UP = true
        PICKED_CRYSTALS_SONG = true
        printToAll(
            "Crystal's Song added to your game. When setting up terrain cards, one crystal Exploration Card will replace one of the normal Exploration Cards that would have been placed instead. You may manually swap it with one of the other Exploration Cards placed if you wish when that happens.")
    else
        is_what_goes_up_curse = false

        if expansionOrCurseCode == "crystals_song" then
            PICKED_CRYSTALS_SONG = true
            is_what_goes_up_curse = true
            printToAll(
                "Crystal's Song added to your game. When setting up terrain cards, one crystal Exploration Card will replace one of the normal Exploration Cards that would have been placed instead. You may manually swap it with one of the other Exploration Cards placed if you wish when that happens.")
        end

        if not DELETED_CRYSTALS_PNP then
            -- Check whether the curse is from the "What Goes Up" expansion
            if not is_what_goes_up_curse then
                for _, curseName in ipairs(WHAT_GOES_UP_CURSES) do
                    if curseName == expansionOrCurseCode then
                        is_what_goes_up_curse = true
                        break
                    end
                end
            end

            if is_what_goes_up_curse then
                -- If this is a "What Goes Up" curse then delete the Crystal's Song PnP items plus the goat token if it's still there, and set up the necessary cards
                for _, guid in
                    ipairs(EXPANSIONS_AND_CURSES["crystals_song_pnp"]) do
                    local object = getObjectFromGUID(guid)
                    if object then object.destruct() end
                end

                local goat = getObjectFromGUID(
                                 EXPANSIONS_AND_CURSES["goes_up"][1])
                if goat then goat.destruct() end

                DELETED_CRYSTALS_PNP = true

                if expansionOrCurseCode ~= "goes_up" then
                    setExpansionOrCurse({'goes_up', true});
                end
            end
        end
    end

    for _, guid in ipairs(EXPANSIONS_AND_CURSES[expansionOrCurseCode]) do
        local object = getObjectFromGUID(guid)
        if object then object.destruct() end
    end

    printToAll(fullExpansionOrCurseName ..
                   (not include_curse and " (Expansion Only)" or "") ..
                   " successfully set up.")
end

function finishSetup(params)
    -- Deletes everything that has not been setup

    -- First thing, check whether any curse has been set
    local bagExpansions = getObjectFromGUID(BAG_EXPANSIONS_GUID)
    local numCurses = 0
    for _, object in ipairs(bagExpansions.getObjects()) do
        if string.find(object.name, "Curse") then
            numCurses = numCurses + 1
        end
    end

    if numCurses >= 12 then
        printToAll("You must pick at least one curse before finishing setup.")
    else
        -- Since at least one curse has been set, delete all others
        printToAll("Removing unused expansions and curses and setting decks up.")
        for _, expansionOrCurse in pairs(EXPANSIONS_AND_CURSES) do
            for _, guid in ipairs(expansionOrCurse) do
                local object = getObjectFromGUID(guid)
                if object then object.destruct() end
            end
        end

        returnCards({getObjectFromGUID(ZONES["ZONE_PAST"]["guid"]), true, {}})

        local finishSetupButton = getObjectFromGUID(params[1])
        if finishSetupButton then finishSetupButton.destruct() end

        -- Shuffle the Action Deck after the Past has been fully returned
        executeFunctionAfterCondition(function()
            return waitUntilZoneEmpty(numberOfCardsInPast);
        end, function()
            shuffleActionDeck();
            SETUP_FINISHED = true;
        end)
    end
end

function executeFunctionAfterCondition(condition, fun, delay)
    -- This function waits until the "condition" function returns "true" to execute the function "fun", optionally after a delay
    -- if "condition" returns a number, reexecute this function after that time has passed
    if not delay then delay = 0; end
    conditionResult = condition()
    if type(conditionResult) == "number" and conditionResult > 0 then
        Wait.time(function()
            executeFunctionAfterCondition(condition, fun, delay)
        end, conditionResult)
    elseif conditionResult or type(conditionResult) == "number" then
        -- the "or" there means that conditionResult is false-y and also it's a number, i.e. conditionResult == 0, and we don't want to loop infinitely in case the "condition" function starts returning 0
        Wait.time(fun, delay)
    elseif type(conditionResult) == "boolean" then
        executeFunctionAfterCondition(condition, fun, delay)
    else
        -- if conditionResult returns a non-number non-boolean something went wrong
        print("Conditional function incorrectly formatted.")
    end
end

function shuffleActionDeck()
    for _, potentialDeck in ipairs(getObjectFromGUID(
                                       ZONES["ZONE_ACTION_DECK"]["guid"]).getObjects()) do
        if potentialDeck.tag == "Deck" then
            potentialDeck.shuffle()
            return
        end
    end
end

function waitUntilZoneEmpty(numCardsZoneFun)
    cards = numCardsZoneFun()
    if cards > 0 then
        return cards * 0.1 + 1
    else
        return 0
    end
end

function numberOfCardsInPast()
    return numberOfCardsInZone(getObjectFromGUID(ZONES["ZONE_PAST"]["guid"]))
end

function numberOfCardsInActionZone()
    return math.max(numberOfCardsInZone(getObjectFromGUID(
                                            ZONES["ZONE_ACTION"]["guid"])) -
                        (numberOfCardsInActionDeck() +
                            numberOfCardsInDiscardPile()), 0)
end

function numberOfCardsInActionDeck()
    return numberOfCardsInZone(getObjectFromGUID(
                                   ZONES["ZONE_ACTION_DECK"]["guid"]))
end

function numberOfCardsInDiscardPile()
    return numberOfCardsInZone(getObjectFromGUID(
                                   ZONES["ZONE_ACTION_DISCARD"]["guid"]))
end

function numberOfCardsInZone(zone)
    local numCardsZone = 0
    for _, obj in ipairs(zone.getObjects()) do
        if obj.tag == "Card" then
            numCardsZone = numCardsZone + 1
        elseif obj.tag == "Deck" then
            numCardsZone = numCardsZone + math.max(obj.getQuantity(), 0)
        end
    end

    return numCardsZone
end

function createDiscardButton(owner, type)
    local rot = {0, 0, 180}
    local tooltip_string = "Discard Card"
    local click_function = "discardExploreCard"
    if type == "satchel" and
        (CARDS_IN_ZONE_MAP[owner.guid] or CARDS_IN_ZONE_DRAW[owner.guid]) then
        tooltip_string = "Store Card under Satchel"
        click_function = "storeCard"
    elseif type == "weather" and
        (CARDS_IN_ZONE_MAP[owner.guid] or CARDS_IN_ZONE_DRAW[owner.guid]) then
        tooltip_string =
            "Put this weather card in play and return any previous weather card in play"
        click_function = "storeWeatherCard"
    elseif type ~= "temp" and
        (CARDS_IN_ZONE_MAP[owner.guid] or CARDS_IN_ZONE_DRAW[owner.guid]) then
        tooltip_string = "Deal this card to an involved character"
        click_function = "dealThisCard"
    end

    if string.find(owner.getDescription(), "exploration") then
        rot = {180, 0, 180}
        if CARDS_IN_ZONE_MAP[owner.guid] then
            tooltip_string = tooltip_string ..
                                 " and replace it with an Adventure Card"
        end
    end

    owner.createButton({
        click_function = click_function,
        function_owner = Global,
        label = "",
        position = {0.9, -1, -1.3},
        rotation = rot,
        scale = {0.5, 0.5, 0.5},
        width = 400,
        height = 400,
        font_size = 200,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100},
        tooltip = tooltip_string
    })
end

function discardExploreCard(obj, playerClickerColor)
    obj.clearButtons()
    local pos = obj.getPosition()
    if (string.find(obj.getDescription(), "exploration")) then
        printToAll('Discarding Exploration Card.')
        if CARDS_IN_ZONE_MAP[obj.guid] then
            Wait.time(function() placeAdvCard(pos) end, 0.5)
        end
    else
        printToAll('Discarding Adventure Card.')
    end

    discardNextCard(obj, false)
end

function dealThisCard(obj, playerClickerColor)
    createPlayerSelectButtons(obj, playerClickerColor, "dealThisCardToPlayer")
end

function createPlayerSelectButtons(obj, playerClickerColor, functionString,
                                   cardName, ownerAction, rstCardName,
                                   is_back_action)
    obj.clearButtons()
    local rot = {0, 0, 180}
    local posY = -1
    local posZ = -1.2
    if string.find(obj.getDescription(), "exploration") or is_back_action then
        rot = {180, 180, 180};
        posY = 1;
    end
    _G['drawAndDealCardToPlayer'] = function(obj, col)
        drawAndDealCardToPlayers(obj, playerClickerColor, cardName, ownerAction,
                                 rstCardName);
    end
    _G['returnCardFromPlayer'] = function(obj, col)
        returnCardFromPlayers(obj, cardName, ownerAction, rstCardName);
    end
    for i = 1, NUM_PLAYERS do
        _G['selectPlayer_' .. i] = function(obj, col)
            selectPlayer(obj, playerClickerColor, i, cardName, ownerAction,
                         rstCardName, functionString);
        end
        clickFunction = "selectPlayer_" .. i
        obj.createButton({
            click_function = clickFunction,
            function_owner = Global,
            label = "Select " .. PLAYER_NAME[i],
            position = {0, posY, posZ},
            rotation = rot,
            width = 1000,
            height = 300,
            font_size = 100,
            font_color = {0, 0, 0}
        })
        posZ = posZ + 0.6
    end
    obj.createButton({
        click_function = "cancelAction",
        function_owner = Global,
        label = "Cancel",
        position = {0.5, posY, posZ},
        rotation = rot,
        width = 500,
        height = 300,
        font_size = 100,
        font_color = {0, 0, 0}
    })
    if functionString ~= "dealThisCardToPlayer" and functionString ~=
        "drawAndDealCardToActivePlayer" and functionString ~=
        "returnCardFromActivePlayer" then
        obj.createButton({
            click_function = functionString,
            function_owner = Global,
            label = "Done",
            position = {-0.5, posY, posZ},
            rotation = rot,
            width = 500,
            height = 300,
            font_size = 100,
            font_color = {0, 0, 0}
        })
    end
end

function dealThisCardToPlayer(obj, playerClickerColor, playerNum)
    obj.clearButtons()
    local pos = obj.getPosition()
    local zonePlayerMat =
        getObjectFromGUID(ZONES["ZONE_P" .. playerNum]['guid'])
    local zonePlayerMatPos = zonePlayerMat.getPosition()
    zonePlayerMatPos.x = zonePlayerMatPos.x - 4.6
    printToAll('Dealing card to ' .. PLAYER_NAME[playerNum] .. '\'s mat')
    if STATE_CARDS[tonumber(obj.getName())] or STATE_CARDS[obj.getGUID()] then
        playerMatPos = returnNextOpenPos(zonePlayerMat,
                                         playerRedStatePos[playerNum], 5, 0,
                                         2.3, 0)
    else
        playerMatPos = returnNextOpenPos(zonePlayerMat, zonePlayerMatPos, 5, 0,
                                         2.3, 0)
    end
    obj.setPosition(playerMatPos)
    Wait.time(function() setupAdvCardButtons(obj) end, 0.1)
    if string.find(obj.getDescription(), "exploration") then
        Wait.time(function() placeAdvCard(pos) end, 0.5);
    end
    Player[playerClickerColor].lookAt({
        position = zonePlayerMat.getPosition(),
        pitch = 70,
        distance = 15
    })
end

function selectPlayer(obj, playerClickerColor, playerNum, cardName, ownerAction,
                      rstCardName, functionString)
    local buttonIndex = playerNum - 1
    if obj.hasTag("Select Player " .. playerNum) then
        obj.editButton({index = buttonIndex, color = {1, 1, 1}})
        obj.removeTag("Select Player " .. playerNum)
    else
        obj.editButton({index = buttonIndex, color = {0.5, 0.5, 0.5}})
        obj.addTag("Select Player " .. playerNum)
    end
    if functionString == "dealThisCardToPlayer" then
        dealThisCardToPlayer(obj, playerClickerColor, playerNum)
    elseif functionString == "drawAndDealCardToActivePlayer" then
        drawAndDealCardToPlayers(obj, playerClickerColor, cardName, ownerAction,
                                 rstCardName)
    elseif functionString == "returnCardFromActivePlayer" then
        returnCardFromPlayers(obj, cardName)
    end
end

function drawAndDealCardToPlayers(obj, playerClickerColor, cardName,
                                  ownerAction, rstCardName)
    local resetBoard = getObjectFromGUID(RESET_BOARD_GUID)
    obj.clearButtons()
    for playerNum = 1, NUM_PLAYERS do
        if obj.hasTag("Select Player " .. playerNum) then
            obj.removeTag("Select Player " .. playerNum)
            local zonePlayerMat = getObjectFromGUID(
                                      ZONES["ZONE_P" .. playerNum]['guid'])
            local playerMatPos = returnNextOpenPos(zonePlayerMat,
                                                   playerRedStatePos[playerNum],
                                                   5, 0, 2.3, 0)
            for card in string.gmatch(cardName, '([^,]+),? *') do
                printToAll("Dealing " .. card .. " card to " ..
                               PLAYER_NAME[playerNum] .. '\'s mat')
                getAdvCard(tonumber(card), false, false, false, playerMatPos,
                           false, false)
                playerMatPos.x = playerMatPos.x - 2.3
            end
        end
    end
    -- Discard, Banish or Return original card after selecting players to deal cards
    if ownerAction then ownerSpecialActionHelper(obj, ownerAction); end
    if rstCardName then
        Wait.time(function()
            resetBoard.call("resetBoardRoutine", rstCardName)
        end, 1);
    end
    if not ownerAction and not rstCardName then setupAdvCardButtons(obj); end
end

function returnCardFromPlayers(obj, returnPlayerCard, ownerAction, rstCardName)
    obj.clearButtons()
    for playerNum = 1, NUM_PLAYERS do
        if obj.hasTag("Select Player " .. playerNum) then
            obj.removeTag("Select Player " .. playerNum)
            for cardName in string.gmatch(returnPlayerCard, '([^,]+),? *') do
                local card_found = false
                printToAll("Returning " .. cardName .. " state from " ..
                               PLAYER_NAME[playerNum])
                local zoneObjects = getObjectFromGUID(ZONES["ZONE_P" ..
                                                          playerNum]['guid']).getObjects()
                for _, obj in ipairs(zoneObjects) do
                    if obj.getName() == STATE_CARDS[cardName] then
                        returnNextCard(obj, false, false)
                        card_found = true
                        break
                    end
                end
                if not card_found then
                    print(PLAYER_NAME[playerNum] .. " is not " .. cardName);
                end
            end
        end
    end
    -- Discard, Banish or Return original card after selecting players to deal cards
    if ownerAction then ownerSpecialActionHelper(obj, ownerAction); end
    if rstCardName then
        Wait.time(function()
            resetBoard.call("resetBoardRoutine", rstCardName)
        end, 1);
    end
    if not ownerAction and not rstCardName then setupAdvCardButtons(obj); end
end

function placeAdvCard(pos)
    local zonePlayMat = getObjectFromGUID(ZONES["ZONE_MAP"]["guid"])
    for _, object in ipairs(zonePlayMat.getObjects()) do
        if object.tag == "Card" then
            local objDesc = object.getDescription()
            if string.find(objDesc, "green") or string.find(objDesc, "gold") or
                string.find(objDesc, "white") then
                local objPos = object.getPosition()
                local advCardConnections = getTerrainConnections(object)

                if math.abs(pos.x - objPos.x) < 0.1 then
                    if pos.z > objPos.z and pos.z - objPos.z < 6.5 and
                        advCardConnections[1] > 0 then
                        getTopAdv(object)
                        return
                    elseif pos.z < objPos.z and objPos.z - pos.z < 6.5 and
                        advCardConnections[2] > 0 then
                        getBottomAdv(object)
                        return
                    end
                elseif math.abs(pos.z - objPos.z) < 0.1 then
                    if pos.x > objPos.x and pos.x - objPos.x < 6.5 and
                        advCardConnections[3] > 0 then
                        getRightAdv(object)
                        return
                    elseif pos.x < objPos.x and objPos.x - pos.x < 6.5 and
                        advCardConnections[4] > 0 then
                        getLeftAdv(object)
                        return
                    end
                end
            end
        end
    end
    printToAll(
        "Could not determine Adventure Card to be placed. Place manually.")
end

function getExplorationDeck(obj, direction, grabCrystalExplore)
    local activeExploreZone = getActiveZone(obj, direction, grabCrystalExplore);
    for _, object in ipairs(activeExploreZone.getObjects()) do
        if object.tag == "Deck" or object.tag == "Card" then
            return object
        end
    end

    return nil
end

function addExploreCards(obj)
    if not EXPLORE_CARDS_PLAYED[obj.guid] then -- Icy Maze cards have their own specific logic
        local name = getNameOfExploreCard(obj);
        local zoneConnections = ZONE_CONNECTIONS[name]
        local advCardConnections = getTerrainConnections(obj)

        addNextExploreCard(obj, name, zoneConnections, advCardConnections, 1,
                           PICKED_CRYSTALS_SONG)
    end
end

function addNextExploreCard(obj, name, zoneConnections, advCardConnections,
                            iter, grabCrystalExplore)
    if iter > 4 then
        if name == "z499" or name == "z449" or name == "z475" or name == "z478" or
            name == "z497" then
            -- Icy Maze cards have their own specific logic
            EXPLORE_CARDS_PLAYED[obj.guid] = true
        end
        return
    end

    local directions = {"n", "s", "e", "w"}
    local currentDirection = directions[iter]
    local explorationDeck = getExplorationDeck(obj, currentDirection,
                                               grabCrystalExplore)

    if zoneConnections[iter] > 0 and
        (currentDirection ~= "w" or obj.guid ~= SPECIAL_Z136_GUID) then -- there's a specific 136 card that does not have the west connection
        if not exploration_card_already_played(advCardConnections[iter], obj,
                                               currentDirection) then
            if grabCrystalExplore then
                grabCrystalExplore = false
                if not explorationDeck then
                    getAdvCard(559, false, false, false,
                               {x = 2.18, y = 4, z = -28.72}, false, false)
                    printToAll(
                        "Crystal's Song Curse completed; please take card number 559.")
                    PICKED_CRYSTALS_SONG = false
                    explorationDeck = getExplorationDeck(obj, currentDirection,
                                                         grabCrystalExplore)
                    for _, pl in ipairs(Player.getPlayers()) do
                        pl.lookAt({
                            position = {x = 0, y = 0, z = -27},
                            pitch = 70,
                            distance = 15
                        })
                    end
                end
            end

            if explorationDeck then
                -- The relevant exploration deck is there
                if explorationDeck.tag == "Deck" then
                    local deckPos = explorationDeck.getPosition()
                    explorationDeck.shuffle()
                    randomEvent = explorationDeck.takeObject({
                        position = {deckPos.x, deckPos.y + 2, deckPos.z},
                        top = true,
                        flip = false
                    })
                else
                    randomEvent = explorationDeck
                end

                local xOffsets = {n = 0, s = 0, e = 6.18, w = -6.18}
                local zOffsets = {n = 6.26, s = -6.26, e = 0, w = 0}
                local pos = obj.getPosition()
                randomEvent.setPosition({
                    pos.x + xOffsets[currentDirection], pos.y + 0.2,
                    pos.z + zOffsets[currentDirection]
                })
                Wait.time(function()
                    setupAdvCardButtons(randomEvent)
                end, 0.1)
                Wait.time(function()
                    addNextExploreCard(obj, name, zoneConnections,
                                       advCardConnections, iter + 1,
                                       grabCrystalExplore)
                end, 0.1)
            else
                -- Exploration Deck is depleted, need to return Past and try again
                local zonePast = getObjectFromGUID(ZONES["ZONE_PAST"]["guid"])
                zone_past_is_empty = true
                for _, obj in ipairs(zonePast.getObjects()) do
                    if obj.tag == "Deck" or obj.tag == "Card" then
                        if obj.tag == "Deck" then
                            obj.shuffle() -- I'm shuffling the Past so that it returns its cards to the exploration pile in a random order
                        end
                        zone_past_is_empty = false
                        break
                    end
                end

                if zone_past_is_empty then
                    -- Exploration deck is depleted *and* Past is empty
                    broadcastToAll("Could not find Exploration Cards", {1, 1, 1})
                    return
                end

                broadcastToAll("Exploration deck depleted ~ Returning Past",
                               {1, 1, 1})

                returnCards({zonePast, true, {}})
                -- Wait until all cards have been returned and then try to grab the exploration card
                executeFunctionAfterCondition(function()
                    return waitUntilZoneEmpty(numberOfCardsInPast);
                end, function()
                    addNextExploreCard(obj, name, zoneConnections,
                                       advCardConnections, iter,
                                       grabCrystalExplore)
                end, 1)
            end

            return
        end
    end

    -- If we got here it's because we didn't have to add this Exploration Card for one reason or another
    addNextExploreCard(obj, name, zoneConnections, advCardConnections, iter + 1,
                       grabCrystalExplore)
end

function createAdvButtons(owner)
    local advCardConnections = getTerrainConnections(owner)
    local name = getNameOfExploreCard(owner);

    if not advCardConnections then
        TERRAIN_CARD_CONNECTIONS[name] = {0, 0, 0, 0}
        advCardConnections = {0, 0, 0, 0}
    end

    if advCardConnections[1] > 0 then
        owner.createButton({
            click_function = "getTopAdv",
            function_owner = Global,
            label = "",
            position = {0, 0, -1.4},
            rotation = {0, 0, 180},
            scale = {0.5, 0.5, 0.5},
            width = 900,
            height = 300,
            font_size = 200,
            color = {1, 1, 1, 0},
            font_color = {0, 0, 0, 100},
            tooltip = "Place Adventure Card " ..
                addTrailingZeros(advCardConnections[1])
        })
    end

    if advCardConnections[2] > 0 then
        owner.createButton({
            click_function = "getBottomAdv",
            function_owner = Global,
            label = "",
            position = {0, 0, 1.4},
            rotation = {0, 0, 180},
            scale = {0.5, 0.5, 0.5},
            width = 900,
            height = 300,
            font_size = 200,
            color = {1, 1, 1, 0},
            font_color = {0, 0, 0, 100},
            tooltip = "Place Adventure Card " ..
                addTrailingZeros(advCardConnections[2])
        })
    end

    if advCardConnections[3] > 0 then
        owner.createButton({
            click_function = "getRightAdv",
            function_owner = Global,
            label = "",
            position = {-0.9, 0, -0.1},
            rotation = {0, 90, 180},
            scale = {0.5, 0.5, 0.5},
            width = 900,
            height = 300,
            font_size = 200,
            color = {1, 1, 1, 0},
            font_color = {0, 0, 0, 100},
            tooltip = "Place Adventure Card " ..
                addTrailingZeros(advCardConnections[3])
        })
    end

    if advCardConnections[4] > 0 and owner.guid ~= SPECIAL_Z136_GUID then
        owner.createButton({
            click_function = "getLeftAdv",
            function_owner = Global,
            label = "",
            position = {0.9, 0, -0.1},
            rotation = {0, 90, 180},
            scale = {0.5, 0.5, 0.5},
            width = 900,
            height = 300,
            font_size = 200,
            color = {1, 1, 1, 0},
            font_color = {0, 0, 0, 100},
            tooltip = "Place Adventure Card " ..
                addTrailingZeros(advCardConnections[4])
        })
    end
end

function getActiveZone(obj, direction, grabCrystalExplore)
    if grabCrystalExplore then return getExploreZoneFromId(99) end

    local name = getNameOfExploreCard(obj);
    data = ZONE_CONNECTIONS[name];
    if direction == "n" then
        return getExploreZoneFromId(data[1]);
    elseif direction == "s" then
        return getExploreZoneFromId(data[2]);
    elseif direction == "e" then
        return getExploreZoneFromId(data[3]);
    else
        return getExploreZoneFromId(data[4]);
    end
end

function getButtonIndex(owner, name, direction)
    if direction == "n" then return 0 end

    local zoneConnectionsData = ZONE_CONNECTIONS[name];
    if zoneConnectionsData then
        local indices = {s = 0, e = 0, w = 0};
        local advCardConnections = getTerrainConnections(owner)
        if zoneConnectionsData[1] > 0 or
            (advCardConnections and advCardConnections[1] > 0) then
            indices["s"] = 1
            indices["e"] = 1
            indices["w"] = 1
        end
        if zoneConnectionsData[2] > 0 or
            (advCardConnections and advCardConnections[2] > 0) then
            indices["e"] = indices["e"] + 1
            indices["w"] = indices["w"] + 1
        end
        if zoneConnectionsData[3] > 0 or
            (advCardConnections and advCardConnections[3] > 0) then
            indices["w"] = indices["w"] + 1
        end

        return indices[direction]
    end

    return 0
end

function getAdv(obj, directionIndex)
    local advCardConnections = getTerrainConnections(obj)
    if advCardConnections[directionIndex] > 0 then
        local pos = obj.getPosition()
        local xOffsets = {0, 0, 6.18, -6.18}
        local zOffsets = {6.26, -6.26, 0, 0}

        searching_for_white_card = false
        if directionIndex == 1 then
            searching_for_white_card = (obj.getDescription() == "white" and
                                           advCardConnections[1] ~= 734) or
                                           (obj.getName() == "768" and
                                               obj.getDescription() == "gold")
        elseif directionIndex == 2 then
            searching_for_white_card = (obj.getDescription() == "white" and
                                           advCardConnections[2] ~= 768 and
                                           advCardConnections[2] ~= 695 and
                                           advCardConnections[2] ~= 681)
        elseif directionIndex == 3 then
            searching_for_white_card = (obj.getDescription() == "white" and
                                           advCardConnections[3] ~= 768)
        elseif directionIndex == 4 then
            searching_for_white_card = (obj.getDescription() == "white" and
                                           advCardConnections[4] ~= 681) or
                                           (obj.getName() == "768" and
                                               obj.getDescription() == "gold")
        end

        getAdvCard(advCardConnections[directionIndex], searching_for_white_card,
                   false, false, {
            x = pos.x + xOffsets[directionIndex],
            y = pos.y,
            z = pos.z + zOffsets[directionIndex]
        }, false, true)
    else
        printToAll("Card connection not yet defined. Place card manually.")
    end
end

function getTopAdv(obj, playerClickerColor) getAdv(obj, 1) end

function getBottomAdv(obj, playerClickerColor) getAdv(obj, 2) end

function getLeftAdv(obj, playerClickerColor) getAdv(obj, 4) end

function getRightAdv(obj, playerClickerColor) getAdv(obj, 3) end

function getNameOfExploreCard(obj)
    local desc = obj.getDescription();
    local name = "z" .. obj.getName();
    if string.find(desc, "gold") then
        name = name .. "g"
    elseif string.find(desc, "white") then
        name = name .. "w"
    end

    if ZONE_CONNECTIONS[name .. "_" .. obj.guid] then
        name = name .. "_" .. obj.guid
    end

    if string.find(desc, "exploration") then
        name = string.gsub(desc, "exploration ", "exp") .. "_" .. obj.guid
    end

    return name;
end

function getExploreZoneFromId(z)
    if z == 1 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE1.guid)
    elseif z == 2 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE2.guid)
    elseif z == 3 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE3.guid)
    elseif z == 4 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE4.guid)
    elseif z == 5 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE5.guid)
    elseif z == 6 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE6.guid)
    elseif z == 7 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE7.guid)
    elseif z == 8 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE8.guid)
    elseif z == 9 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE9.guid)
    elseif z == 10 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE10.guid)
    elseif z == 11 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE11.guid)
    elseif z == 12 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE12.guid)
    elseif z == 99 then
        return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONEC.guid)
    end

    return getObjectFromGUID(Global.getTable('EXPLORE_ZONES').ZONE1.guid)
end

fixedUpdateTick = 0;
enableUpdateScales = false;
fixScaleCardList = {};
function onFixedUpdate()
    fixedUpdateTick = fixedUpdateTick + 1;
    if fixedUpdateTick > 10 and enableUpdateScales then
        fixedUpdateTick = 0
        enableUpdateScales = false
        updateScales();
    end
end

callTime = 0;
function updateScales()
    callTime = callTime + 1

    for i = 1, #fixScaleCardList do
        local obj = getObjectFromGUID(fixScaleCardList[i])
        if obj and not string.find(obj.getName(), "Page ") then
            resizeCardInZone(obj)
        end
    end

    fixScaleCardList = {}
end

function resizeCardInZone(enterObject)
    if string.find(enterObject.getName(), "Page ") then return; end

    local zoneChooseCharacter1 = getObjectFromGUID(
                                     ZONES['ZONE_CHOOSE_CHARACTER']['guid'][1]).getObjects()
    for _, obj in pairs(zoneChooseCharacter1) do
        if obj == enterObject then
            enterObject.setScale({0.69, 1.00, 0.69})
            return;
        end
    end
    local zoneChooseCharacter2 = getObjectFromGUID(
                                     ZONES['ZONE_CHOOSE_CHARACTER']['guid'][2]).getObjects()
    for _, obj in pairs(zoneChooseCharacter2) do
        if obj == enterObject then
            enterObject.setScale({0.69, 1.00, 0.69})
            return;
        end
    end

    local zoneMatObjects =
        getObjectFromGUID(ZONES['ZONE_MATS']['guid']).getObjects()
    for k, obj in pairs(zoneMatObjects) do
        if obj == enterObject then
            enterObject.setScale({0.69, 1.00, 0.69})
            return;
        end
    end

    local zoneSearchObjects =
        getObjectFromGUID(ZONES['ZONE_SEARCH']['guid']).getObjects()
    for k, obj in pairs(zoneSearchObjects) do
        if obj == enterObject then
            enterObject.setScale({0.69, 1.00, 0.69})
            return;
        end
    end

    local zoneMapObjects =
        getObjectFromGUID(ZONES['ZONE_MAP']['guid']).getObjects()
    for k, obj in pairs(zoneMapObjects) do
        if obj == enterObject then
            enterObject.setScale({2.00, 1.00, 2.00})
            return;
        end
    end

    enterObject.setScale({1.00, 1.00, 1.00})
end

function onObjectLeaveScriptingZone(zone, enterObject)
    if enterObject and (enterObject.tag == "Card" or enterObject.tag == "Deck") and
        (not string.find(enterObject.getName(), "Page ")) then
        table.insert(fixScaleCardList, enterObject.guid)
        enableUpdateScales = true;
    end
    if zone.guid == ZONES["ZONE_MAP"].guid then
        CARDS_IN_ZONE_MAP[enterObject.guid] = nil;
    end
    if zone.guid == ZONES["ZONE_DRAW"].guid then
        CARDS_IN_ZONE_DRAW[enterObject.guid] = nil;
    end
end

function onObjectEnterScriptingZone(zone, enterObject)
    if enterObject and (enterObject.tag == "Card" or enterObject.tag == "Deck") and
        (not string.find(enterObject.getName(), "Page ")) then
        table.insert(fixScaleCardList, enterObject.guid)
        enableUpdateScales = true;
    end
    if zone.guid == ZONES["ZONE_MAP"].guid then
        CARDS_IN_ZONE_MAP[enterObject.guid] = true;
    end
    if zone.guid == ZONES["ZONE_DRAW"].guid then
        CARDS_IN_ZONE_DRAW[enterObject.guid] = true;
    end
end

function discardCards(params)
    local originZone = params[1]
    local actionCardList = params[2]

    if #actionCardList == 0 then
        for _, object in ipairs(originZone.getObjects()) do
            if object.tag == "Deck" or object.tag == "Card" then
                discardCardsHelper(originZone, false, {})
                return
            end
        end

        printToAll('No cards or deck found to discard.')
    else
        discardCardsHelper(originZone, true, actionCardList)
    end
end

function discardCardsHelper(originZone, is_discarding_action_cards,
                            actionCardList)
    local cardsToDiscard = {}
    if not is_discarding_action_cards then
        cardsToDiscard = originZone.getObjects()
    else
        cardsToDiscard = actionCardList
    end

    if #cardsToDiscard > 0 then
        for _, object in ipairs(cardsToDiscard) do
            if object.tag == "Deck" or object.tag == "Card" then
                discardNextCard(object, is_discarding_action_cards)
                if is_discarding_action_cards then
                    table.remove(cardsToDiscard, 1)
                end
                Wait.time(function()
                    discardCardsHelper(originZone, is_discarding_action_cards,
                                       cardsToDiscard)
                end, 0.1, 0)
                return
            end
        end
    end

    printToAll("Done.")
end

function discardNextCard(cardOrDeck, is_discarding_action_cards)
    if not cardOrDeck or
        not (cardOrDeck.tag == "Deck" or cardOrDeck.tag == "Card") then
        -- Don't think it's actually possible to get here but just in case
        return
    elseif cardOrDeck.tag == "Deck" then
        for _, card in ipairs(cardOrDeck.getObjects()) do
            local pos = getDiscardCardPos(card.description,
                                          is_discarding_action_cards)
            local rot = getDiscardCardRot(card.description,
                                          is_discarding_action_cards)
            cardOrDeck.takeObject({
                position = pos,
                rotation = rot,
                guid = card.guid
            })
            break
        end
    else
        local desc = cardOrDeck.getDescription()
        local pos = getDiscardCardPos(desc, is_discarding_action_cards)
        local rot = getDiscardCardRot(desc, is_discarding_action_cards)
        cardOrDeck.setRotation(rot)
        cardOrDeck.setPosition(pos)
    end
end

function getDiscardCardPos(desc, is_discarding_action_cards)
    if is_discarding_action_cards or
        (not (string.find(desc, "exploration") or string.find(desc, "green") or
            string.find(desc, "gold") or string.find(desc, "white"))) then
        return ACTION_DISCARD_POS
    else
        return PAST_POS
    end
end

function getDiscardCardRot(desc, is_discarding_action_cards)
    if is_discarding_action_cards or
        (not (string.find(desc, "exploration") or string.find(desc, "green") or
            string.find(desc, "gold") or string.find(desc, "white"))) then
        return {0, 180, 0}
    else
        return {0, 180, 180}
    end
end

function returnCards(params)
    local originZone = params[1]
    local is_from_the_past = params[2]
    local actionCardList = params[3]

    printToAll("Returning cards...")
    if #actionCardList == 0 then
        for _, object in ipairs(originZone.getObjects()) do
            if object.tag == "Deck" or object.tag == "Card" then
                returnCardsHelper(originZone, false, false, {}, -1)
                return
            end
        end

        if is_from_the_past then
            printToAll('No past deck found to return.')
        else
            printToAll('No cards found to return.')
        end
    else
        returnCardsHelper(originZone, true, false, actionCardList, -1)
    end
end

function returnActionCardsFromDiscardPile(numCardsToReturn, verbose)
    if verbose or verbose == nil then printToAll("Returning Action Cards...") end

    zoneActionDeck = getObjectFromGUID(ZONES["ZONE_ACTION_DECK"]["guid"])
    zoneActionDiscard = getObjectFromGUID(ZONES["ZONE_ACTION_DISCARD"]["guid"])
    for _, obj in ipairs(zoneActionDiscard.getObjects()) do
        if obj.tag == "Deck" then obj.shuffle() end
    end
    returnCardsHelper(zoneActionDiscard, true, false, {}, numCardsToReturn)
end

function discardActionCardsFromActionDeck(numCardsToReturn, verbose)
    if verbose or verbose == nil then
        printToAll("Discarding Action Cards...")
    end

    zoneActionDeck = getObjectFromGUID(ZONES["ZONE_ACTION_DECK"]["guid"])
    zoneActionDiscard = getObjectFromGUID(ZONES["ZONE_ACTION_DISCARD"]["guid"])
    for _, obj in ipairs(zoneActionDeck.getObjects()) do
        if obj.tag == "Deck" then obj.shuffle() end
    end
    returnCardsHelper(zoneActionDeck, false, true, {}, numCardsToReturn)
end

function returnCardsHelper(originZone, is_returning_action_cards,
                           is_discarding_action_cards, actionCardList,
                           numCardsToReturn)
    if numCardsToReturn == 0 then
        -- if numCardsToReturn starts out negative, then I'll just return every card from the relevant zone
        -- if it's positive then I'll stop once I've returned that many cards
        return
    end

    local cardsToReturn = {}
    if not is_returning_action_cards or #actionCardList < 1 then
        cardsToReturn = originZone.getObjects()
    else
        cardsToReturn = actionCardList
    end

    if #cardsToReturn > 0 then
        for _, object in ipairs(cardsToReturn) do
            if object.tag == "Deck" or object.tag == "Card" then
                returnNextCard(object, is_returning_action_cards,
                               is_discarding_action_cards)
                if is_returning_action_cards then
                    if #actionCardList > 0 then
                        table.remove(cardsToReturn, 1)
                        if #cardsToReturn == 0 then break end
                    else
                        cardsToReturn = {}
                    end
                end
                Wait.time(function()
                    returnCardsHelper(originZone, is_returning_action_cards,
                                      is_discarding_action_cards, cardsToReturn,
                                      numCardsToReturn - 1)
                end, 0.1, 0)
                return
            end
        end
    end

    printToAll("Done.")
end

function returnNextCard(cardOrDeck, is_returning_action_cards,
                        is_discarding_action_cards)
    if not cardOrDeck or
        not (cardOrDeck.tag == "Deck" or cardOrDeck.tag == "Card") then
        -- Don't think it's actually possible to get here but just in case
        return
    elseif cardOrDeck.tag == "Deck" then
        for _, card in ipairs(cardOrDeck.getObjects()) do
            local pos = getReturnCardPos(card.description,
                                         is_returning_action_cards,
                                         is_discarding_action_cards)
            local rot = getReturnCardRot(card.description,
                                         is_returning_action_cards,
                                         is_discarding_action_cards)
            cardOrDeck.takeObject({
                position = pos,
                rotation = rot,
                guid = card.guid
            })
            break
        end
    else
        local desc = cardOrDeck.getDescription()
        local pos = getReturnCardPos(desc, is_returning_action_cards,
                                     is_discarding_action_cards)
        local rot = getReturnCardRot(desc, is_returning_action_cards,
                                     is_discarding_action_cards)
        if CARDS_IN_ZONE_MAP[cardOrDeck.guid] then
            cardOrDeck.setScale({1.00, 1.00, 1.00}) -- required if returing exploration cards directly from the play mat
        end
        cardOrDeck.setRotation(rot)
        cardOrDeck.setPosition(pos)
    end
end

function getReturnCardPos(desc, is_returning_action_cards,
                          is_discarding_action_cards)
    if is_returning_action_cards then
        return ACTION_DECK_POS
    elseif is_discarding_action_cards then
        return ACTION_DISCARD_POS
    elseif string.find(desc, "exploration") then
        if string.find(desc, "10") then
            return EXPLORATION_DECKS_POSITIONS.TEN
        elseif string.find(desc, "11") then
            return EXPLORATION_DECKS_POSITIONS.ELEVEN
        elseif string.find(desc, "12") then
            return EXPLORATION_DECKS_POSITIONS.TWELVE
        elseif string.find(desc, "1") then
            return EXPLORATION_DECKS_POSITIONS.ONE
        elseif string.find(desc, "2") then
            return EXPLORATION_DECKS_POSITIONS.TWO
        elseif string.find(desc, "3") then
            return EXPLORATION_DECKS_POSITIONS.THREE
        elseif string.find(desc, "4") then
            return EXPLORATION_DECKS_POSITIONS.FOUR
        elseif string.find(desc, "5") then
            return EXPLORATION_DECKS_POSITIONS.FIVE
        elseif string.find(desc, "6") then
            return EXPLORATION_DECKS_POSITIONS.SIX
        elseif string.find(desc, "7") then
            return EXPLORATION_DECKS_POSITIONS.SEVEN
        elseif string.find(desc, "8") then
            return EXPLORATION_DECKS_POSITIONS.EIGHT
        elseif string.find(desc, "9") then
            return EXPLORATION_DECKS_POSITIONS.NINE
        elseif string.find(desc, "c") then
            return EXPLORATION_DECKS_POSITIONS.CRYSTAL
        end
    elseif string.find(desc, "green") or string.find(desc, "gold") or
        string.find(desc, "white") then
        return ADVENTURE_DECK_POS
    else
        return ACTION_DECK_POS
    end
end

function getReturnCardRot(desc, is_returning_action_cards,
                          is_discarding_action_cards)
    if is_returning_action_cards or is_discarding_action_cards or
        (not (string.find(desc, "green") or string.find(desc, "gold") or
            string.find(desc, "white"))) then
        return {0, 180, 180}
    else
        return {0, 180, 0}
    end
end

function getBanishCardRot(desc)
    if string.find(desc, 'green') or string.find(desc, 'gold') or
        string.find(desc, 'white') then
        return {x = 0, y = 180, z = 0}
    else
        return {x = 0, y = 180, z = 180}
    end
end

function banishCard(obj)
    obj.setRotation(getBanishCardRot(obj.getDescription()))
    obj.setPosition(BANISH_DECK_POS)
end

function storeCard(obj)
    obj.clearButtons()
    local satchelX0Pos = -1.42
    local satchelZ0Pos = -45.96
    local zoneSatchel = getObjectFromGUID(ZONES["ZONE_SATCHEL"]['guid'])
    local startingPos = {x = -1.42, y = 3, z = -45.96}
    local pos = returnNextOpenPos(zoneSatchel, startingPos, 2, 3, 1.42, 1.1,
                                  obj.getName())
    obj.setPosition(pos)
    Wait.time(function() setupAdvCardButtons(obj) end, 0.5)
end

function storeWeatherCard(obj)
    obj.clearButtons()

    returnActiveWeatherCard()

    Wait.time(function()
        obj.setPosition(WEATHER_POS);
        activeWeatherCard = obj.getGUID();
    end, 0.2)
    Wait.time(function() setupAdvCardButtons(obj) end, 0.5)
end

function returnActiveWeatherCard(obj)
    local zonePast = getObjectFromGUID(ZONES["ZONE_PAST"]['guid'])
    for _, object in ipairs(zonePast.getObjects()) do
        if object.tag == "Card" then
            if object.getGUID() == activeWeatherCard then
                returnNextCard(object, false, false)
                return
            end
        end
    end
end

function getAdvCard(firstParam, is_white_search, is_gold_search,
                    is_only_green_search, pos, fetch_from_banish_pile,
                    return_past_if_necessary)
    -- This first check is for whether the function is being called from Global or from the search mat
    if type(firstParam) == "number" then
        cardNumber = firstParam
        is_sorting = false
    else
        cardNumber = firstParam[1]
        is_white_search = firstParam[2]
        pos = firstParam[3]
        is_sorting = firstParam[4]
        is_gold_search = false
        is_only_green_search = false
        fetch_from_banish_pile = false
        return_past_if_necessary = false
    end

    local zoneAdvDeck = getObjectFromGUID(ZONES["ZONE_ADVENTURE_DECK"]["guid"])
    local zoneGameboard = getObjectFromGUID(ZONES["ZONE_MAP"]["guid"])
    local zonePast = getObjectFromGUID(ZONES["ZONE_PAST"]["guid"])
    local zoneBanishDeck = getObjectFromGUID(ZONES['ZONE_BANISH']['guid'])

    if fetch_from_banish_pile then
        targetZone = zoneBanishDeck
    else
        targetZone = zoneAdvDeck;
    end

    searchParam = addTrailingZeros(cardNumber)

    if is_white_search then
        printToAll("Searching for '" .. searchParam .. "' with white back.")
    else
        printToAll("Searching for '" .. searchParam .. "'.")
    end

    local card_is_in_play = false
    local card_is_in_past = false

    local cardCode = nil
    local deck = nil
    local lastCard = nil

    -- first we check whether the relevant zone has any cards at all
    local listOfDecksOrCards = {}
    local target_deck_found = false
    for _, object in ipairs(targetZone.getObjects()) do
        if object.tag == "Deck" then
            table.insert(listOfDecksOrCards, object)
            target_deck_found = true
        elseif object.tag == "Card" then
            table.insert(listOfDecksOrCards, object)
            target_deck_found = true
        end
    end

    -- if it does, then we go through them to try to find the relevant card
    -- this should only really matter for the "fetch from banish pile" routine, since the adventure deck should in general always be singular
    for _, deckOrCard in ipairs(listOfDecksOrCards) do
        -- check each item we found in the target zone one by one for the card we want
        if deckOrCard.tag == "Deck" then
            deck = deckOrCard
            if not is_sorting then deck.shuffle(); end
            lasCard = nil
        else
            lastCard = deckOrCard
            deck = nil
        end

        if not is_white_search then
            cardCode = "z" .. searchParam
            if not is_gold_search then
                -- look for green first, then if no green, take the gold
                if getAdvCardRoutine(cardCode, searchParam, "green", deck,
                                     lastCard, pos) then
                    return true;
                end
            end

            if not is_only_green_search then
                cardCode = cardCode .. "g"
                if getAdvCardRoutine(cardCode, searchParam, "gold", deck,
                                     lastCard, pos) then
                    return true;
                end
            end
        end

        if is_white_search or is_sorting then
            -- look for white cards if either this is specifically a white search or if I am sorting cards
            cardCode = "z" .. searchParam .. "w"
            if getAdvCardRoutine(cardCode, searchParam, "white", deck, lastCard,
                                 pos) then return true; end
        end
    end

    if is_sorting then
        printToAll("Card not found. Looking for next card...")
        return false
    end

    -- didn't find the card in the deck, now to check whether it's on the gameboard or the Past
    local desc1 = ""
    local desc2 = ""
    if is_white_search then
        desc1 = "white"
        desc2 = "white"
    elseif is_gold_search then
        desc1 = "gold"
        desc2 = "gold"
    else
        desc1 = "green"
        desc2 = "gold"
    end

    -- gameboard first
    for _, object in ipairs(zoneGameboard.getObjects()) do
        if object.tag == "Card" then
            if object.getName() == searchParam and
                (string.find(object.getDescription(), desc1) or
                    string.find(object.getDescription(), desc2)) then
                -- found the card in play
                if is_white_search then
                    printToAll("Card '" .. searchParam ..
                                   "' with white back is already in play.")
                elseif is_gold_search then
                    printToAll("Card '" .. searchParam ..
                                   "' with gold back is already in play.")
                else
                    printToAll("Card '" .. searchParam ..
                                   "' with green or gold back is already in play.")
                end
                return true
            end
        end
    end

    -- finally, look for it in the past
    for _, object in ipairs(zonePast.getObjects()) do
        if object.tag == "Card" then
            if object.getName() == searchParam and
                (string.find(object.getDescription(), desc1) or
                    string.find(object.getDescription(), desc2)) then
                card_is_in_past = true
                break
            end
        elseif object.tag == "Deck" then
            for _, card in ipairs(object.getObjects()) do
                if card.name == searchParam and
                    (string.find(card.description, desc1) or
                        string.find(card.description, desc2)) then
                    card_is_in_past = true
                    break
                end
            end

            if card_is_in_past then break end
        end
    end

    if card_is_in_past then
        if not fetch_from_banish_pile and return_past_if_necessary then
            -- If the card we want is in the past and this is a situation where I can return the past if necessary, I return it here then call this function again
            printToAll("Card is in the Past. Returning Past and trying again.")
            returnCards({zonePast, true, {}})
            -- Wait until all cards have been returned and then try to grab the adventure card
            executeFunctionAfterCondition(function()
                return waitUntilZoneEmpty(numberOfCardsInPast);
            end, function()
                getAdvCard(cardNumber, is_white_search, is_gold_search,
                           is_only_green_search, pos, false, false)
            end, 1.5)
            return true
        elseif is_white_search then
            printToAll("Card '" .. searchParam ..
                           "' with white back is in the Past. Return all cards in the Past and try this search again if applicable.")
        elseif is_gold_search then
            printToAll("Card '" .. searchParam ..
                           "' with gold back is in the Past. Return all cards in the Past and try this search again if applicable.")
        else
            printToAll("Card '" .. searchParam ..
                           "' with green or gold back is in the Past. Return all cards in the Past and try this search again if applicable.")
        end
        return false
    elseif not target_deck_found then
        printToAll('No deck found.')
        return false
    else
        if is_white_search then
            printToAll("Card '" .. searchParam .. "' with white back not found.")
        elseif is_gold_search then
            printToAll("Card '" .. searchParam .. "' with gold back not found.")
        else
            printToAll("Card '" .. searchParam ..
                           "' with green or gold back not found.")
        end
        return false
    end
end

function getAdvCardRoutine(cardCode, name, desc, deck, lastCard, pos)
    -- returns true if found the relevant card and placed it where it needed to be placed
    local zoneGameboard = getObjectFromGUID(ZONES["ZONE_MAP"]["guid"])
    local zonePast = getObjectFromGUID(ZONES["ZONE_PAST"]["guid"])

    if deck then
        for _, card in ipairs(deck.getObjects()) do
            if card.name == name and string.find(card.description, desc) then
                local deckPos = deck.getPosition()
                local advCard = deck.takeObject({
                    position = {deckPos.x, deckPos.y + 2, deckPos.z},
                    guid = card.guid
                })
                advCard.setRotation({0, 180, 0})
                advCard.setPosition({pos.x, pos.y + 0.2, pos.z})

                local advCardConnections = getTerrainConnections(advCard)
                if CARD_BACK_ACTIONS[cardCode .. "__back"] then
                    createSpecialActionButtons(advCard);
                end
                Wait.time(function()
                    updateConnectionButtons(advCard, advCardConnections)
                end, 1)

                return true
            end
        end
    elseif lastCard then
        if lastCard.getName() == name and
            string.find(lastCard.getDescription(), "green") then
            lastCard.setRotation({0, 180, 0})
            lastCard.setPosition({pos.x, pos.y + 0.2, pos.z})

            local advCardConnections = getTerrainConnections(lastCard)
            if CARD_BACK_ACTIONS[cardCode .. "__back"] then
                createSpecialActionButtons(lastCard);
            end
            Wait.time(function()
                updateConnectionButtons(lastCard, advCardConnections)
            end, 1)

            return true
        end
    end

    return false
end

function createDudButtons(owner, direction)
    local name = getNameOfExploreCard(owner);
    local buttonIndex = getButtonIndex(owner, name, direction);

    local existingButtons = owner.getButtons()
    if existingButtons then
        local button_exists = false
        for _, button in ipairs(existingButtons) do
            if button["index"] == buttonIndex then
                button_exists = true
                break
            end
        end

        if button_exists then
            if direction == "n" then
                owner.editButton({
                    index = buttonIndex,
                    click_function = "dud",
                    function_owner = Global,
                    label = "",
                    position = {0, 0, -1.4},
                    rotation = {0, 0, 180},
                    scale = {0.5, 0.5, 0.5},
                    width = 900,
                    height = 300,
                    font_size = 200,
                    color = {1, 1, 1, 0},
                    font_color = {0, 0, 0, 100},
                    tooltip = "No further action"
                })
            elseif direction == "s" then
                owner.editButton({
                    index = buttonIndex,
                    click_function = "dud",
                    function_owner = Global,
                    label = "",
                    position = {0, 0, 1.4},
                    rotation = {0, 0, 180},
                    scale = {0.5, 0.5, 0.5},
                    width = 900,
                    height = 300,
                    font_size = 200,
                    color = {1, 1, 1, 0},
                    font_color = {0, 0, 0, 100},
                    tooltip = "No further action"
                })
            elseif direction == "e" then
                owner.editButton({
                    index = buttonIndex,
                    click_function = "dud",
                    function_owner = Global,
                    label = "",
                    position = {-0.9, 0, -0.1},
                    rotation = {0, 270, 180},
                    scale = {0.5, 0.5, 0.5},
                    width = 900,
                    height = 300,
                    font_size = 200,
                    color = {.5, .5, .5, 0},
                    font_color = {0, 0, 0, 100},
                    tooltip = "No further action"
                })
            elseif direction == "w" then
                owner.editButton({
                    index = buttonIndex,
                    click_function = "dud",
                    function_owner = Global,
                    label = "",
                    position = {0.9, 0, -0.1},
                    rotation = {0, 90, 180},
                    scale = {0.5, 0.5, 0.5},
                    width = 900,
                    height = 300,
                    font_size = 200,
                    color = {1, 1, 1, 0},
                    font_color = {0, 0, 0, 100},
                    tooltip = "No further action"
                })
            end
        end
    end
end

function updateConnectionButtons(owner, advCardConnections)
    if not advCardConnections then return end

    local zonePlayMat = getObjectFromGUID(ZONES["ZONE_MAP"]["guid"])
    local myPos = owner.getPosition()
    for _, object in ipairs(zonePlayMat.getObjects()) do
        if object.tag == "Card" then
            local objPos = object.getPosition()
            local objDesc = object.getDescription()
            local newCardConnections = getTerrainConnections(object)

            if math.abs(myPos.x - objPos.x) < 0.1 then
                if math.abs(myPos.z + 6.26 - objPos.z) < 0.1 then
                    -- this card is N of me
                    if string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") then
                        if advCardConnections[1] > 0 then
                            createDudButtons(owner, "n")
                        end
                        if newCardConnections and newCardConnections[2] > 0 then
                            createDudButtons(object, "s")
                        end
                    end
                elseif math.abs(myPos.z - 6.26 - objPos.z) < 0.1 then
                    -- this card is S of me
                    if string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") then
                        if newCardConnections and newCardConnections[1] > 0 then
                            createDudButtons(object, "n")
                        end
                        if advCardConnections[2] > 0 then
                            createDudButtons(owner, "s")
                        end
                    end
                end
            elseif math.abs(myPos.z - objPos.z) < 0.1 then
                if math.abs(myPos.x + 6.18 - objPos.x) < 0.1 then
                    -- this card is E of me
                    if string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") then
                        if advCardConnections[3] > 0 then
                            createDudButtons(owner, "e")
                        end
                        if newCardConnections and newCardConnections[4] > 0 then
                            createDudButtons(object, "w")
                        end
                    end
                elseif math.abs(myPos.x - 6.18 - objPos.x) < 0.1 then
                    -- this card is W of me
                    if string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") then
                        if newCardConnections and newCardConnections[3] > 0 then
                            createDudButtons(object, "e")
                        end
                        if advCardConnections[4] > 0 then
                            createDudButtons(owner, "w")
                        end
                    end
                end
            end
        end
    end
end

function exploration_card_already_played(card, origin, direction)
    local name = addTrailingZeros(card)
    name = "z" .. name

    if name == "z709" then
        -- Card 709 shows up multiple times
        name = name .. "_" .. origin.getGUID()
    end

    -- If there is already a card where this explore card would be played we just assume the relevant explore card has been played
    local zonePlayMat = getObjectFromGUID(ZONES["ZONE_MAP"]["guid"])
    local myPos = origin.getPosition()
    for _, object in ipairs(zonePlayMat.getObjects()) do
        if object.tag == "Card" then
            local objPos = object.getPosition()
            local objDesc = object.getDescription()
            if (direction == "n" or direction == "s") and
                math.abs(myPos.x - objPos.x) < 0.1 then
                if direction == "n" and math.abs(myPos.z + 6.26 - objPos.z) <
                    0.1 and
                    (string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") or
                        string.find(objDesc, "exploration")) then
                    EXPLORE_CARDS_PLAYED[name] = true
                    return true
                elseif direction == "s" and math.abs(myPos.z - 6.26 - objPos.z) <
                    0.1 and
                    (string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") or
                        string.find(objDesc, "exploration")) then
                    EXPLORE_CARDS_PLAYED[name] = true
                    return true
                end
            elseif (direction == "e" or direction == "w") and
                math.abs(myPos.z - objPos.z) < 0.1 then
                if direction == "e" and math.abs(myPos.x + 6.18 - objPos.x) <
                    0.1 and
                    (string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") or
                        string.find(objDesc, "exploration")) then
                    EXPLORE_CARDS_PLAYED[name] = true
                    return true
                elseif direction == "w" and math.abs(myPos.x - 6.18 - objPos.x) <
                    0.1 and
                    (string.find(objDesc, "green") or
                        string.find(objDesc, "gold") or
                        string.find(objDesc, "white") or
                        string.find(objDesc, "exploration")) then
                    EXPLORE_CARDS_PLAYED[name] = true
                    return true
                end
            end
        end
    end

    if name == "z499" or name == "z449" or name == "z475" or name == "z478" or
        name == "z497" then
        -- If we got all the way here and it's an Icy Maze card then we don't have an exploration card yet
        return false
    elseif not EXPLORE_CARDS_PLAYED[name] then
        if name ~= "z000" then EXPLORE_CARDS_PLAYED[name] = true end
        return false
    else
        return true
    end
end

function createSpecialActionButtons(owner)
    local name = getNameOfExploreCard(owner)
    local tooltipString = ""
    local h = 2
    local v = 4
    if SPECIAL_ACTIONS[name .. "_" .. owner.guid] then
        name = name .. "_" .. owner.guid
    end
    local type = nil

    if name == "z482" or name == "z490" then
        -- super special secret button that will only exist in case the right terrain card is visible on the mat
        if #(SPECIAL_ACTIONS[name]) < 2 then
            if CARDS_IN_ZONE_MAP["6e43e6"] then -- card 259
                local posCode = "H2.5 V"
                if name == "z482" then
                    posCode = posCode .. "6"
                else
                    posCode = posCode .. "5"
                end
                SPECIAL_ACTIONS[name][2] = {
                    banish = "259",
                    fetch = "259",
                    pos = posCode
                }
            end
        end
    end

    for k, specialActionList in pairs(SPECIAL_ACTIONS) do
        if k == name or string.find(k, name .. "__") then
            for i, specialAction in pairs(specialActionList) do
                if next(specialAction) ~= nil then
                    local ownerAction = specialAction.owner
                    if ownerAction then
                        if ownerAction == "banish" then
                            tooltipString = "Banish this "
                        elseif ownerAction == "discard" then
                            tooltipString = "Discard this "
                        elseif ownerAction == "return" then
                            tooltipString = "Return this "
                        elseif ownerAction == "satchel" then
                            tooltipString = "Store this "
                        end
                        if string.match(owner.getDescription(), "exploration") then
                            tooltipString = tooltipString .. "Exploration Card"
                        else
                            tooltipString = tooltipString .. "Adventure Card"
                        end
                        if ownerAction == "satchel" then
                            tooltipString = tooltipString ..
                                                " under the Satchel";
                        end
                        if string.match(owner.getDescription(), "exploration") and
                            owner.getGUID() ~= "de5c84" then
                            tooltipString = tooltipString ..
                                                " and replace it with an Adventure Card"
                        end
                    end

                    if name == "z585" and i == 2 then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString ..
                                            "Randomly shuffle 4 Advanced Skill Cards into the action deck"
                    end

                    local returnAction = specialAction.returnAction
                    if returnAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        if returnAction > -1 then
                            tooltipString =
                                tooltipString .. "Return " .. returnAction ..
                                    " Action Card"
                            if returnAction > 1 then
                                tooltipString = tooltipString .. "s";
                            end
                        else
                            tooltipString = tooltipString ..
                                                "Return all Action Cards"
                        end
                        tooltipString = tooltipString ..
                                            " from the Discard Pile"
                    end

                    local discardAction = specialAction.discardAction
                    if discardAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString .. "Discard " ..
                                            discardAction .. " Action Card"
                        if discardAction > 1 then
                            tooltipString = tooltipString .. "s";
                        end

                        tooltipString = tooltipString .. " from the Action Deck"
                    end

                    local banishAction = specialAction.banish
                    if banishAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString ..
                                            "Banish following Adventure Card"
                        if string.find(banishAction, ",") or
                            string.find(banishAction, "x") then
                            tooltipString = tooltipString .. "s"
                        end
                        tooltipString = tooltipString .. ": " .. banishAction
                    end

                    local banishPlayerCardAction =
                        specialAction.banishPlayerCard
                    local banishClueCardAction = specialAction.banishClueCard
                    if banishPlayerCardAction or banishClueCardAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString ..
                                            "Banish specified card(s)"
                    end

                    local resetAction = specialAction.rst
                    if specialAction.rst then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString ..
                                            "Return all cards and place "
                        if specialAction.rst == "balloonGreenTerrainCard" then
                            tooltipString = tooltipString ..
                                                "green or gold card corresponding to current balloon terrain card.";
                        elseif specialAction.rst == "balloonWhiteTerrainCard" then
                            tooltipString = tooltipString ..
                                                "white card corresponding to current balloon terrain card."
                        elseif specialAction.rst == true then
                            tooltipString = tooltipString .. "specified card."
                        else
                            tooltipString =
                                tooltipString .. "card " .. specialAction.rst
                        end
                    end

                    local retAction = specialAction.ret
                    if retAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString ..
                                            "Return Adventure Card " ..
                                            retAction
                    end

                    local discard = specialAction.discard
                    if discard then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString ..
                                            "Discard Adventure Card " .. discard
                    end

                    local fetchAction = specialAction.fetch
                    if fetchAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        if fetchAction == "gold" then
                            tooltipString = tooltipString ..
                                                "Replace this Adventure Card with one with gold back"
                        else
                            tooltipString = tooltipString ..
                                                "Place Adventure Card " ..
                                                fetchAction
                        end

                        if specialAction.is_white_search then
                            tooltipString = tooltipString .. " with white back"
                        end
                    end

                    local drawAction = specialAction.draw
                    local numadd = specialAction.numAdd
                    if drawAction then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString .. "Draw card"
                        if string.find(drawAction, ",") or
                            string.find(drawAction, "x") then
                            tooltipString = tooltipString .. "s"
                        end
                        if string.match(drawAction, "balloonWhiteTerrainCard") then
                            drawAction =
                                string.gsub(drawAction,
                                            "balloonWhiteTerrainCard",
                                            balloonWhiteTerrainCard);
                        end
                        tooltipString = tooltipString .. " " .. drawAction
                        if numadd then
                            tooltipString = tooltipString .. " + " .. numadd;
                        end
                    end

                    local dealStateToAll = specialAction.dealStateToAll
                    if dealStateToAll then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString ..
                                            "Give each character a " ..
                                            dealStateToAll .. " card"
                    end

                    local returnState = specialAction.returnState
                    if returnState then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString ..
                                            "Return each involved character's " ..
                                            returnState .. " state"
                        if string.find(returnState, ",") then
                            tooltipString = tooltipString .. "s";
                        end
                    end

                    local returnActiveState = specialAction.returnActiveState
                    if returnActiveState then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString ..
                                            "Return active character's " ..
                                            returnActiveState .. " state"
                        if string.find(returnActiveState, ",") then
                            tooltipString = tooltipString .. "s";
                        end
                    end

                    local dealState = specialAction.dealState
                    if dealState then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString ..
                                            "Select all involved characters to deal card"
                        if string.find(dealState, ",") or
                            string.find(dealState, "x") then
                            tooltipString = tooltipString .. "s"
                        end
                        tooltipString = tooltipString .. " " .. dealState
                    end

                    local dealActiveState = specialAction.dealActiveState
                    if dealActiveState then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString .. "Deal Card"
                        if string.find(dealActiveState, ",") or
                            string.find(dealActiveState, "x") then
                            tooltipString = tooltipString .. "s"
                        end
                        tooltipString =
                            tooltipString .. " " .. dealActiveState ..
                                " to one involved/active character"
                    end

                    local drawKeyword = specialAction.drawKeyword
                    if drawKeyword then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end

                        tooltipString = tooltipString ..
                                            "Search discard pile for cards with keyword"
                        if string.find(drawKeyword, ",") then
                            tooltipString = tooltipString .. "s"
                        end
                        tooltipString = tooltipString .. " " .. drawKeyword
                    end

                    local figurine = specialAction.figurine
                    if figurine then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        tooltipString = tooltipString .. "Place the " ..
                                            figurine .. " figurine"
                        if string.find(figurine, "x") or
                            string.find(figurine, ",") then
                            tooltipString = tooltipString .. "s"
                        end
                        tooltipString = tooltipString .. " on the board"
                    end

                    local fetchSpecifiedCard = specialAction.fetchSpecifiedCard
                    if fetchSpecifiedCard then
                        if tooltipString ~= "" then
                            tooltipString = tooltipString .. "\n";
                        end
                        if fetchSpecifiedCard == "fetch" then
                            tooltipString = tooltipString ..
                                                "Replace this card with specified card."
                        elseif fetchSpecifiedCard == "rst" then
                            tooltipString = tooltipString ..
                                                "Reset the board to specified card."
                        elseif fetchSpecifiedCard == "draw" then
                            tooltipString = tooltipString ..
                                                "Draw specified card."
                        end
                    end

                    if k == "z001_bdb6e1" then
                        tooltipString =
                            "Randomly take 7 cards from the Discard Pile\nDiscard those with the 'serenity' keyword\nShuffle the rest back into the Action Deck\nReturn this Adventure Card"
                    end

                    _G['takeSpecialAction__' .. k .. '__' .. i] = function(obj,
                                                                           col)
                        takeSpecialAction(obj, col, k, i)
                    end

                    local pos = specialAction.pos
                    if pos then
                        h = tonumber(string.match(pos, 'H(%d%.%d+).*'))
                        if not h then
                            h = tonumber(string.match(pos, 'H(%d).*'));
                        end
                        if not h then h = 2; end

                        v = tonumber(string.match(pos, '.*V(%d%.%d+).*'))
                        if not v then
                            v = tonumber(string.match(pos, '.*V(%d).*'));
                        end
                        if not v then v = 4; end
                    end
                    if EXPLORATION_PERMANENT_EVENTS[k] or
                        EXPLORATION_TEMP_EVENTS[k] or
                        EXPLORATION_ITEMS_BONUSES_STATES[k] then
                        pos = {(h - 2) * .5, 0, (v - 4) * 0.4}
                    else
                        pos = {(2 - h) * .5, 0, (v - 4) * 0.4}
                    end

                    if CARD_BACK_ACTIONS[k] or EXPLORATION_PERMANENT_EVENTS[k] or
                        EXPLORATION_TEMP_EVENTS[k] or
                        EXPLORATION_ITEMS_BONUSES_STATES[k] then
                        owner.createButton({
                            click_function = "takeSpecialAction__" .. k .. "__" ..
                                i,
                            function_owner = Global,
                            label = "",
                            position = pos,
                            rotation = {0, 0, 0},
                            scale = {0.5, 0.5, 0.5},
                            width = 400,
                            height = 400,
                            font_size = 200,
                            color = {1, 1, 1, 0},
                            font_color = {0, 0, 0, 100},
                            tooltip = tooltipString
                        })
                    else
                        owner.createButton({
                            click_function = "takeSpecialAction__" .. k .. "__" ..
                                i,
                            function_owner = Global,
                            label = "",
                            position = pos,
                            rotation = {0, 0, 180},
                            scale = {0.8, 0.5, 0.6},
                            width = 400,
                            height = 400,
                            font_size = 200,
                            color = {1, 1, 1, 0},
                            font_color = {0, 0, 0, 100},
                            tooltip = tooltipString
                        })
                    end
                    h = 2;
                    v = v + 0.6
                    tooltipString = ""
                end
            end
        end
    end
end

function takeSpecialAction(advCard, playerClickerColor, name, index)
    local pos = advCard.getPosition()
    local zoneMapObjects =
        getObjectFromGUID(ZONES['ZONE_MAP']['guid']).getObjects()
    local zoneBanishObjects =
        getObjectFromGUID(ZONES['ZONE_BANISH']['guid']).getObjects()
    local zonePastObjects =
        getObjectFromGUID(ZONES['ZONE_PAST']['guid']).getObjects()
    local zoneActionDeck = getObjectFromGUID(ZONES['ZONE_ACTION_DECK']['guid'])
    local zoneActionDiscard = getObjectFromGUID(
                                  ZONES['ZONE_ACTION_DISCARD']['guid'])
    local zoneMatObjects =
        getObjectFromGUID(ZONES['ZONE_MATS']['guid']).getObjects()
    local resetBoard = getObjectFromGUID(RESET_BOARD_GUID)
    local specialAction = SPECIAL_ACTIONS[name][index]

    local discard = specialAction.discard
    local ret = specialAction.ret
    local rst = specialAction.rst
    local banish = specialAction.banish
    local banishClueCard = specialAction.banishClueCard
    local banishPlayerCard = specialAction.banishPlayerCard
    local owner_action = specialAction.owner
    local is_white_search = specialAction.is_white_search
    local draw = specialAction.draw
    local fetch = specialAction.fetch
    local dealStateToAll = specialAction.dealStateToAll
    local dealState = specialAction.dealState
    local dealActiveState = specialAction.dealActiveState
    local returnState = specialAction.returnState
    local returnActiveState = specialAction.returnActiveState
    local banner = specialAction.banner
    local returnActionCards = specialAction.returnAction
    local discardActionCards = specialAction.discardAction
    local return_past_if_necessary = specialAction.return_past_if_necessary
    local necessaryKeyword = specialAction.necessaryKeyword
    local drawKeyword = specialAction.drawKeyword
    local figurine = specialAction.figurine
    local fetchSpecifiedCard = specialAction.fetchSpecifiedCard
    local numbox = 0
    local timeToWaitBeforeDrawOrFetch = 0

    local is_back_action = false
    if CARD_BACK_ACTIONS[name] and CARD_BACK_ACTIONS[name][index] then
        is_back_action = true;
    end

    if specialAction.balloonGreenTerrainCard then
        balloonGreenTerrainCard = specialAction.balloonGreenTerrainCard;
        balloonWhiteTerrainCard = "w" .. balloonGreenTerrainCard
    end
    if specialAction.balloonWhiteTerrainCard then
        balloonWhiteTerrainCard = specialAction.balloonWhiteTerrainCard;
        balloonGreenTerrainCard = string.match(balloonWhiteTerrainCard, "%d+")
    end
    local numadd = 0
    if specialAction.numAdd then numadd = specialAction.numAdd; end

    if name == "z001_bdb6e1" then
        -- This card returns only Action Cards that don't have the word "serenity" to the Action Deck
        local resetBoard = getObjectFromGUID(RESET_BOARD_GUID)
        resetBoard.call("draw", {7, true})
        Wait.time(function()
            local zoneActionObjects = getObjectFromGUID(
                                          ZONES['ZONE_ACTION']['guid']).getObjects()
            local zoneActionDeckObjects = zoneActionDeck.getObjects()
            local zoneActionDiscardObjects = zoneActionDiscard.getObjects()
            if #zoneActionObjects > 0 then
                local cardsToReturn = {}
                local cardsToDiscard = {}
                for _, obj in ipairs(zoneActionObjects) do
                    if obj.tag == "Card" then
                        local card_is_deck = false

                        for _, potentialCard in ipairs(zoneActionDeckObjects) do
                            if potentialCard.tag == "Card" and obj.guid ==
                                potentialCard.guid then
                                card_is_deck = true
                                break
                            end
                        end

                        if not card_is_deck then
                            for _, potentialCard in ipairs(
                                                        zoneActionDiscardObjects) do
                                if potentialCard.tag == "Card" and obj.guid ==
                                    potentialCard.guid then
                                    card_is_deck = true
                                    break
                                end
                            end

                            if not card_is_deck then
                                if string.find(obj.getDescription(), "serenity") then
                                    table.insert(cardsToDiscard, obj)
                                else
                                    table.insert(cardsToReturn, obj)
                                end
                            end
                        end
                    end
                end

                if #cardsToReturn > 0 then
                    returnCards({nil, false, cardsToReturn})
                end

                if #cardsToDiscard > 0 then
                    discardCards({nil, cardsToDiscard})
                end
            end

            executeFunctionAfterCondition(function()
                return waitUntilZoneEmpty(numberOfCardsInActionZone);
            end, shuffleActionDeck, 0)
        end, 1)
    end

    if figurine then
        local searchBags = {}
        local wgumcdBag = getObjectFromGUID(BAG_WGUMCD_TOKENS)
        local fteBag = getObjectFromGUID(BAG_FACING_THE_ELEMENTS_TOKENS)
        local frBag = getObjectFromGUID(BAG_FLYING_ROOTS_TOKENS)
        local ftdBag = getObjectFromGUID(BAG_FEAR_THE_DEVOURERS_TOKENS)
        local fireBag = getObjectFromGUID(BAG_FIRE_TOKENS)
        if wgumcdBag then table.insert(searchBags, wgumcdBag); end
        if fteBag then table.insert(searchBags, fteBag); end
        if frBag then table.insert(searchBags, frBag); end
        if ftdBag then table.insert(searchBags, ftdBag); end

        local figurinePos = pos
        figurinePos.y = figurinePos.y + 2

        for figurineToPlace in string.gmatch(figurine, '([^,]+),? *') do
            if string.find(figurineToPlace, "x") then
                numberOfFigurinesToPlace, figurineName = string.match(
                                                             figurineToPlace,
                                                             "(%d) *x *(.*)")
            else
                numberOfFigurinesToPlace = 1
                figurineName = figurineToPlace
            end

            -- local takenFigurineGUIDs = {}
            for i = 1, numberOfFigurinesToPlace do
                if figurineName == "fire" then
                    fireBag.takeObject({position = figurinePos})
                else
                    local found_figurine = false

                    for _, searchBag in ipairs(searchBags) do
                        for _, obj in ipairs(searchBag.getObjects()) do
                            if obj.name == figurineName then
                                local figurineObj =
                                    searchBag.takeObject({
                                        position = {0, 1.97, 15.65},
                                        guid = obj.guid
                                    })
                                figurineObj.setPosition(figurinePos)
                                -- table.insert(takenFigurineGUIDs, figurineObj.getGUID())
                                found_figurine = true
                                break
                            end
                        end

                        if found_figurine then break end
                    end

                    -- Logic for taking a figurine from the board if none can be found in the bags, but those are player choice so probably shouldn't use it
                    -- if not found_figurine then
                    --   for _, obj in ipairs(getObjectFromGUID(ZONES.ZONE_MAP.guid).getObjects()) do
                    --     if obj.getName() == figurineName then
                    --       local figurine_has_already_been_taken = false
                    --       for _, guid in ipairs(takenFigurineGUIDs) do
                    --         if guid == obj.getGUID() then
                    --           figurine_has_already_been_taken = true
                    --           break
                    --         end
                    --       end

                    --       if not figurine_has_already_been_taken then
                    --         obj.setPosition(figurinePos)
                    --         table.insert(takenFigurineGUIDs, obj.getGUID())
                    --         break
                    --       end
                    --     end
                    --   end
                    -- end
                end

                figurinePos.x = figurinePos.x + 2
            end

            timeToWaitBeforeDrawOrFetch = 0.2
        end
    end

    if banner then
        numbox = searchForBanner(banner)
        if numbox == 0 then
            printToAll("Banner not found.")
            return
        else
            printToAll("Banner found. Adding " .. numbox .. " to card number.")
        end
    end

    if banishClueCard then
        -- If I'm banishing specific clue cards because I ended a curse, add them to the banishPlayerCard list
        for banishString in string.gmatch(banishClueCard, '([^,]+),? *') do
            local banishGUID = "998_" .. BANNERS[banishString]['guid']
            if not banishPlayerCard then
                banishPlayerCard = banishGUID
            else
                banishPlayerCard = banishPlayerCard .. "," .. banishGUID
            end
        end
    end

    if banishPlayerCard then
        printToAll("Banishing specified card(s).")
        for banishString in string.gmatch(banishPlayerCard, '([^,]+),? *') do
            playerCardName, playerCardGUID =
                string.match(banishString, "(%d*)_(.*)")

            for _, obj in ipairs(zoneMatObjects) do
                if obj.tag == "Card" then
                    if (playerCardGUID and obj.getGUID() == playerCardGUID) or
                        obj.getName() == playerCardName then
                        banishCard(obj)
                        break
                    end
                elseif obj.tag == "Deck" then
                    local found_card = false
                    for _, card in ipairs(obj.getObjects()) do
                        if (playerCardGUID and card.guid == playerCardGUID) or
                            card.name == playerCardName then
                            obj.takeObject({
                                position = {
                                    BANISH_DECK_POS.x, BANISH_DECK_POS.y + 2,
                                    BANISH_DECK_POS.z
                                },
                                guid = card.guid,
                                rotation = getBanishCardRot(card.description)
                            })
                            break
                        end
                    end

                    if found_card then break end
                end
            end
        end

        timeToWaitBeforeDrawOrFetch = math.max(0.2, timeToWaitBeforeDrawOrFetch)
    end

    -- if dealing/returning cards to/from a player then deal/return first and then take owner and rst actions
    if returnState then
        createPlayerSelectButtons(advCard, playerClickerColor,
                                  "returnCardFromPlayer", returnState,
                                  owner_action, rst, is_back_action)
    elseif returnActiveState then
        createPlayerSelectButtons(advCard, playerClickerColor,
                                  "returnCardFromActivePlayer",
                                  returnActiveState, owner_action, rst,
                                  is_back_action)
    elseif dealState then
        createPlayerSelectButtons(advCard, playerClickerColor,
                                  "drawAndDealCardToPlayer", dealState,
                                  owner_action, rst, is_back_action)
    elseif dealActiveState then
        createPlayerSelectButtons(advCard, playerClickerColor,
                                  "drawAndDealCardToActivePlayer",
                                  dealActiveState, owner_action, rst,
                                  is_back_action)
    elseif fetchSpecifiedCard then
        advCard.clearButtons()
        local rot = {0, 0, 180}
        local posY = -1
        local posZ = -1.2
        if is_back_action then
            rot = {180, 180, 180};
            posY = 1;
        end
        selectNumberedCard = function(obj, playerClickerColor)
            local inputs = obj.getInputs()
            searchParam = string.match(inputs[1].value, "%d%d%d")
            if not searchParam then
                printToAll("Please input a valid card code.")
                obj.clearInputs()
                setupAdvCardButtons(obj)
            else
                if fetchSpecifiedCard == "draw" then
                    if TEMP_EVENTS[name] then
                        pos = {x = 2.18, y = 4, z = -29.22};
                    else
                        pos = {x = 2.18, y = 4, z = -28.72};
                    end
                end

                if getAdvCard(tonumber(searchParam), is_white_search, false,
                              false, pos, false, false) then
                    cancelAction(advCard)

                    if fetchSpecifiedCard == "draw" then
                        for _, pl in ipairs(Player.getPlayers()) do
                            pl.lookAt({
                                position = {x = 0, y = 0, z = -27},
                                pitch = 70,
                                distance = 15
                            })
                        end
                    end

                    if owner_action then
                        ownerSpecialActionHelper(obj, owner_action)
                    end

                    if fetchSpecifiedCard == "rst" then
                        Wait.time(function()
                            zoneMapObjects = getObjectFromGUID(
                                                 ZONES["ZONE_MAP"]["guid"]).getObjects()
                            for _, potentialCard in ipairs(zoneMapObjects) do
                                if tonumber(potentialCard.getName()) ==
                                    tonumber(searchParam) then
                                    resetToNumberedCard = function(obj,
                                                                   playerClickerColor)
                                        obj.clearButtons()
                                        Wait.time(function()
                                            resetBoard.call("resetBoardRoutine",
                                                            searchParam)
                                        end, 1)
                                    end

                                    potentialCard.createButton({
                                        click_function = "cancelAction",
                                        function_owner = Global,
                                        label = "Cancel",
                                        position = {0.5, 1, -1.2},
                                        rotation = {180, 180, 180},
                                        width = 500,
                                        height = 300,
                                        font_size = 100,
                                        font_color = {0, 0, 0}
                                    })
                                    potentialCard.createButton({
                                        click_function = "resetToNumberedCard",
                                        function_owner = Global,
                                        label = "Reset",
                                        position = {-0.5, 1, -1.2},
                                        rotation = {180, 180, 180},
                                        width = 500,
                                        height = 300,
                                        font_size = 100,
                                        font_color = {0, 0, 0}
                                    })

                                    return
                                end
                            end
                        end, 0.2)

                        -- TODO any cards that get here may perhaps have a non-numerical rst? haven't found any yet tho
                    end
                else
                    obj.editInput({index = 0, value = ''})
                end

                pos = advCard.getPosition()
            end
        end
        advCard.createInput({
            input_function = "dud",
            label = "Type in Number",
            function_owner = Global,
            position = {0, posY, posZ},
            rotation = rot,
            width = 1000,
            height = 200,
            font_size = 100,
            font_color = {0, 0, 0},
            value = ""
        })
        posZ = posZ + 0.5
        advCard.createButton({
            click_function = "cancelAction",
            function_owner = Global,
            label = "Cancel",
            position = {0.5, posY, posZ},
            rotation = rot,
            width = 500,
            height = 300,
            font_size = 100,
            font_color = {0, 0, 0}
        })
        advCard.createButton({
            click_function = "selectNumberedCard",
            function_owner = Global,
            label = "Done",
            position = {-0.5, posY, posZ},
            rotation = rot,
            width = 500,
            height = 300,
            font_size = 100,
            font_color = {0, 0, 0}
        })
        return
    else
        if owner_action then
            ownerSpecialActionHelper(advCard, owner_action)
            timeToWaitBeforeDrawOrFetch = math.max(0.2,
                                                   timeToWaitBeforeDrawOrFetch)
        end

        if rst then
            local rstNumbox = 0
            if string.find(rst, "banner") then
                rstNumbox = numbox;
                rst = string.match(rst, "%d+")
            end
            if rst == "balloonWhiteTerrainCard" then
                rst = balloonWhiteTerrainCard;
            end
            if rst == "balloonGreenTerrainCard" then
                rst = balloonGreenTerrainCard;
            end
            if banner then
                rst = addTrailingZeros(tonumber(rst) + rstNumbox);
            end
            if not returnState and not dealState and not returnActiveState and
                not dealActiveState then
                Wait.time(function()
                    resetBoard.call("resetBoardRoutine", rst)
                end, 1)
            end

            timeToWaitBeforeDrawOrFetch = math.max(8,
                                                   timeToWaitBeforeDrawOrFetch)
        end
    end

    if returnActionCards then
        local numCardsToReturn = returnActionCards
        if numCardsToReturn < 0 then
            numCardsToReturn = numberOfCardsInDiscardPile()
        end

        local z585WaitTime = 0
        if name == "z585" then
            -- Grab 4 random Advanced Skill cards and shuffle them into Skill deck
            advancedSkillDeck = getObjectFromGUID(ADVANCED_SKILL_DECK)
            advancedSkillDeck.shuffle()
            Wait.time(function()
                advancedSkillDeck.takeObject({
                    position = {
                        ACTION_DECK_POS[1], ACTION_DECK_POS[2] + 2,
                        ACTION_DECK_POS[3]
                    },
                    rotation = {0, 180, 180}
                })
                Wait.time(function()
                    advancedSkillDeck.takeObject({
                        position = {
                            ACTION_DECK_POS[1], ACTION_DECK_POS[2] + 2,
                            ACTION_DECK_POS[3]
                        },
                        rotation = {0, 180, 180}
                    })
                end, 0.1)
                Wait.time(function()
                    advancedSkillDeck.takeObject({
                        position = {
                            ACTION_DECK_POS[1], ACTION_DECK_POS[2] + 2,
                            ACTION_DECK_POS[3]
                        },
                        rotation = {0, 180, 180}
                    })
                end, 0.2)
                Wait.time(function()
                    advancedSkillDeck.takeObject({
                        position = {
                            ACTION_DECK_POS[1], ACTION_DECK_POS[2] + 2,
                            ACTION_DECK_POS[3]
                        },
                        rotation = {0, 180, 180}
                    })
                end, 0.3)
            end, 0.2)

            z585WaitTime = 0.6

            -- TODO edge case when advanced skill deck is no longer there or is only one card
        end

        if numCardsToReturn > 0 then
            Wait.time(function()
                returnActionCardsFromDiscardPile(returnActionCards)
            end, z585WaitTime)
        end

        Wait.time(shuffleActionDeck, numCardsToReturn * 0.1 + z585WaitTime + 2)
    end

    if discardActionCards then
        discardActionCardsFromActionDeck(discardActionCards)

        Wait.time(shuffleActionDeck, discardActionCards * 0.1 + 2)
    end

    if discard then
        local card_found = false
        local is_only_green_search = string.match(discard, "green")
        discard = string.match(discard, "%d+")
        for _, obj in ipairs(zoneMapObjects) do
            if obj.getName() == discard then
                pos = obj.getPosition()
                discardNextCard(obj, false)
                card_found = true
                break
            end
        end
        if not card_found then -- discard card from adventure deck
            getAdvCard(tonumber(discard), is_white_search, false,
                       is_only_green_search, PAST_DECK_POS, false,
                       return_past_if_necessary)
        end

        timeToWaitBeforeDrawOrFetch = math.max(0.4, timeToWaitBeforeDrawOrFetch)
    end

    if banish then
        banishWaitTime = -0.1
        for banishString in string.gmatch(banish, '([^,]+),? *') do
            banishWaitTime = banishWaitTime + 0.1
            Wait.time(function()
                if string.find(banishString, "x") then
                    numberOfCardsToDraw, drawCardName = string.match(
                                                            banishString,
                                                            "(%d) *x *(.*)")
                else
                    numberOfCardsToDraw = 1
                    drawCardName = banishString
                end

                local is_only_green_search = string.match(drawCardName, "green")
                local is_white_search = string.match(drawCardName, "white")
                local name = string.match(drawCardName, "%d+")

                printToAll("Banishing " .. banishString .. ".")
                for i = 1, numberOfCardsToDraw do
                    local card_found = false
                    local searchZones = {"ZONE_MAP", "ZONE_SATCHEL"}
                    for _, zone in ipairs(searchZones) do
                        local zoneObjects = getObjectFromGUID(
                                                ZONES[zone]['guid']).getObjects()
                        for _, obj in ipairs(zoneObjects) do
                            if obj.tag == "Deck" then
                                for _, card in ipairs(obj.getObjects()) do
                                    if card.name == name and
                                        (not is_only_green_search or
                                            string.find(card.description,
                                                        "green")) then
                                        obj.takeObject({
                                            position = {
                                                BANISH_DECK_POS.x,
                                                BANISH_DECK_POS.y + 2,
                                                BANISH_DECK_POS.z
                                            },
                                            guid = card.guid,
                                            rotation = getBanishCardRot(
                                                card.description)
                                        })
                                        card_found = true
                                        break
                                    end
                                end
                                if card_found then
                                    pos = obj.getPosition()
                                    break
                                end
                            elseif obj.getName() == name and
                                (not is_only_green_search or
                                    string.find(obj.getDescription(), "green")) then
                                pos = obj.getPosition()
                                banishCard(obj)
                                card_found = true
                                break
                            end
                        end
                    end
                    if not card_found then -- banish card from adventure deck
                        getAdvCard(tonumber(name), is_white_search, false,
                                   is_only_green_search, BANISH_DECK_POS, false,
                                   return_past_if_necessary)
                    end
                end
            end, banishWaitTime)
        end

        timeToWaitBeforeDrawOrFetch = math.max(0.2 + banishWaitTime,
                                               timeToWaitBeforeDrawOrFetch)
    end

    if ret then
        for returnString in string.gmatch(ret, '([^,]+),? *') do
            printToAll("Returning " .. returnString .. ".")
            if string.find(returnString, "banished") then
                targetZone = zoneBanishObjects
                returnString = string.gsub(returnString, "banished ", "")
            elseif string.find(returnString, "past") then
                targetZone = zonePastObjects
                returnString = string.gsub(returnString, "past ", "")
            else
                targetZone = zoneMapObjects
            end

            for _, obj in ipairs(targetZone) do
                if obj.tag == "Deck" then
                    for _, card in ipairs(obj.getObjects()) do
                        if card.name == returnString then
                            local return_card = obj.takeObject({
                                guid = card.guid
                            })
                            returnNextCard(return_card, false, false)
                            break
                        end
                    end
                elseif obj.getName() == returnString then
                    pos = obj.getPosition()
                    returnNextCard(obj, false, false)
                    break
                end
            end
        end

        timeToWaitBeforeDrawOrFetch = math.max(0.4, timeToWaitBeforeDrawOrFetch)
    end

    if drawKeyword then
        printToAll("Drawing all action cards with keyword " .. drawKeyword ..
                       " in the action discard pile.")
        for name in string.gmatch(drawKeyword, '([^,]+),? *') do
            resetBoard.call("getKeywordCardsRoutine", name)
        end

        timeToWaitBeforeDrawOrFetch = math.max(1.5, timeToWaitBeforeDrawOrFetch)
    end

    -- do a short wait before fetching in case the above actions are still happening
    Wait.time(function()
        if fetch then
            local fetchNumbox = 0
            local fetch_from_banish_pile = false;
            if string.find(fetch, "banner") then
                fetchNumbox = numbox;
                fetch = string.gsub(fetch, "[\\+ ]*banner *", "");
            end
            if string.find(fetch, "banished") then
                fetch_from_banish_pile = true;
                fetch = string.gsub(fetch, " *banished *", "");
            end
            for _, potentialFigurine in ipairs(zoneMapObjects) do
                -- Lift a figurine just a teensy bit so it doesn't collide with the card that's being placed, just in case
                if string.find(potentialFigurine.getDescription(), "figurine") then
                    local figurinePos = potentialFigurine.getPosition()
                    potentialFigurine.setPosition({
                        figurinePos.x, figurinePos.y + 0.25, figurinePos.z
                    })
                end
            end

            if fetch == "gold" then
                getAdvCard(tonumber(advCard.getName()), false, true, false, pos,
                           false, return_past_if_necessary)
            else
                getAdvCard(tonumber(fetch) + fetchNumbox, is_white_search,
                           false, false, pos, fetch_from_banish_pile,
                           return_past_if_necessary)
            end
        end

        if draw then
            local is_white_search = false
            local is_gold_search = false
            local drawNumbox = 0
            local draw_from_banish_pile = false;
            if searchForKeyword(necessaryKeyword) then
                -- If draw action is triggered from a temp event then place the drawn card one row below the temp event card
                if TEMP_EVENTS[name] then
                    pos = {x = 2.18, y = 4, z = -29.22};
                else
                    pos = {x = 2.18, y = 4, z = -28.72};
                end

                if string.match(draw, "balloonWhiteTerrainCard") then
                    draw = string.gsub(draw, "balloonWhiteTerrainCard",
                                       balloonWhiteTerrainCard);
                end

                for drawCardName in string.gmatch(draw, '([^,]+),? *') do
                    if string.find(drawCardName, "x") then
                        numberOfCardsToDraw, drawCardName = string.match(
                                                                drawCardName,
                                                                "(%d) *x *(.*)")
                    else
                        numberOfCardsToDraw = 1
                    end

                    if string.find(drawCardName, "banner") then
                        drawNumbox = numbox;
                    end
                    if string.find(drawCardName, "banished") then
                        draw_from_banish_pile = true;
                    end
                    if string.find(drawCardName, "w") then
                        is_white_search = true;
                    end
                    if string.find(drawCardName, "g") then
                        is_gold_search = true;
                    end

                    drawCardName = string.match(drawCardName, "%d+")

                    if tonumber(numberOfCardsToDraw) == 7 and drawCardName ==
                        "003" then
                        numberOfCardsToDraw = 1
                        is_gold_search = true
                    end

                    for i = 1, numberOfCardsToDraw do
                        -- Send weather card to Weather location on board
                        --            if drawCardName == "700" then
                        --                getAdvCard(tonumber(drawCardName), is_white_search, false, false, WEATHER_POS, false, return_past_if_necessary)
                        --              else
                        getAdvCard(tonumber(drawCardName) + drawNumbox + numadd,
                                   is_white_search, is_gold_search, false, pos,
                                   draw_from_banish_pile,
                                   return_past_if_necessary)
                        pos.x = pos.x + 0.5
                        pos.y = pos.y + 0.5
                        --            end
                    end
                    pos.z = pos.z - 0.5
                    pos.x = 2.18
                end
                if not returnState and not dealState and not returnActiveState and
                    not dealActiveState then -- if not selecting player then look at search mat
                    for _, pl in ipairs(Player.getPlayers()) do
                        pl.lookAt({
                            position = {x = 0, y = 0, z = -27},
                            pitch = 70,
                            distance = 15
                        })
                    end
                end
            else
                printToAll("Keyword " .. necessaryKeyword ..
                               " not found on any character mat.")
            end
        end

        if dealStateToAll then
            for playerNum = 1, NUM_PLAYERS do
                local zonePlayerMat = getObjectFromGUID(ZONES["ZONE_P" ..
                                                            playerNum]['guid'])
                local playerMatPos = returnNextOpenPos(zonePlayerMat,
                                                       playerRedStatePos[playerNum],
                                                       5, 0, 2.3, 0)
                for name in string.gmatch(dealStateToAll, '([^,]+),? *') do
                    getAdvCard(tonumber(name), is_white_search, false, false,
                               playerMatPos, false, false)
                    printToAll("Sending " .. name .. " card to " ..
                                   PLAYER_NAME[playerNum] .. "\'s mat.")
                    pos.x = pos.x - 2.3
                end
            end
        end

    end, timeToWaitBeforeDrawOrFetch)
end

function ownerSpecialActionHelper(card, owner_action)
    card.clearButtons()
    local ownerPos = card.getPosition()
    if owner_action == "banish" then
        printToAll("Banishing card " .. card.getName() .. ".")
        banishCard(card)
    elseif owner_action == "discard" then
        printToAll("Discarding card " .. card.getName() .. ".")
        discardNextCard(card, false)
    elseif owner_action == "return" then
        printToAll("Returning card " .. card.getName() .. ".")
        returnNextCard(card, false, false)
    elseif owner_action == "satchel" then
        printToAll("Storing card " .. card.getName() .. " under satchel.")
        storeCard(card)
    end

    if string.find(card.getDescription(), "exploration") and card.getGUID() ~=
        "de5c84" and CARDS_IN_ZONE_MAP[card.guid] then
        Wait.time(function() placeAdvCard(ownerPos) end, 0.5);
    end
end

function onObjectRotate(obj, spin, flip, playerColor, oldSpin, oldFlip)
    if obj.tag == "Card" then
        local name = getNameOfExploreCard(obj)
        local exploreCard = string.find(obj.getDescription(), "exploration")
        if (not exploreCard and flip == 180) or (exploreCard and flip == 0) then
            Wait.time(function() setupAdvCardButtons(obj) end, 0.1)
        elseif CARD_BACK_ACTIONS[name .. "__back"] then
            createSpecialActionButtons(obj)
        end
    end
end

function onObjectLeaveContainer(container, object)
    local desc = object.getDescription()
    if string.find(desc, 'archive') then
        if container then
            local params = {}
            local destination = container.getPosition()
            destination.y = destination.y + 2

            params.position = destination
            local clone = object.clone(params)
            desc = string.gsub(desc, 'archive', 'copy')
            object.setDescription(desc)
        end
    end

    local name = getNameOfExploreCard(object)
end

function onObjectDrop(playerColor, obj)
    local name = getNameOfExploreCard(obj)
    local cardFaceUp = true
    if obj.tag == "Card" then
        local exploreCard = string.find(obj.getDescription(), "exploration")
        if (exploreCard and obj.getRotation().z > 150) or
            (not exploreCard and obj.getRotation().z < 30) then
            cardFaceUp = false
        end
        if CARD_BACK_ACTIONS[name .. "__back"] and not cardFaceUp then
            createSpecialActionButtons(obj)
        elseif cardFaceUp then
            Wait.time(function() setupAdvCardButtons(obj) end, 0.1)
        end
    end
end

function setupAdvCardButtons(card)
    local name = getNameOfExploreCard(card)
    if SPECIAL_ACTIONS[name .. "_" .. card.guid] then
        name = name .. "_" .. card.guid;
    end

    local desc = card.getDescription()
    card.clearButtons()

    -- setup buttons for terrain cards and permanent events only in the play mat
    if CARDS_IN_ZONE_MAP[card.guid] then
        if TERRAIN_CARD_CONNECTIONS[name] then
            local advCardConnections = getTerrainConnections(card)
            if advCardConnections then
                if advCardConnections[1] ~= 0 or advCardConnections[2] ~= 0 or
                    advCardConnections[3] ~= 0 or advCardConnections[4] ~= 0 then
                    Wait.time(function()
                        createAdvButtons(card)
                    end, .1)
                    Wait.time(function()
                        updateConnectionButtons(card, advCardConnections)
                    end, .1)
                end
            end
        end

        if ZONE_CONNECTIONS[name] then
            Wait.time(function() addExploreCards(card) end, .1)
            EXPLORE_CARDS_PLAYED[name] = true
        end

        if PERMANENT_EVENTS[name] then
            Wait.time(function() createSpecialActionButtons(card) end, 1)
        end
    end

    -- setup discard/store buttons for TEMP_EVENTS, ITEMS_BONUSES_STATES, SATCHEL_CARDS and exploration cards
    if SATCHEL_CARDS[name] or EXPLORATION_SATCHEL_CARDS[name] then
        createDiscardButton(card, "satchel")
    elseif ITEMS_BONUSES_STATES[name] or EXPLORATION_ITEMS_BONUSES_STATES[name] then
        createDiscardButton(card, "items")
    elseif WEATHER_CARDS[name] or EXPLORATION_WEATHER_CARDS[name] then
        createDiscardButton(card, "weather")
    elseif TEMP_EVENTS[name] or
        (string.find(desc, "exploration") and
            not EXPLORATION_PERMANENT_EVENTS[name]) then
        createDiscardButton(card, "temp")
    end

    -- setup buttons for TEMP_EVENTS, ITEMS_BONUSES_STATES, SATCHEL_CARDS, WEATHER_CARDS and exploration cards anywhere in the world
    if TEMP_EVENTS[name] or ITEMS_BONUSES_STATES[name] or SATCHEL_CARDS[name] or
        WEATHER_CARDS[name] or string.find(desc, "exploration") then
        Wait.time(function() createSpecialActionButtons(card) end, .1)
    end

end

function getTerrainConnections(card)
    cardName = getNameOfExploreCard(card)
    if cardName == "z499" or cardName == "z449" or cardName == "z475" or
        cardName == "z478" or cardName == "z497" then
        return ICY_MAZE_TERRAIN_CARD_CONNECTIONS["z" .. card.guid]
    else
        return TERRAIN_CARD_CONNECTIONS[cardName]
    end
end

function clearExploreCardsTable(params) EXPLORE_CARDS_PLAYED = {} end

function searchForBanner(banner)
    local searchZones = {
        "ZONE_P1", "ZONE_P2", "ZONE_P3", "ZONE_P4", "ZONE_SATCHEL"
    }
    local numbox = 0
    for _, zone in ipairs(searchZones) do
        local zoneMatObjects =
            getObjectFromGUID(ZONES[zone]['guid']).getObjects()
        for name in string.gmatch(banner, '([^,]+),? *') do
            for _, obj in ipairs(zoneMatObjects) do
                if obj.tag == "Deck" then
                    for _, card in ipairs(obj.getObjects()) do
                        if string.find(BANNERS[name]['guid'], card.guid) then
                            numbox = numbox + BANNERS[name]['numbox']
                            break
                        end
                    end
                elseif string.find(BANNERS[name]['guid'], obj.getGUID()) then
                    numbox = numbox + BANNERS[name]['numbox']
                    break
                end
            end
        end
    end
    return numbox
end

function searchForKeyword(keyword)
    if keyword == nil then return true end
    local searchZones = {"ZONE_P1", "ZONE_P2", "ZONE_P3", "ZONE_P4"}
    for _, zone in ipairs(searchZones) do
        local zoneMatObjects =
            getObjectFromGUID(ZONES[zone]['guid']).getObjects()
        for _, obj in ipairs(zoneMatObjects) do
            if obj.tag == "Deck" then
                for _, card in ipairs(obj.getObjects()) do
                    if string.find(card.description, keyword) then
                        printToAll("Keyword " .. keyword ..
                                       " found on character mat.")
                        return true
                    end
                end
            elseif string.find(obj.getDescription(), keyword) then
                printToAll("Keyword " .. keyword .. " found on character mat.")
                return true
            end
        end
    end
    return false
end

function loadPlayerMats(verbose)
    if verbose then
        printToAll("Setting player mats for " .. NUM_PLAYERS .. " players.")
    end

    for _, obj in ipairs(getAllObjects()) do
        if obj.getName() == "player1_mat" or obj.getName() == "player2_mat" or
            obj.getName() == "player3_mat" or obj.getName() == "player4_mat" then
            obj.interactable = false
        end
    end

    players = {
        "White", "Brown", "Red", "Orange", "Yellow", "Green", "Teal", "Blue",
        "Purple", "Pink", "Grey", "Black"
    }
    for _, obj in ipairs(getAllObjects()) do
        if obj.getName() == "player1_mat" or obj.getName() == "player2_mat" or
            obj.getName() == "player3_mat" or obj.getName() == "player4_mat" then
            if obj.getName() == "player4_mat" and NUM_PLAYERS < 4 then
                obj.setInvisibleTo(players)
            elseif obj.getName() == "player3_mat" and NUM_PLAYERS < 3 then
                obj.setInvisibleTo(players)
            elseif obj.getName() == "player2_mat" and NUM_PLAYERS < 2 then
                obj.setInvisibleTo(players)
            else
                obj.setInvisibleTo()
                if obj.getStateId() ~= NUM_PLAYERS then
                    obj.setState(NUM_PLAYERS)
                end
            end
        end
    end
end

function returnNextOpenPos(zone, startingPos, xRange, zRange, xStep, zStep,
                           cardName)
    local pos = {x = 0, y = startingPos.y, z = 0}
    local card_found
    for z = 0, zRange do
        for x = 0, xRange do
            pos.x = startingPos.x + x * xStep
            pos.z = startingPos.z - z * zStep
            card_found = false
            for _, object in ipairs(zone.getObjects()) do
                if object.tag == "Card" or object.tag == "Deck" then
                    objectPos = object.getPosition()
                    if math.abs(pos.x - objectPos.x) < 0.1 and
                        math.abs(pos.z + -objectPos.z) < 0.1 then
                        if object.tag == "Card" and object.getName() ~= cardName then
                            card_found = true
                            break
                        elseif object.tag == "Deck" then
                            for _, card in ipairs(object.getObjects()) do
                                if card.name ~= cardName then
                                    card_found = true
                                    break
                                end
                            end
                        end
                    end
                end
            end
            if not card_found then break end
        end
        if not card_found then break end
    end
    return (pos)
end

function cancelAction(obj)
    obj.clearButtons()
    obj.clearInputs()
    for playerNum = 1, NUM_PLAYERS do
        obj.removeTag("Select Player " .. playerNum)
    end
    setupAdvCardButtons(obj)
end

function dud() end
