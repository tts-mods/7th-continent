function onLoad()
    createButtons()
    bag_rules = getObjectFromGUID('abc131')
    zone = getObjectFromGUID('f5ba77')
    contents_menu = getObjectFromGUID('5b1e39')
end

function createButtons()
    self.createButton({
        click_function = "previous_page",
        function_owner = self,
        label = "",
        position = {-5.3, 0.6, 7.9},
        width = 2000,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "next_page",
        function_owner = self,
        label = "",
        position = {5.2, 0.6, 7.9},
        width = 2000,
        height = 300,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "remove_current_page",
        function_owner = self,
        label = "",
        position = {0, 0.6, 8},
        scale = {0.5, 0.5, 0.5},
        width = 4100,
        height = 400,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })
end

function remove_current_page()
    for _, object in ipairs(zone.getObjects()) do
        if object.tag == "Card" or object.tag == "Deck" then
            object.destroy()
        end
    end
end

function next_page()
    remove_current_page()
    for _, page in ipairs(zone.getObjects()) do
        if page.getName() == "Page 1" then
            contents_menu.call("display_2")
        elseif page.getName() == "Page 2" then
            contents_menu.call("display_3")
        elseif page.getName() == "Page 3" then
            contents_menu.call("display_4")
        elseif page.getName() == "Page 4" then
            contents_menu.call("display_5")
        elseif page.getName() == "Page 5" then
            contents_menu.call("display_6")
        elseif page.getName() == "Page 6" then
            contents_menu.call("display_7")
        elseif page.getName() == "Page 7" then
            contents_menu.call("display_8")
        elseif page.getName() == "Page 8" then
            contents_menu.call("display_9")
        elseif page.getName() == "Page 9" then
            contents_menu.call("display_10")

        elseif page.getName() == "Page 10" then
            contents_menu.call("display_11")
        elseif page.getName() == "Page 11" then
            contents_menu.call("display_12")
        elseif page.getName() == "Page 12" then
            contents_menu.call("display_13")
        elseif page.getName() == "Page 13" then
            contents_menu.call("display_14")
        elseif page.getName() == "Page 14" then
            contents_menu.call("display_15")
        elseif page.getName() == "Page 15" then
            contents_menu.call("display_16")
        elseif page.getName() == "Page 16" then
            contents_menu.call("display_17")
        elseif page.getName() == "Page 17" then
            contents_menu.call("display_18")
        elseif page.getName() == "Page 18" then
            contents_menu.call("display_19")
        elseif page.getName() == "Page 19" then
            contents_menu.call("display_20")
        elseif page.getName() == "Page 20" then
            contents_menu.call("display_21")
        elseif page.getName() == "Page 21" then
            contents_menu.call("display_22")
        elseif page.getName() == "Page 22" then
            contents_menu.call("display_23")
        elseif page.getName() == "Page 23" then
            contents_menu.call("display_24")

        end
    end
end

function previous_page()
    remove_current_page()
    for _, page in ipairs(zone.getObjects()) do

        if page.getName() == "Page 2" then
            contents_menu.call("display_1")
        elseif page.getName() == "Page 3" then
            contents_menu.call("display_2")
        elseif page.getName() == "Page 4" then
            contents_menu.call("display_3")
        elseif page.getName() == "Page 5" then
            contents_menu.call("display_4")
        elseif page.getName() == "Page 6" then
            contents_menu.call("display_5")
        elseif page.getName() == "Page 7" then
            contents_menu.call("display_6")
        elseif page.getName() == "Page 8" then
            contents_menu.call("display_7")
        elseif page.getName() == "Page 9" then
            contents_menu.call("display_8")

        elseif page.getName() == "Page 10" then
            contents_menu.call("display_9")
        elseif page.getName() == "Page 11" then
            contents_menu.call("display_10")
        elseif page.getName() == "Page 12" then
            contents_menu.call("display_11")
        elseif page.getName() == "Page 13" then
            contents_menu.call("display_12")
        elseif page.getName() == "Page 14" then
            contents_menu.call("display_13")
        elseif page.getName() == "Page 15" then
            contents_menu.call("display_14")
        elseif page.getName() == "Page 16" then
            contents_menu.call("display_15")
        elseif page.getName() == "Page 17" then
            contents_menu.call("display_16")
        elseif page.getName() == "Page 18" then
            contents_menu.call("display_17")
        elseif page.getName() == "Page 19" then
            contents_menu.call("display_18")
        elseif page.getName() == "Page 20" then
            contents_menu.call("display_19")
        elseif page.getName() == "Page 21" then
            contents_menu.call("display_20")
        elseif page.getName() == "Page 22" then
            contents_menu.call("display_21")
        elseif page.getName() == "Page 23" then
            contents_menu.call("display_22")
        elseif page.getName() == "Page 24" then
            contents_menu.call("display_23")

        end
    end
end

function remove_current_page()
    for _, object in ipairs(zone.getObjects()) do
        if object.tag == "Card" or object.tag == "Deck" then
            object.destroy()
        end
    end
end
