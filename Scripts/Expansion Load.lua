function onLoad()
    self.createButton({
        click_function = "setup",
        function_owner = self,
        tooltip = "Set Up Swamp of Madness (Expansion Only)",
        position = {0, 0.6, 0},
        rotation = {0, 0, 0},
        scale = {0.5, 0.5, 0.5},
        width = 2300,
        height = 2800,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })
end

function setup() Global.call('setExpansionOrCurse', {"swamp_ep", false}) end
