function onLoad()
    createButtons()
    createInput()
    zoneActionDeck = getObjectFromGUID(Global.getTable('ZONES').ZONE_ACTION_DECK
                                           .guid)
    zonePast = getObjectFromGUID(Global.getTable('ZONES').ZONE_PAST.guid)
    zoneDiscard = getObjectFromGUID(Global.getTable('ZONES').ZONE_ACTION_DISCARD
                                        .guid)
    bagCurses = getObjectFromGUID('6dd285')
    zoneDrawnCards =
        getObjectFromGUID(Global.getTable('ZONES').ZONE_ACTION.guid)
end

pos1 = {-2.35, 2.24, -41.38}
pos2 = {0.00, 2.23, -41.38}
pos3 = {2.35, 2.23, -41.38}
pos4 = {-5.85, 2.24, -39.03}
pos5 = {-3.49, 2.24, -39.03}
pos6 = {-1.14, 2.24, -39.03}
pos7 = {1.14, 2.23, -39.03}
pos8 = {3.49, 2.23, -39.03}
pos9 = {5.85, 2.23, -39.03}
pos10 = {-4.65, 2.24, -36.66}
pos11 = {-2.29, 2.24, -36.66}
pos12 = {0.06, 2.23, -36.66}
pos13 = {2.42, 2.23, -36.66}
pos14 = {4.69, 2.23, -36.66}

function createButtons()
    self.createButton({
        click_function = "draw_8",
        function_owner = self,
        label = "",
        position = {-2.7, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_9",
        function_owner = self,
        label = "",
        position = {-1.8, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_10",
        function_owner = self,
        label = "",
        position = {-0.9, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_11",
        function_owner = self,
        label = "",
        position = {0, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_12",
        function_owner = self,
        label = "",
        position = {0.9, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_13",
        function_owner = self,
        label = "",
        position = {1.7, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_14",
        function_owner = self,
        label = "",
        position = {2.6, 0.6, -0.3},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_7",
        function_owner = self,
        label = "",
        position = {2.3, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_6",
        function_owner = self,
        label = "",
        position = {1.5, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_5",
        function_owner = self,
        label = "",
        position = {0.7, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_4",
        function_owner = self,
        label = "",
        position = {-0.1, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_3",
        function_owner = self,
        label = "",
        position = {-0.9, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_2",
        function_owner = self,
        label = "",
        position = {-1.7, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "draw_1",
        function_owner = self,
        label = "",
        position = {-2.5, 0.6, -0.75},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "returnPast",
        function_owner = self,
        label = "",
        position = {-5.6, 0.6, 3.6},
        scale = {0.5, 0.5, 0.5},
        width = 2900,
        height = 700,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "returnActionCards",
        function_owner = self,
        label = "",
        position = {-5.6, 0.6, -7.5},
        scale = {0.5, 0.5, 0.5},
        width = 2900,
        height = 700,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "getKeywordCards",
        function_owner = self,
        label = "",
        position = {5.5, 0.6, 0.8},
        scale = {0.5, 0.5, 0.5},
        width = 2000,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "discardActionCards",
        function_owner = self,
        label = "",
        position = {4.9, 0.6, -0.4},
        scale = {0.5, 0.5, 0.5},
        width = 2000,
        height = 400,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })

    self.createButton({
        click_function = "resetBoard",
        function_owner = self,
        label = "",
        position = {0, 0.6, 7.75},
        scale = {0.5, 0.5, 0.5},
        width = 3600,
        height = 700,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100}
    })
end

function createInput()
    self.createInput({
        input_function = "dud",
        label = "Type Keyword",
        function_owner = self,
        alignment = 3,
        position = {5.5, 0.6, 0.4},
        scale = {0.5, 0.5, 0.5},
        width = 4500,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100},
        value = ""
    })

    self.createInput({
        input_function = "dud",
        label = "Type in Number",
        function_owner = self,
        alignment = 3,
        position = {0, 0.6, 7},
        scale = {0.5, 0.5, 0.5},
        width = 4500,
        height = 400,
        font_size = 300,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100},
        value = ""
    })
end

function dud() end

function getKeywordCards()
    local inputs = self.getInputs()
    searchParam = inputs[1].value
    if searchParam == "" then
        print('Enter search value first')
        return;
    end
    getKeywordCardsRoutine(searchParam)
end

function getKeywordCardsRoutine(searchParam)
    local card = nil;
    local deck = nil;
    for _, object in ipairs(zoneDiscard.getObjects()) do
        if object.tag == "Deck" then
            deck = object
            break
        else
            if object.getGUID() ~= 'c671c0' then card = object end
        end
    end
    local found = false
    if deck ~= nil then
        for _, card in ipairs(deck.getObjects()) do
            local desc = card.description
            if string.find(desc, searchParam) then
                deck.takeObject({
                    position = {
                        deck.getPosition().x, deck.getPosition().y + 2,
                        deck.getPosition().z - 4.39
                    },
                    guid = card.guid,
                    flip = false
                })
                found = true
            end
        end
    end
    if card ~= nil then
        local desc = card.getDescription()
        -- print(desc .. '-' ..searchParam )
        if string.find(desc, searchParam) then
            local pos = card.getPosition()
            pos.y = pos.y + 2
            pos.z = pos.z - 4.39
            card.setPosition(pos);
            found = true
        end
    end

    if not found then
        print("Did not find any discard card with attribute '" .. searchParam ..
                  "'")
    end
end

function returnPast() Global.call('returnCards', {zonePast, true, {}}) end

function getDeck(zoneToCheck)
    for _, object in ipairs(zoneToCheck.getObjects()) do
        if object.tag == "Deck" or object.tag == "Card" then
            return object
        end
    end

    return nil
end

function has_enough_action_cards(numCards, deck, is_drawing_from_discard)
    local found = (deck and
                      (((deck.tag == "Deck") and
                          (#(deck.getObjects()) >= numCards)) or
                          ((deck.tag == "Card") and (numCards == 1))))

    if not found then
        if not is_drawing_from_discard then
            print('Not enough cards in Action Deck to draw ' .. numCards ..
                      '. If you wish to perform the draw anyway, click the button with the total number of cards remaining in the deck and then click the number for however many more you need, which will be drawn from the Discard Pile.')
        else
            print(
                'Not enough action cards found in either Action Deck or Discard Pile. Please make sure your game is properly set up.')
        end
    elseif is_drawing_from_discard then
        print('Drawing from the Discard Pile.')
        if deck.tag == "Deck" then deck.shuffle() end
    end

    return found
end

function draw(params)
    local numCards = params[1]
    local is_drawing_from_discard = params[2]

    local actionDeck = getDeck(zoneActionDeck)
    if not actionDeck or is_drawing_from_discard then
        -- Action Deck is empty, time to draw from the Discard Pile
        -- alternatively, I'm being forced to draw from Discard
        actionDeck = getDeck(zoneDiscard)
        is_drawing_from_discard = true
    end

    if not has_enough_action_cards(numCards, actionDeck, is_drawing_from_discard) then
        return
    end

    if actionDeck.tag == "Card" then
        -- Only gets here if there was just one card to be drawn
        if not is_drawing_from_discard then actionDeck.flip() end
        actionDeck.setPosition(pos1)
    else
        local positions = {}
        if numCards == 1 then
            positions = {pos1}
        elseif numCards == 2 then
            positions = {pos1, pos2}
        elseif numCards == 3 then
            positions = {pos1, pos2, pos3}
        elseif numCards == 4 then
            positions = {pos5, pos6, pos7, pos8}
        elseif numCards == 5 then
            positions = {pos1, pos2, pos3, pos6, pos7}
        elseif numCards == 6 then
            positions = {pos4, pos5, pos6, pos7, pos8, pos9}
        elseif numCards == 7 then
            positions = {pos1, pos2, pos3, pos5, pos6, pos7, pos8}
        elseif numCards == 8 then
            positions = {pos1, pos2, pos3, pos5, pos6, pos7, pos8, pos12}
        elseif numCards == 9 then
            positions = {pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, pos9}
        elseif numCards == 10 then
            positions = {
                pos1, pos2, pos3, pos5, pos6, pos7, pos8, pos11, pos12, pos13
            }
        elseif numCards == 11 then
            positions = {
                pos4, pos5, pos6, pos7, pos8, pos9, pos10, pos11, pos12, pos13,
                pos14
            }
        elseif numCards == 12 then
            positions = {
                pos2, pos4, pos5, pos6, pos7, pos8, pos9, pos10, pos11, pos12,
                pos13, pos14
            }
        elseif numCards == 13 then
            positions = {
                pos1, pos2, pos4, pos5, pos6, pos7, pos8, pos9, pos10, pos11,
                pos12, pos13, pos14
            }
        elseif numCards == 14 then
            positions = {
                pos1, pos2, pos3, pos4, pos5, pos6, pos7, pos8, pos9, pos10,
                pos11, pos12, pos13, pos14
            }
        end

        for _, pos in ipairs(positions) do
            actionDeck.takeObject({
                position = pos,
                flip = (not is_drawing_from_discard)
            })
        end
    end
end

function draw_1() draw({1, false}) end

function draw_2() draw({2, false}) end

function draw_3() draw({3, false}) end

function draw_4() draw({4, false}) end

function draw_5() draw({5, false}) end

function draw_6() draw({6, false}) end

function draw_7() draw({7, false}) end

function draw_8() draw({8, false}) end

function draw_9() draw({9, false}) end

function draw_10() draw({10, false}) end

function draw_11() draw({11, false}) end

function draw_12() draw({12, false}) end

function draw_13() draw({13, false}) end

function draw_14() draw({14, false}) end

function discardActionCards()
    local explicitCardList = {}
    local cardsToIgnore = {}
    for _, object in ipairs(zoneDiscard.getObjects()) do
        if object.tag == "Card" then
            table.insert(cardsToIgnore, object.guid)
        end
    end
    for _, object in ipairs(zoneActionDeck.getObjects()) do
        if object.tag == "Card" then
            table.insert(cardsToIgnore, object.guid)
        end
    end

    for _, object in ipairs(zoneDrawnCards.getObjects()) do
        if object.tag == "Card" then
            local skip_this_card = false
            if #cardsToIgnore > 0 then
                for _, guid in ipairs(cardsToIgnore) do
                    if guid == object.guid then
                        skip_this_card = true
                        break
                    end
                end
            end

            if not skip_this_card then
                table.insert(explicitCardList, object)
            end
        end
    end

    if #explicitCardList > 0 then
        Global.call('discardCards', {zoneDrawnCards, explicitCardList})
    else
        print('No cards found to discard.')
    end
end

function returnActionCards()
    local explicitCardList = {}
    local cardsToIgnore = {}
    for _, object in ipairs(zoneDiscard.getObjects()) do
        if object.tag == "Card" then
            table.insert(cardsToIgnore, object.guid)
        end
    end
    for _, object in ipairs(zoneActionDeck.getObjects()) do
        if object.tag == "Card" then
            table.insert(cardsToIgnore, object.guid)
        end
    end

    for _, object in ipairs(zoneDrawnCards.getObjects()) do
        if object.tag == "Card" then
            local skip_this_card = false
            if #cardsToIgnore > 0 then
                for _, guid in ipairs(cardsToIgnore) do
                    if guid == object.guid then
                        skip_this_card = true
                        break
                    end
                end
            end

            if not skip_this_card then
                table.insert(explicitCardList, object)
            end
        end
    end

    if #explicitCardList > 0 then
        Global.call('returnCards', {zoneDrawnCards, false, explicitCardList})
    else
        print('No cards found to return.')
    end
end

function resetBoard(obj, playerClickerColor)
    local inputs = self.getInputs()
    searchParam = inputs[2].value
    if searchParam == "" then
        print('Enter search value first')
        return;
    end

    resetBoardRoutine(searchParam)
    Player[playerClickerColor].lookAt({
        position = {x = 0, y = 0, z = 9.4},
        pitch = 70,
        distance = 40
    })
end

function resetBoardRoutine(searchParam)
    Global.call('clearExploreCardsTable', {})

    local reset_to_white_card = (string.sub(searchParam, 1, 1) == 'w')
    if reset_to_white_card then
        searchParam = string.gsub(searchParam, 'w', '', 1)
    end

    ---------------------------------------

    print('Looking for tokens, figurines, and card...')

    local allCards = {}
    local cardsToReturn = {}
    local playerFigurines = {}
    local allFires = {}
    local allWGUMCDTokens = {}
    local allFlyingRootsTokens = {}
    local allFacingTheElementsTokens = {}
    local allFearTheDevourersTokens = {}

    local WGUMCDBag = getObjectFromGUID('75d247')
    local flyingRootsBag = getObjectFromGUID('ad4dea')
    local facingTheElementsBag = getObjectFromGUID('0ad451')
    local fearTheDevourersBag = getObjectFromGUID('04ce4d')

    local zonePlayMat =
        getObjectFromGUID(Global.getTable('ZONES').ZONE_MAP.guid)
    local zoneAdvDeck = getObjectFromGUID(
                            Global.getTable('ZONES').ZONE_ADVENTURE_DECK.guid)

    for _, object in ipairs(zonePlayMat.getObjects()) do
        if object.getName() == "Campfire" or object.getName() == "Fire" then
            table.insert(allFires, object)
        elseif object.getDescription() == "wgumcd" then
            table.insert(allWGUMCDTokens, object)
        elseif object.getName() == "Flying Root" then
            table.insert(allFlyingRootsTokens, object)
        elseif object.getDescription() == "fte" then
            table.insert(allFacingTheElementsTokens, object)
        elseif string.find(object.getName(), "Rockworm") then
            table.insert(allFearTheDevourersTokens, object)
        elseif string.find(object.getDescription(), "figurine") then
            table.insert(playerFigurines, object)
        elseif object.tag == "Deck" or object.tag == "Card" then
            if object.tag == "Deck" then object.shuffle() end
            table.insert(allCards, object)
            table.insert(cardsToReturn, object)
        end
    end
    if #playerFigurines == 0 then
        print('No player figurines found in-game')
        -- return;
    end

    for _, object in ipairs(zoneAdvDeck.getObjects()) do
        if object.tag == "Deck" or object.tag == "Card" then
            if object.tag == "Deck" then object.shuffle() end
            table.insert(allCards, object)
        end
    end

    for _, object in ipairs(zonePast.getObjects()) do
        if object.tag == "Deck" or object.tag == "Card" then
            if object.tag == "Deck" then object.shuffle() end
            table.insert(allCards, object)
        end
    end

    ----------------------

    green_card = false
    green_deck = false
    local deckFound = nil
    cardFound = nil
    local cardGuid = ''
    for _, object in pairs(allCards) do
        if object.tag == "Deck" then
            for _, card in pairs(object.getObjects()) do
                if card.name == searchParam then
                    if (not reset_to_white_card and
                        not string.find(card.description, "white")) or
                        (reset_to_white_card and
                            string.find(card.description, "white")) then
                        deckFound = object
                        cardGuid = card.guid

                        if string.find(card.description, "green") or
                            string.find(card.description, "white") then
                            green_deck = true
                            break
                        end
                    end
                end
            end

            if green_deck then break end
        elseif object.getName() and object.getName() == searchParam and
            ((not reset_to_white_card and
                not string.find(object.getDescription(), "white")) or
                (reset_to_white_card and
                    string.find(object.getDescription(), "white"))) then
            cardFound = object
            if object.getDescription() and
                (string.find(object.getDescription(), "green") or
                    string.find(object.getDescription(), "white")) then
                green_card = true
                break
            end
        end
    end

    if cardFound and (green_card or not green_deck) then
        for idx, object in pairs(cardsToReturn) do
            if object.guid == cardFound.guid then
                table.remove(cardsToReturn, idx)
                break
            end
        end
        cardFound.setPosition({0.00, 5, 15.65})
    elseif deckFound then
        cardFound = deckFound.takeObject({
            position = {0.00, 5, 15.65},
            guid = cardGuid
        })
    else
        print('Could not find card ' .. searchParam)
        return;
    end

    ----------------------

    if #allFires > 0 then
        for _, fire in pairs(allFires) do fire.destroy() end
    end
    if #allWGUMCDTokens > 0 and WGUMCDBag then
        for _, token in pairs(allWGUMCDTokens) do
            WGUMCDBag.putObject(token)
        end
    end
    if #allFlyingRootsTokens > 0 and flyingRootsBag then
        for _, token in pairs(allFlyingRootsTokens) do
            flyingRootsBag.putObject(token)
        end
    end
    if #allFacingTheElementsTokens > 0 and facingTheElementsBag then
        for _, token in pairs(allFacingTheElementsTokens) do
            facingTheElementsBag.putObject(token)
        end
    end
    if #allFearTheDevourersTokens > 0 and fearTheDevourersBag then
        for _, token in pairs(allFearTheDevourersTokens) do
            fearTheDevourersBag.putObject(token)
        end
    end

    ----------------------

    print('Collecting board...')

    local pastPile = nil
    for _, object in ipairs(zonePast.getObjects()) do
        if object.tag == "Deck" then
            pastPile = object
            break
        end
    end

    if not pastPile and #cardsToReturn > 0 then
        pastPile = table.remove(cardsToReturn)

        if pastPile.tag == "Card" then
            pastPile.setPosition({-4.71, 5, -49.77})
            if #cardsToReturn > 0 then
                pastPile = table.remove(cardsToReturn)
                Wait.time(function()
                    pastPile.setPosition({-4.71, 5, -49.77})
                end, 0.1, 0)
            end
        else
            local pileToReturn = pastPile
            local firstCardGuid = ''
            local secondCardGuid = ''
            for _, card in ipairs(pileToReturn.getObjects()) do
                if firstCardGuid == '' then
                    firstCardGuid = card.guid
                else
                    secondCardGuid = card.guid
                    break
                end
            end
            pileToReturn.takeObject({
                position = {-4.71, 5, -49.77},
                guid = firstCardGuid
            })
            Wait.time(function()
                pileToReturn.takeObject({
                    position = {-4.71, 5, -49.77},
                    guid = secondCardGuid
                })
                table.insert(cardsToReturn, pileToReturn)
            end, 0.1, 0)
        end

        Wait.time(function()
            print('Setting up past...')
            for _, object in ipairs(zonePast.getObjects()) do
                if object.tag == "Deck" or object.tag == "Card" then
                    pastPile = object
                    break
                end
            end
        end, 2, 0)

        Wait.time(function()
            resetBoardHelper(cardsToReturn, searchParam, pastPile,
                             playerFigurines, cardFound)
        end, 3, 0)
    else
        resetBoardHelper(cardsToReturn, searchParam, pastPile, playerFigurines,
                         cardFound)
    end
end

function resetBoardHelper(cardsToReturn, searchParam, pastPile, playerFigurines,
                          card)
    if #cardsToReturn > 0 then
        for _, object in pairs(cardsToReturn) do
            pastPile.putObject(object)
        end
    end

    print('Setting up board...')

    card.setRotation({0, 180, 0})
    card.setPosition({0, 5, 15.65})

    posOffset = 0
    for _, figurine in pairs(playerFigurines) do
        figurine.setPosition({posOffset, 5, 11.45})
        posOffset = posOffset + 2
    end

    Wait.time(returnPast, 2, 0)
end
