function onLoad()
    self.createButton({
        click_function = "removeFire",
        function_owner = self,
        label = "",
        position = {0, 1.2, 0},
        scale = {0.5, 0.5, 0.5},
        width = 700,
        height = 700,
        font_size = 400,
        color = {1, 1, 1, 0},
        font_color = {0, 0, 0, 100},
        tooltip = "Click to put all fires out"
    })
end

function removeFire()
    broadcastToAll("The Fire has gone out", {1, 1, 1})
    local zone = getObjectFromGUID(Global.getTable('ZONES').ZONE_MAP.guid)
    for _, object in ipairs(zone.getObjects()) do
        if object.getName() == "Campfire" then
            object.destroy()
        elseif object.getName() == "Fire" then
            object.destroy()
        end
    end
end
