---
---Adds a bindable hotkey to the game.
---
---Players can bind key to hotkeys from the Options -> Game Keys UI after this function is called.
---@overload fun(label: string, callback: fun(playerColor: tts__PlayerColor, object: tts__Object, pointerPosition: nil | tts__Vector, isKeyUp: boolean): void): true
---@param label string
---@param callback fun(playerColor: tts__PlayerColor, hoveredObject: tts__Object, pointerPosition: nil | tts__Vector, isKeyUp: boolean): void
---@param triggerOnKeyUp boolean @Defaults to false
---@return true
function addHotkey(label, callback, triggerOnKeyUp) end

---
---Adds a menu item to the Global right-click context menu. Global menu is shown when player right-clicks on empty space or table.
---@overload fun(label: string, callback: (fun(playerColor: tts__PlayerHandColor, menuPosition: nil | tts__Vector): void)): true
---@overload fun(label: string, callback: (fun(playerColor: tts__PlayerHandColor, menuPosition: nil | tts__Vector): void), keepOpen: boolean): true
---@param label string @Text for the menu item.
---@param toRunFunc fun(playerColor: tts__PlayerHandColor, menuPosition: nil | tts__Vector): void
---@param keep_open boolean @Whether the context menu should remain open after the item is selected. Defaults to false.
---@param require_table boolean @Whether the menu item is only included if the table is being hovered over. Defaults to false.
---@return true @Technically, returns false if your `callback` param is nil. However, Luanalysis won't allow you to make that mistake.
function addContextMenuItem(label, toRunFunc, keep_open, require_table) end

---
---Clears all menu items added by function addContextMenuItem(...).
---@return true
function clearContextMenu() end

---
---Print an on-screen message to all Players.
---@overload fun(message: string): boolean
---@param message string
---@param message_tint tts__ColorShape
---@return boolean
function broadcastToAll(message, message_tint) end

---
---Print an on-screen message to a specified Player and their in-game chat.
---@overload fun(message: string, playerColor: tts__PlayerColor): boolean
---@param message string
---@param playerColor tts__PlayerColor
---@param message_tint tts__ColorShape
---@return boolean
function broadcastToColor(message, playerColor, message_tint) end

---
---Groups objects together, like how the G key does for players. It returns a table of object references to any decks/stacks formed.
---
---Not all objects CAN be grouped. If the G key won't work on them, neither will this function.
---@param objects tts__Object[]
---@return (tts__Container | tts__Stackable)[]
function group(objects) end

---
---Logs a message to the host's System Console (accessible from ~ pane of in-game chat window).
---@overload fun(value: any): string
---@overload fun(value: any, label: string): string
---@param value any
---@param label string
---@param tags string
---@return string
function log(value, label, tags) end

---
---Returns a String formatted similarly to the output of log(...).
---@overload fun(value: any): string
---@overload fun(value: any, label: string): string
---@overload fun(value: any, label: string, tag: string): string
---@overload fun(value: any, label: string, tag: string, concise: boolean): string
---@param value any
---@param label string
---@param tag string
---@param concise boolean @Default false
---@param displayTag boolean @Default false
---@return string
function logString(value, label, tag, concise, displayTag) end

---
---Print a message into the in-game chat of all connected players.
---@overload fun(message: string): boolean
---@param message string
---@param message_tint? tts__ColorShape
---@return boolean
function printToAll(message, message_tint) end

---
---Print a message to the in-game chat of a specific player.
---@overload fun(message: string, player_color: tts__PlayerColor): boolean
---@param message string
---@param player_color tts__PlayerColor
---@param message_tint? tts__ColorShape
---@return boolean
function printToColor(message, player_color, message_tint) end

---
---Converts a Player Color string into a Color Table for tinting.
---@overload fun(player_color: string): string
---@param player_color tts__PlayerColor
---@return string
function stringColorToRGB(player_color) end
