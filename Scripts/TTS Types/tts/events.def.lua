---Called whenever a script needs to save its state.
---@return string
function onSave() end

---Called when a save has completely finished loading.
---@param script_state string
function onLoad(script_state) end

---Called when an object is dropped by a player.
---@param player_color tts__PlayerColor
---@param object tts__Object
function onObjectDrop(player_color, object) end
