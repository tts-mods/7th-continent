import json
import argparse


def clean_up_coordinates(
    s: dict | list, ndigits: int = 2, fix_rotation_distance: float = 2.0, snap_digits: int = None
) -> None:
    """Clean up the coordinates of TTS JSON save files by rounding them.

    Args:
        ndigits (int, optional): precision in decimal digits. Defaults to 2.
        fix_rotation_distance (float, optional): maximum distance from default rotations (0, 90, 180, or 270) to round them. Defaults to 2.0.
        snap_digits (int, optional): precision in decimal digits of the coordinates of Snap Points specifically. Defaults to 2 * ndigits.
    """
    if (
        (type(s) is not dict and type(s) is not list)
        or not type(ndigits) is int
        or not type(fix_rotation_distance) is float
        or (snap_digits is not None and type(snap_digits) is not int)
    ):
        raise TypeError

    if ndigits < 0:
        ndigits = 0
    if fix_rotation_distance < 0:
        fix_rotation_distance = -fix_rotation_distance
    if snap_digits is None:
        snap_digits = 2 * ndigits

    if type(s) is dict:
        iterator = s.items()
    elif type(s) is list:
        iterator = enumerate(s)

    for key, value in iterator:
        if type(value) is dict or type(value) is list:
            if key == "AttachedSnapPoints":
                clean_up_coordinates(value, snap_digits, fix_rotation_distance)
            else:
                clean_up_coordinates(value, ndigits, fix_rotation_distance, snap_digits)
        elif type(value) is float:
            s[key] = round(value, ndigits)
            if fix_rotation_distance > 0 and "rot" in key:
                if (
                    abs(s[key]) < fix_rotation_distance
                    or abs(s[key] - 360) < fix_rotation_distance
                ):
                    s[key] = 0
                elif abs(s[key] - 90) < fix_rotation_distance:
                    s[key] = 90
                elif abs(s[key] - 180) < fix_rotation_distance:
                    s[key] = 180
                elif abs(s[key] - 270) < fix_rotation_distance:
                    s[key] = 270
            if int(s[key]) == s[key]:
                s[key] = int(s[key])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "filepath",
        type=str,
        help="path to the save file being cleaned up",
    )
    parser.add_argument(
        "-n",
        "--ndigits",
        type=int,
        help="precision to round coordinates to in number of decimal digits",
        default=2,
    )
    parser.add_argument(
        "-r",
        "--fixrot",
        type=float,
        help="fix rotation values, bringing any values this close to 0, 90, 180, or 270 degrees to the closest one of those (set to 0 to disable)",
        default=2.0,
    )
    parser.add_argument(
        "-s",
        "--snapdigits",
        type=int,
        help="precision to round specifically Snap Point coordinates to in number of decimal digits (if not set will use twice the value set by --ndigits)",
    )

    args = parser.parse_args()
    config = vars(args)

    with open(config["filepath"]) as f:
        data = json.load(f)

    clean_up_coordinates(data, config["ndigits"], config["fixrot"], config["snapdigits"])

    with open(config["filepath"], "w") as f:
        f.write(json.dumps(data, indent=2))
