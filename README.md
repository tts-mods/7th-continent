# The 7th Continent

This is a project to create a Tabletop Simulator version of the game "The 7th Continent".

- The scripts are written in Lua 5.2, the version supported by Tabletop Simulator.
- The formatter used is [EmmyLuaCodeStyle](https://marketplace.visualstudio.com/items?itemName=CppCXY.emmylua-codestyle).
- The debugger used is [EmmyLua](https://marketplace.visualstudio.com/items?itemName=tangzx.emmylua), for compatibility with [Benjamin Dobell's tts-types](<https://marketplace.visualstudio.com/items?itemName=tangzx.emmylua>).
- For standardisation, we will use:
  - camelCase for functions;
  - snake_case for variables;
  - and SCREAMING_SNAKE_CASE for constants.
